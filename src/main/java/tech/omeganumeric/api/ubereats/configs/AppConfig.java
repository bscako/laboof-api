package tech.omeganumeric.api.ubereats.configs;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
//@PropertySource("classpath:swagger.properties")
public class AppConfig {

    public static final String BASE_PATH = "api/";

    public static final String API_PRODUCES = MediaType.APPLICATION_JSON_VALUE;
    public static final String API_CONSUMES = MediaType.APPLICATION_JSON_VALUE;

    @Bean
    public ModelMapper modelMapperBean() {
        return new ModelMapper();
    }

}
