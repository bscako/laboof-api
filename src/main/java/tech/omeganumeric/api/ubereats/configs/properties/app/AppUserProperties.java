package tech.omeganumeric.api.ubereats.configs.properties.app;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app.user", ignoreInvalidFields = true)
@Data
public class AppUserProperties implements IAppUserProperties {
    private String name;
    private String email;
    private String login;
    private String phone;
    private String password;
    private String firstname;
    private String lastname;
}
