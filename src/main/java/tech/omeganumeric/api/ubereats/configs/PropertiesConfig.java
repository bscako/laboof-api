package tech.omeganumeric.api.ubereats.configs;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import tech.omeganumeric.api.ubereats.configs.properties.AppProperties;
import tech.omeganumeric.api.ubereats.configs.properties.SwaggerProperties;
import tech.omeganumeric.api.ubereats.configs.properties.app.AppAdminProperties;
import tech.omeganumeric.api.ubereats.configs.properties.app.AppUserProperties;
import tech.omeganumeric.api.ubereats.modules.authentication.configs.properties.AuthenticationProperties;
import tech.omeganumeric.api.ubereats.modules.registration.configs.properties.MailProperties;
import tech.omeganumeric.api.ubereats.modules.registration.configs.properties.RegistrationProperties;
import tech.omeganumeric.api.ubereats.modules.store.configs.properties.StoreProperties;


@Configuration
@PropertySources({
        @PropertySource("classpath:app.properties"),
        @PropertySource("classpath:authentication.properties"),
        @PropertySource("classpath:store.properties"),
        @PropertySource("classpath:file-storage.properties"),
        @PropertySource("classpath:mail.properties"),
        @PropertySource("classpath:swagger.properties"),
        @PropertySource("classpath:registration.properties"),
})
@EnableConfigurationProperties({
        AppProperties.class,
        AppAdminProperties.class,
        AppUserProperties.class,
        StoreProperties.class,
        AuthenticationProperties.class,
        SwaggerProperties.class,
        MailProperties.class,
        RegistrationProperties.class
})
public class PropertiesConfig {
}
