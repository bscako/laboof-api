package tech.omeganumeric.api.ubereats.configs;

import com.fasterxml.classmate.TypeResolver;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.*;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static springfox.documentation.schema.AlternateTypeRules.newRule;

@Configuration
@EnableSwagger2WebMvc
@Import({
        SpringDataRestConfiguration.class,
        BeanValidatorPluginsConfiguration.class

})
public class SwaggerConfig {

    private final List<SecuritySchemeDefinition> SECURITY_SCHEMES_DEFINITION = Arrays.asList(
            SecuritySchemeDefinition.builder()
                    .name("API KEY Authentication")
                    .reference("API-KEY")
                    .build()
            ,
            SecuritySchemeDefinition.builder()
                    .name("User Authentication Token")
                    .reference("Authorization")
                    .build()
//            ,
//            SecuritySchemeDefinition.builder()
//                    .name("User Authentication Token")
//                    .reference("Authorization")
//                    .type(SecuritySchemeDefinition.TypeSecurity.BASIC)
//                    .build()
    );
    private TypeResolver typeResolver;
    private BuildProperties buildProperties;

    @Autowired
    public SwaggerConfig(TypeResolver typeResolver, BuildProperties buildProperties) {
        this.buildProperties = buildProperties;
        this.typeResolver = typeResolver;
    }

    @Bean
    public Docket ubereatsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("ubereats-webservices")
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiMetadata())
                .pathMapping("/")
                .directModelSubstitute(LocalDate.class, String.class)
                .genericModelSubstitutes(ResponseEntity.class)
                .alternateTypeRules(
                        newRule(typeResolver.resolve(DeferredResult.class,
                                typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
                                typeResolver.resolve(WildcardType.class)))
                .useDefaultResponseMessages(false)
//                .globalResponseMessage(RequestMethod.GET,
//                        CollectionHelper.newArrayList(new ResponseMessageBuilder()
//                                .code(500)
//                                .message("500 message")
//                                .responseModel(new ModelRef("Error"))
//                                .build()))
                .securitySchemes(securitySchemes())
                .securityContexts(securityContext())
                .enableUrlTemplating(true)
//                .globalOperationParameters(
//                        new ArrayList(new ParameterBuilder()
//                                .name("someGlobalParameter")
//                                .description("Description of someGlobalParameter")
//                                .modelRef(new ModelRef("string"))
//                                .parameterType("query")
//                                .required(true)
//                                .build()))
//                .tags(
//                        new Tag("Repositories", "All services relating to repositories"))
//                .additionalModels(typeResolver.resolve(AdditionalModel.class))
                ;
    }

    private List<ResponseMessage> getCustomizedResponseMessages() {
        List<ResponseMessage> responseMessages = new ArrayList<>();
        responseMessages.add(new ResponseMessageBuilder().code(500).message("Server has crashed!!")
                .responseModel(new ModelRef("Error")).build());
        responseMessages.add(new ResponseMessageBuilder().code(403).message("You shall not pass!!")
                .build());
        return responseMessages;
    }


//    @Bean
//    public Docket api() {
//        return new
//                Docket(DocumentationType.SWAGGER_2)
//                .groupName("Example")
//                .directModelSubstitute(XMLGregorianCalendar.class, String.class)
//                .select()
//                .apis(RequestHandlerSelectors.any())
////                        .apis(RequestHandlerSelectors.withMethodAnnotation(RepositoryRestResource.class))
////                        .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
//                .paths(PathSelectors.any())
//                .build().apiInfo(apiMetadata())
////                    .securitySchemes(this.securitySchemes())
////                    .securityContexts(Arrays.asList(this.securityContext()))
//                ;
//
//    }

    @Bean
    UiConfiguration uiConfig() {
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .displayOperationId(false)
                .defaultModelsExpandDepth(1)
                .defaultModelExpandDepth(1)
                .defaultModelRendering(ModelRendering.EXAMPLE)
                .displayRequestDuration(false)
                .docExpansion(DocExpansion.NONE)
                .filter(false)
                .maxDisplayedTags(null)
                .operationsSorter(OperationsSorter.ALPHA)
                .showExtensions(false)
                .tagsSorter(TagsSorter.ALPHA)
                .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
                .validatorUrl(null)
                .build();
    }

    /*
        Security implementation
     */

//    public List<SecurityContext> securityContext() {
//        AuthorizationScope[] scopes = { new AuthorizationScope("read", "for read operation"),
//                new AuthorizationScope("write", "for write operation") };
//        List<SecurityReference> securityReferences = Arrays.asList(
////                new SecurityReference("basicAuth", scopes),
//                new SecurityReference("Key", scopes),
//                new SecurityReference("User Authentication Token", scopes)
//        );
//        return Stream.of(
//                SecurityContext.builder().securityReferences(securityReferences)
//                        .forPaths(PathSelectors.any())
//                        .build()
//        ).collect(Collectors.toList());
//    }
//
//    public List<SecurityScheme> securitySchemes() {
////        SecurityScheme basicAuth = new BasicAuth("basicAuth");
//        SecurityScheme userAuthToken = new ApiKey(
//                "User Authentication Token",
//                "Authorization",
//                "header"
//        );
//        SecurityScheme keyAuth = new ApiKey(
//                "API KEY",
//                "API-KEY",
//                "header"
//        );
//        return Arrays.asList(
//                keyAuth,
//                userAuthToken
////                basicAuth
//        );
//    }

    private List<SecurityContext> securityContext() {
        List<SecurityReference> securityReferences = SECURITY_SCHEMES_DEFINITION.stream()
                .map(SecuritySchemeDefinition::getSecurityReference)
                .collect(Collectors.toList());
        return Stream.of(
                SecurityContext.builder().securityReferences(securityReferences)
                        .forPaths(PathSelectors.any())
                        .build()
        ).collect(Collectors.toList());
    }

    public List<SecurityScheme> securitySchemes() {
        return SECURITY_SCHEMES_DEFINITION.stream()
                .map(SecuritySchemeDefinition::getSecurityScheme)
                .collect(Collectors.toList());
    }

    /**
     * Adds meta to Swagger
     *
     * @return ApiInfo
     */
    private ApiInfo apiMetadata() {
        String title = "UBER-EATS WEBSERVICE";
        String description = "Description";
        String company = "OMEGANUMERIC";
        String website = "omeganumeric.tech";
        String email = "contact@omeganumeric.tech";
        String licence = "";
        String licenceUrl = "";
        String termService = "";
        return new ApiInfoBuilder().title(title)
                .description(description)
                .version(buildProperties.getVersion())
                .contact(new Contact(company,
                        website,
                        email))
                .license(licence)
                .licenseUrl(licenceUrl)
                .termsOfServiceUrl(termService).build();
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Data
    @Builder
    private static class SecuritySchemeDefinition {

        private String name;
        private String reference;
        @Builder.Default
        private String passAs = "header";
        @Builder.Default
        private TypeSecurity type = TypeSecurity.KEY;
        @Builder.Default
        private AuthorizationScope[] authorizationScopes = {
                new AuthorizationScope("read", "for read operation"),
                new AuthorizationScope("write", "for write operation")
        };

        SecurityScheme getSecurityScheme() {
            if (type == TypeSecurity.BASIC) {
                return new BasicAuth(this.getReference());
            }
            return new ApiKey(this.getName(), this.getReference(), this.getPassAs());
        }

        SecurityReference getSecurityReference() {
            return new SecurityReference(this.getName(), this.getAuthorizationScopes());
        }

        public enum TypeSecurity {
            BASIC, KEY
        }
    }


}
