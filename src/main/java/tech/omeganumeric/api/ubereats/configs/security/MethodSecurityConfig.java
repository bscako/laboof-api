package tech.omeganumeric.api.ubereats.configs.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.expression.method.DefaultMethodSecurityExpressionHandler;
import org.springframework.security.access.expression.method.MethodSecurityExpressionHandler;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.GlobalMethodSecurityConfiguration;
import tech.omeganumeric.api.ubereats.configs.properties.AppProperties;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.handlers.AuthorizationRolePermissionEvaluator;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {

    private final UserEncoder usernameEncoder;
    private final AppProperties appProperties;

    public MethodSecurityConfig(
            UserEncoder usernameEncoder,
            AppProperties appProperties
    ) {
        this.usernameEncoder = usernameEncoder;
        this.appProperties = appProperties;
    }

    @Bean
    public AuthorizationRolePermissionEvaluator authorizationRole() {
        return new AuthorizationRolePermissionEvaluator(this.usernameEncoder, appProperties);
    }

    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        DefaultMethodSecurityExpressionHandler expressionHandler =
                new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(authorizationRole());
        return expressionHandler;
    }
}
