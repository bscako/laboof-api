package tech.omeganumeric.api.ubereats.configs.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import tech.omeganumeric.api.ubereats.configs.properties.app.AppAdminProperties;
import tech.omeganumeric.api.ubereats.configs.properties.app.AppUserProperties;

@Configuration
@ConfigurationProperties("app")
@Data
public class AppProperties {

    private String name;
    private String description;
    private String type;
    private AppAdminProperties admin;
    private AppUserProperties user;

}
