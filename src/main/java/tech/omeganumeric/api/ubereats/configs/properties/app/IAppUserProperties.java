package tech.omeganumeric.api.ubereats.configs.properties.app;

public interface IAppUserProperties {
    String getName();
    String getEmail();
    String getLogin();
    String getPhone();
    String getPassword();
    String getFirstname();
    String getLastname();
}
