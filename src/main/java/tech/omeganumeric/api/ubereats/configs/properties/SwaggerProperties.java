package tech.omeganumeric.api.ubereats.configs.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("swagger")
@Data
public class SwaggerProperties {

    private Group group;
    private Path path;
    private MetaData metaData;


    @Data
    public static class MetaData {
        private String title;
        private String company;
        private String website;
        private String email;
        private Licence licence;
        private Team team;
    }

    @Data
    public static class Group {
        private String name;
    }

    @Data
    public static class Path {
        private String mapping;
    }

    @Data
    public static class Licence {
        private String index;
        private String url;
    }

    @Data
    public static class Team {
        private String service;
    }

}
