package tech.omeganumeric.api.ubereats.configs.security;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import tech.omeganumeric.api.ubereats.controllers.AppRestController;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiTokenConfigurer;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiTokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiTokenService;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.handlers.AuthorizationEntryPoint;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserTokenConfigurer;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserTokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserTokenService;
import tech.omeganumeric.api.ubereats.modules.authentication.controllers.AuthenticationRestController;
import tech.omeganumeric.api.ubereats.modules.registration.controllers.RegistrationRestController;
import tech.omeganumeric.api.ubereats.modules.registration.controllers.ValidationOtpRestController;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String SEPARATOR = "/";
    private static final String ALL = SEPARATOR + "**";
    private static final String REST_REPOSITORY_PATH = "store";
    private static final String PROFILE_PATH = "profile";

    public static final String[] API_KEY_WHITELIST = {
            PROFILE_PATH, PROFILE_PATH + ALL,
            REST_REPOSITORY_PATH, REST_REPOSITORY_PATH + ALL,
            AppRestController.PATH + SEPARATOR + AppRestController.KEY
    };

    public static final String[] USER_KEY_WHITELIST = {
            AuthenticationRestController.PATH,
            AuthenticationRestController.PATH + SEPARATOR + ALL,
            AuthenticationRestController.PATH + SEPARATOR + ALL,
            ValidationOtpRestController.PATH,
            ValidationOtpRestController.PATH + SEPARATOR + ALL,
            RegistrationRestController.PATH,
            RegistrationRestController.PATH + SEPARATOR + ALL,

//            RegistrationRestController.PATH,
//            RegistrationRestController.PATH + SEPARATOR + ALL,
//            SecurityRestController.PATH + SEPARATOR + SecurityRestController.AUTH,
//            PRE_REPOSITORY_PATH + RegionRepository.PATH,
//            PRE_REPOSITORY_PATH + RegionRepository.PATH + ALL,
//
//            PRE_REPOSITORY_PATH + RoleRepository.PATH,
//            PRE_REPOSITORY_PATH + RoleRepository.PATH + ALL,
////            PRE_REPOSITORY_PATH + MediaRepository.PATH,
////            PRE_REPOSITORY_PATH + MediaRepository.PATH + ALL,
//            PRE_REPOSITORY_PATH + OrderStatusRepository.PATH,
//            PRE_REPOSITORY_PATH + OrderStatusRepository.PATH + ALL,
//            PRE_REPOSITORY_PATH + PhoneRepository.PATH,
//            PRE_REPOSITORY_PATH + PhoneRepository.PATH + ALL,
//            PRE_REPOSITORY_PATH + PaymentModeRepository.PATH,
//            PRE_REPOSITORY_PATH + PaymentModeRepository.PATH + ALL,
    };

    public static final String[] AUTH_BLACKLIST = {
            AuthenticationRestController.PATH + SEPARATOR + AuthenticationRestController.USER,
            AuthenticationRestController.PATH + SEPARATOR + AuthenticationRestController.USER + ALL,
            AuthenticationRestController.PATH + SEPARATOR + AuthenticationRestController.LOGOUT,
            AuthenticationRestController.PATH + SEPARATOR + AuthenticationRestController.LOGOUT + ALL,
//
//            OTPResourceController.PATH,
//            OTPResourceController.PATH + ALL
    };

    private static final String[] AUTH_BLACKLIST_ADMIN = {
//            SecurityUserRestController.PATH,
//            SecurityUserRestController.PATH + ALL,
//            PRE_REPOSITORY_PATH + UserApiRepository.PATH,
//            PRE_REPOSITORY_PATH + UserApiRepository.PATH + ALL,
//            PRE_REPOSITORY_PATH + AuthorityApiRepository.PATH,
//            PRE_REPOSITORY_PATH + AuthorityApiRepository.PATH + ALL,

    };


    private final AuthorizationEntryPoint unauthorizedHandler;
    private final AuthenticationSuccessHandler successHandler;
    private final AuthenticationManagerBuilder authenticationManagerBuilder;
    private final PasswordEncoder passwordEncoder;
    private final ApiTokenService apiTokenService;
    private final ApiTokenProvider apiTokenProvider;
    private final UserTokenService userTokenService;
    private final UserTokenProvider userTokenProvider;
    private final DataSource dataSource;

    public SecurityConfig(
            AuthorizationEntryPoint unauthorizedHandler,
            AuthenticationSuccessHandler successHandler,
            AuthenticationManagerBuilder authenticationManagerBuilder,
            PasswordEncoder passwordEncoder,
            ApiTokenService apiTokenService,
            ApiTokenProvider apiTokenProvider,
            UserTokenService userTokenService,
            UserTokenProvider userTokenProvider,
            DataSource dataSource
    ) {
        this.unauthorizedHandler = unauthorizedHandler;
        this.successHandler = successHandler;
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.passwordEncoder = passwordEncoder;
        this.apiTokenService = apiTokenService;
        this.apiTokenProvider = apiTokenProvider;
        this.userTokenService = userTokenService;
        this.userTokenProvider = userTokenProvider;
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userTokenService)
                .passwordEncoder(this.passwordEncoder)
//                .and()
//                .authenticationProvider(this.authenticationProvider())
//                .jdbcAuthentication()
//                .dataSource(dataSource)
        ;
    }

//    @Bean
//    public DaoAuthenticationProvider authenticationProvider() {
//        final DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
//        authProvider.setUserDetailsService(this.userTokenService);
//        authProvider.setPasswordEncoder(this.passwordEncoder);
//        return authProvider;
//    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    private ApiTokenConfigurer apiTokenConfigurer() {
        return new ApiTokenConfigurer(
                this.apiTokenService,
                this.apiTokenProvider
        );
    }

    private UserTokenConfigurer userTokenConfigurer() throws Exception {
        return new UserTokenConfigurer(
                this.userTokenService,
                this.userTokenProvider,
                this.successHandler
        );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                // we don't need CSRF because our token is invulnerable
                .csrf().disable()

                .formLogin().disable()
//                .formLogin().loginProcessingUrl(AuthenticationRestController.PATH + SEPARATOR + AuthenticationRestController.AUTH)
//                .successHandler(this.successHandler).failureHandler(this.failureHandler)
//                .and()
                .httpBasic().disable()
                .logout().disable()

                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler)

                .and()
                .cors()
                .and()

                // don't create session
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()

                .authorizeRequests()

                .antMatchers(API_KEY_WHITELIST).permitAll()
                .antMatchers(USER_KEY_WHITELIST).permitAll()

                .antMatchers(AUTH_BLACKLIST).authenticated()
//
//                .antMatchers(AUTH_BLACKLIST_ADMIN).authenticated()

//         .and().requiresChannel().
                .and().apply(this.apiTokenConfigurer())
                .and().apply(this.userTokenConfigurer())

                // disable page caching
                .and().headers().frameOptions().sameOrigin()  // required to set for H2 else H2 Console will be blank.
                .cacheControl()

        ;


    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // AuthenticationTokenFilter will ignore the below paths
        web
                // .ignoring()
                // .antMatchers(
                // HttpMethod.POST,
                // this.authenticationPath
                // )
                //
                // // allow anonymous resource requests
                // .and()
                .ignoring()
                .antMatchers(HttpMethod.OPTIONS, "/**")
                .antMatchers(
                        HttpMethod.GET,
                        "/",
                        "/*.html",
                        "/favicon.ico",
                        "/**/*.html",
                        "/**/*.css",
                        "/**/*.js"
                );

        // Un-secure H2 Database (for testing purposes, H2 console shouldn't be unprotected in production)
        // .and().ignoring().antMatchers("/h2-console/**/**");
    }


}
