package tech.omeganumeric.api.ubereats.modules.store.entities.embeddable;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class RateMenuId extends RestaurantMenuId {

    @Column(name = "user_id")
    @NotNull
    private Long userId;

}
