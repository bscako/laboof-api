package tech.omeganumeric.api.ubereats.modules.store.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CredentialValidator.class)
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidCredential {
    String message() default "Invalid credential";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
