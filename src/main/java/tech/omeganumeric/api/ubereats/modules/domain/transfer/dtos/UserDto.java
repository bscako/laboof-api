package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.*;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;

import javax.validation.constraints.Null;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends UserData<UserDto, PhoneDto, UserInformationDto,
        ApplicationDto, RoleDto,
        UserCredentialDto> {

    @JsonIgnore
    @Null
    private Long id;


    @Builder(builderMethodName = "UserDto")
    public UserDto(
            UserIdDataAbstract<UserIdDto, PhoneDto> identifier,
            UserInformationDto details,
            ApplicationDto application,
            RoleDto role,
            UserCredentialDto authentication,
            Long id
    ) {
        super(identifier, application, role, details, authentication);
        this.id = id;
    }

    public static UserDto of(UserData<UserDto, PhoneData, UserInformationData,
            ApplicationData, RoleData,
            UserCredentialData> user) {
        UserIdDataAbstract<UserIdDto, PhoneDto> identifierDto = new UserIdDataAbstract<>(
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getLogin)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getEmail)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getPhone)
                        .map(PhoneData::toDto)
                        .orElse(null)
        );
        return UserDto.UserDto()
                .identifier(identifierDto)
                .application(user.getApplication().toDto())
                .role(user.getRole().toDto())
                .authentication(user.getCredential().toDto())
                .details(user.getDetails().toDto())
                .build();
    }

    public static UserDto of(
            UserDataPrincipal<UserDto, PhoneData,
            ApplicationData, RoleData> user
    ) {
        UserIdDataAbstract<UserIdDto, PhoneDto> identifierDto = new UserIdDataAbstract<>(
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getLogin)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getEmail)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getPhone)
                        .map(PhoneData::toDto)
                        .orElse(null)
        );
        return UserDto.UserDto()
                .identifier(identifierDto)
                .application(user.getApplication().toDto())
                .role(user.getRole().toDto())
                .build();
    }

    public static UserDto of(UserDataUpdate<UserDto, UserInformationData, UserCredentialData, PhoneData> user) {
        UserIdDataAbstract<UserIdDto, PhoneDto> identifierDto = new UserIdDataAbstract<>(
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getLogin)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getEmail)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getPhone)
                        .map(PhoneData::toDto)
                        .orElse(null)
        );
        return UserDto.UserDto()
                .identifier(
                        identifierDto
                )
                .details(
                        Optional.ofNullable(user.getDetails())
                                .map(UserInformationData::toDto)
                                .orElse(null)
                )
                .authentication(
                        Optional.ofNullable(user.getCredential())
                                .map(UserCredentialData::toDto)
                                .orElse(null)
                )
                .build();
    }

    public static UserDto of(User user, Application application, Role role) {
        UserIdDataAbstract<UserIdDto, PhoneDto> identifierDto = new UserIdDataAbstract<>(
                user.getLogin(),
                user.getEmail(),
                PhoneDto.of(user.getPhone())
        );
        return UserDto.UserDto()
                .id(user.getId())
                .identifier(identifierDto)
                .application(ApplicationDto.of(application))
                .role(RoleDto.of(role))
                .authentication(user.getUserCredentials().stream()
                        .filter(uc -> uc.getRole().getName().equalsIgnoreCase(role.getName()))
                        .filter(uc -> uc.getApplication().getName().equalsIgnoreCase(application.getName()))
                        .findAny().map(UserCredentialDto::of).orElse(null)
                )
                .details(UserInformationDto.of(user))
                .build();
    }

    public User map(User user, Application application, Role role) {
        user.setEmail(
                Optional.ofNullable(this.getIdentifier())
                        .map(UserIdDataAbstract::getEmail)
                        .orElse(user.getEmail())
        );
        user.setLogin(
                Optional.ofNullable(this.getIdentifier())
                        .map(UserIdDataAbstract::getLogin)
                        .orElse(user.getLogin())
        );
        user.setPhone(
                Optional.ofNullable(this.getIdentifier())
                        .map(UserIdDataAbstract::getPhone)
                        .map(PhoneData::toDto)
                        .map(c -> c.map(user.getPhone()))
                        .orElse(user.getPhone())
        );
        user.getUserCredentials().forEach(
                credential -> {
                    if (
                            credential.getApplication().getName().equalsIgnoreCase(application.getName())
                                    && credential.getRole().getName().equalsIgnoreCase(role.getName())
                    ) {
                        UserCredential finalCredential = credential;
                        credential = Optional.ofNullable(this.getCredential())
                                .map(c -> c.map(finalCredential))
                                .orElse(credential);
                    }
                }
        );
        user.setUserInformation(
                Optional.ofNullable(this.getDetails())
                        .map(u -> u.map(user.getUserInformation()))
                        .orElse(user.getUserInformation())
        );
        return user;
    }

    @JsonIgnoreProperties({"id"})
    @Override
    public UserIdDataAbstract<UserIdDto, PhoneDto> getIdentifier() {
        return super.getIdentifier();
    }

    @JsonIgnoreProperties({"id", "identifier"})
    @Override
    public UserInformationDto getDetails() {
        return super.getDetails();
    }

    @JsonIgnoreProperties({"id"})
    @Override
    public ApplicationDto getApplication() {
        return super.getApplication();
    }

    @JsonIgnoreProperties({"id"})
    @Override
    public RoleDto getRole() {
        return super.getRole();
    }

    @JsonIgnore
    @Override
    public UserCredentialDto getCredential() {
        return super.getCredential();
    }

    @JsonIgnoreProperties({"user", "application", "role"})
    public UserCredentialDto getAuthentication() {
        return this.getCredential();
    }

    @Override
    public UserDto toDto() {
        return this;
    }
}
