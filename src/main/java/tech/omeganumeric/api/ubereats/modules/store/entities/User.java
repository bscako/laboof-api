package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;
import tech.omeganumeric.api.ubereats.modules.store.entities.address.Location;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.IdUserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@RestResource
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"userCredentials"})
@ToString(callSuper = true,
        exclude = {"userCredentials"})
@Table(
        name = User.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = "id",
                        name = "uk_user_id"),
                @UniqueConstraint(
                        columnNames = "email",
                        name = "uk_user_email"),
                @UniqueConstraint(
                        columnNames = "login",
                        name = "uk_user_login"),
                @UniqueConstraint(
                        columnNames = {"id", "email", "login"},
                        name = "uk_user_pkey")
        })
public class User extends IdUserDateAudit {

    public static final String FIELD_ENTITY = "user";
    public static final String FIELD_ENTITY_TABLE_NAME = "users";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_LOGIN = "login";

    private static final long serialVersionUID = 3679301844774105320L;

    @OneToOne(cascade = CascadeType.ALL, targetEntity = Phone.class)
    @JoinTable(
            name = "users_phones",
            joinColumns = @JoinColumn(name = "user_id", unique = true),
            inverseJoinColumns = @JoinColumn(
                    name = "phone_id",
                    unique = true
            )
    )
    @JsonIgnoreProperties(value = {"user"})
//    @NotNull
    private Phone phone;

    @Column(name = "email", unique = true)
    @NotNull
    private String email;

    @Column(name = "login", unique = true)
    @NotNull
    private String login;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id", unique = true)
    @MapsId
    @JsonIgnoreProperties(value = {"user"})
    @NotNull
    private UserInformation userInformation;


    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = {"user"})
    @Builder.Default
    private Set<UserCredential> userCredentials = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "location")
    @JsonIgnoreProperties(value = {"users"})
    @Builder.Default
    private Location location = null;

    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "residence", nullable = false)
    @JoinColumn(name = "residence")
    @JsonIgnoreProperties(value = {"residents"})
//    @NotNull
    @Builder.Default
    private Address residence = null;

    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "delivery", nullable = false)
    @JoinColumn(name = "delivery")
    @JsonIgnoreProperties(value = {"deliveryUsers"})
    @Builder.Default
    private Address delivery = null;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eater", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"eater"})
    @Builder.Default
    private Set<Order> eaterOrders = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "driver", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"driver"})
    @Builder.Default
    private Set<Order> driverOrders = new HashSet<>();

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "users_saved_addresses",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "address_id", referencedColumnName = "id"))
    @JsonIgnoreProperties(value = {"addressSavedBy"})
    @Builder.Default
    private Set<Address> savedAddresses = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "user", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"user"})
    @Builder.Default
    private Set<RateMenu> rateMenus = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "eater", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"eater"})
    @Builder.Default
    private Set<RateDriver> ratedEaters = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "driver", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"driver"})
    @Builder.Default
    private Set<RateDriver> ratedDrivers = new HashSet<>();



    private void basics() {
        if (this.getDelivery() == null) {
            this.setDelivery(this.getResidence());
        }
    }


    private void auditingAssociations() {
        for (UserCredential userCredential : this.getUserCredentials()) {
            userCredential.setUser(this);
        }
    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.auditingAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.auditingAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }

}
