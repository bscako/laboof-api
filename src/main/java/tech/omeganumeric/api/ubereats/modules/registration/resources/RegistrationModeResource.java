package tech.omeganumeric.api.ubereats.modules.registration.resources;

public interface RegistrationModeResource {
    RegistrationUserResource userResource();
    RegistrationUserOtpResource otpResource();
}
