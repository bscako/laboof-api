package tech.omeganumeric.api.ubereats.modules.registration.otp;


public interface SenderService<T extends SenderData> {

    boolean send(T data);

}
