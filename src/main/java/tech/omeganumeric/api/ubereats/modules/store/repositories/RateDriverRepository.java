package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.RateDriver;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.RateDriverId;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = RateDriverRepository.PATH,
        path = RateDriverRepository.PATH,
        exported = false
)
public interface RateDriverRepository extends MetaRepository<RateDriver, RateDriverId> {
    String PATH = "rates_drivers";

    String SELECTION = "SELECT l from RateDriver l " +
            "left join fetch  l.eater " +
            "left join fetch  l.driver " +
            "";

    @Query("SELECT l from RateDriver l " +
            "left join fetch  l.eater " +
            "left join fetch  l.driver " +
            ""
    )
    @Override
    List<RateDriver> findAll();


    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends RateDriver> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    <S extends RateDriver> Optional<S> findOne(Example<S> example);

    @Override
    @RestResource(exported = false)
    List<RateDriver> findAllById(Iterable<RateDriverId> iterable);
}
