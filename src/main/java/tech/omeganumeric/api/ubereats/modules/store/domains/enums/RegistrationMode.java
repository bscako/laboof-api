package tech.omeganumeric.api.ubereats.modules.store.domains.enums;

public enum RegistrationMode {
    OTP,
    SOCIAL,
    DEFAULT,
}
