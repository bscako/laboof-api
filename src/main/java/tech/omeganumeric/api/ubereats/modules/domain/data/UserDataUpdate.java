package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.hibernate.cfg.NotYetImplementedException;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserIdDto;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class UserDataUpdate
        <Dto, Information, Credential, Phone>
        extends ValidatorDataInterfaces<Dto>
{

    //    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private UserIdDataAbstract<UserIdDto, Phone> identifier;

    //    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private Information details;

    @JsonIgnore
//    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private Credential credential;


    @Builder(builderMethodName = "UserData")
    public UserDataUpdate(
            UserIdDataAbstract<UserIdDto, Phone> identifier,
            Information details,
            Credential credential
    ) {
        this.identifier = identifier;
        this.details = details;
        this.credential = credential;
    }

    @Override
    public Dto toDto() {
        throw new NotYetImplementedException();
    }
}
