package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.UserDateAudit;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.RateDriverId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"eater", "driver"})
@ToString(callSuper = true, exclude = {"eater", "driver"})
@Table(name = RateDriver.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = {"eater_id", "driver_id"},
                        name = "uk_rate_driver_ids")
        }

)
public class RateDriver extends UserDateAudit {

    public static final String FIELD_ENTITY = "Rate_Driver";
    public static final String FIELD_ENTITY_TABLE_NAME = "Rates_Drivers";
    private static final long serialVersionUID = 7691304461650702232L;

    @EmbeddedId
    private RateDriverId id;

    @Column(nullable = false)
    @NotNull
    @Size(min = 1, max = 5)
    private int rate;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("eaterId")
    @JsonIgnoreProperties(value = {"ratedEaters"})
    @NotNull
    private User eater;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("driverId")
    @JsonIgnoreProperties(value = {"ratedDrivers"})
    @NotNull
    private User driver;


    public void updateAssociations() {
    }

    private void basics() {

    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.updateAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.updateAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }

}
