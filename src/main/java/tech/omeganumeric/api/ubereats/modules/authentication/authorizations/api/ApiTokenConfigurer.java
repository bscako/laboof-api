package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenConfigurer;

@Slf4j
public class ApiTokenConfigurer extends TokenConfigurer<ApiTokenService, ApiTokenProvider> {


    public ApiTokenConfigurer(ApiTokenService service, ApiTokenProvider provider) {
        super(service, provider);
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        ApiTokenFilter filter = new ApiTokenFilter(
                getService(),
                getProvider()
        );
        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    }
}
