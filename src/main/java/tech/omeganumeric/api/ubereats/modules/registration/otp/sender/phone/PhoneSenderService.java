package tech.omeganumeric.api.ubereats.modules.registration.otp.sender.phone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.registration.otp.SenderService;
import tech.omeganumeric.api.ubereats.modules.registration.otp.sender.mail.SenderDataEmail;

import java.util.stream.Collectors;

@Service
public class PhoneSenderService implements SenderService<SenderDataEmail> {

    private final Logger LOGGER = LoggerFactory.getLogger(SenderService.class);

    private final JavaMailSender emailSender;

    public PhoneSenderService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    /**
     * Method for sending simple e-mail message.
     *
     * @param data
     * @return
     */
    @Override
    public boolean send(SenderDataEmail data) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(data.getRecipients().stream().collect(Collectors.joining(",")));
        mailMessage.setSubject(data.getSubject());
        mailMessage.setText(data.getBody());

        boolean isSent = false;
        try {
            emailSender.send(mailMessage);
            isSent = true;
        } catch (Exception e) {
            LOGGER.error("Sending e-mail error: {}", e.getMessage());
        }
        return isSent;
    }
}
