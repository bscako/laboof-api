package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.MediaData;
import tech.omeganumeric.api.ubereats.modules.store.entities.Media;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MediaDto extends MediaData {

    @JsonIgnore
    @Null
    private Long id;


    @Builder(builderMethodName = "MediaDto")
    public MediaDto(@NotNull String name, @NotNull String type, byte[] resource, boolean avatar, @Null Long id) {
        super(name, type, resource, avatar);
        this.id = id;
    }

    public static MediaDto of(Media media) {
        return Optional.ofNullable(media)
                .map(media1 -> MediaDto.MediaDto()
                        .id(media.getId())
                        .name(media.getName())
                        .type(media.getType())
                        .resource(media.resource())
                        .avatar(media.isAvatar())
                        .build()
                ).orElse(null);
    }

    public static MediaDto of(MediaData media) {
        return Optional.ofNullable(media)
                .map(mediaData -> MediaDto.MediaDto()
                        .type(media.getType())
                        .resource(media.getResource())
                        .avatar(media.isAvatar())
                        .build()
                ).orElse(null);
    }

    public Media map(Media media) {
        media.setName(
                Optional.ofNullable(this.getName()).orElse(media.getName())
        );
        media.setType(
                Optional.ofNullable(this.getType()).orElse(media.getType())
        );
        media.setFile(
                Optional.ofNullable(this.getResource()).orElse(media.getFile())
        );

        media.setAvatar(
//                Optional.ofNullable().orElse(media.isAvatar())
                this.isAvatar()
        );
        return media;
    }

    interface New {

    }

    interface Exist {

    }

    interface Update extends Exist {

    }

    interface Details {

    }

    interface AdminDetails extends Details {

    }
}
