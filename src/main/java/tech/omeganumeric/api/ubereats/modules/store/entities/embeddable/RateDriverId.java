package tech.omeganumeric.api.ubereats.modules.store.entities.embeddable;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class RateDriverId implements Serializable {

    @Column(name = "eater_id")
    @NotNull
    private Long eaterId;

    @Column(name = "driver_id")
    @NotNull
    private Long driverId;

}
