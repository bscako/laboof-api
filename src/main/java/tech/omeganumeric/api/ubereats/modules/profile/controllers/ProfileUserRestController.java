package tech.omeganumeric.api.ubereats.modules.profile.controllers;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.omeganumeric.api.ubereats.configs.AppConfig;
import tech.omeganumeric.api.ubereats.controllers.AppRestController;
import tech.omeganumeric.api.ubereats.exceptions.ControllerException;
import tech.omeganumeric.api.ubereats.modules.profile.domains.ProfileUserUpdateDataRequest;
import tech.omeganumeric.api.ubereats.modules.profile.resources.ProfileUserResource;
import tech.omeganumeric.api.ubereats.modules.profile.services.ProfileUserService;

@RestController
//@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = ProfileUserRestController.PATH)
@Api(
        value = ProfileUserRestController.PATH,
        tags = {AppRestController.TAG, ProfileUserRestController.TAG},
        description = "All services relating to user profile"
)
public class ProfileUserRestController extends ControllerException {

    public static final String PATH = AppConfig.BASE_PATH + "user/profile";
    public static final String TAG = "User Profile";

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private final ProfileUserService profileUserService;

    public ProfileUserRestController(ProfileUserService profileUserService) {
        this.profileUserService = profileUserService;
    }

    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity accountUser(
            @RequestParam final String identifier,
            @RequestParam final String applicationName,
            @RequestParam final String roleName
    ) {
        log.debug("Retrieving profile for identifier {} in application {} with role {}",
                identifier, applicationName, roleName);
        ProfileUserResource resource = this.profileUserService
                .retrieveUserProfile(identifier, applicationName, roleName);
        return ResponseEntity.ok(resource);
    }

    @RequestMapping(
            method = RequestMethod.PUT
    )
    public ResponseEntity updateUserProfile(
            @RequestParam final String identifier,
            @RequestParam final String applicationName,
            @RequestParam final String roleName,
            @RequestBody ProfileUserUpdateDataRequest dataRequest
    ) {
        log.info("Updating profile for user with identifier {} in application {} with role {}",
                identifier, applicationName, roleName);
        ProfileUserResource resource = this.profileUserService
                .updateUserProfile(identifier, applicationName, roleName, dataRequest);
        return ResponseEntity.ok(resource);
    }

    @RequestMapping(
            method = RequestMethod.DELETE
    )
    public ResponseEntity deleteUserProfile(
            @RequestParam final String identifier,
            @RequestParam final String applicationName,
            @RequestParam final String roleName
    ) {
        log.debug("Deleting profile for user with identifier {} in application {} with role {}",
                identifier, applicationName, roleName);
        boolean deleted = this.profileUserService
                .deleteUserProfile(identifier, applicationName, roleName);
        return ResponseEntity.ok(deleted);
    }
}
