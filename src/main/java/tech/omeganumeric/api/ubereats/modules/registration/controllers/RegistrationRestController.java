package tech.omeganumeric.api.ubereats.modules.registration.controllers;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.omeganumeric.api.ubereats.configs.AppConfig;
import tech.omeganumeric.api.ubereats.controllers.AppRestController;
import tech.omeganumeric.api.ubereats.exceptions.ControllerException;
import tech.omeganumeric.api.ubereats.modules.registration.domains.RegistrationRequest;
import tech.omeganumeric.api.ubereats.modules.registration.domains.RegistrationUserDataRequest;
import tech.omeganumeric.api.ubereats.modules.registration.resources.RegistrationResource;
import tech.omeganumeric.api.ubereats.modules.registration.services.RegistrationService;
import tech.omeganumeric.api.ubereats.modules.store.domains.enums.RegistrationMode;

import javax.transaction.NotSupportedException;
import java.net.URI;

@RestController
//@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = RegistrationRestController.PATH)
@Api(
        value = RegistrationRestController.PATH,
        tags = {AppRestController.TAG, RegistrationRestController.TAG},
        description = RegistrationRestController.DESCRIPTION
)
public class RegistrationRestController extends ControllerException {

    public static final String PATH = AppConfig.BASE_PATH + "user/register";
    public static final String TAG = "Registration";
    public static final String DESCRIPTION = " All services relating to registration";
    public static final String SOCIAL = "social";
    public static final String OTP = "otp";

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final RegistrationService registrationService;

    public RegistrationRestController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<?> register(
            @RequestBody RegistrationRequest registrationRequest
    ) throws NotSupportedException {
        log.debug("registering user : mode {} request data {}",
                registrationRequest.getMode(),
                registrationRequest.getUser()
        );
        RegistrationResource registrationResource = this.registrationService.register(
                registrationRequest
        );
        log.debug("registered {}", registrationResource);
        final URI uri = ServletUriComponentsBuilder.fromCurrentRequest().build().toUri();
        return ResponseEntity.created(uri).body(registrationResource);
    }

    @RequestMapping(
            value = SOCIAL,
            method = RequestMethod.POST
    )
    public ResponseEntity<?> registerSocial(
            @RequestBody RegistrationUserDataRequest registrationUserDataRequest
    ) throws NotSupportedException {
        log.debug("registering social user {}", registrationUserDataRequest);
        RegistrationRequest registrationRequest = RegistrationRequest.RegistrationRequest()
                .user(registrationUserDataRequest)
                .mode(RegistrationMode.SOCIAL)
                .build();
        return this.register(registrationRequest);
    }


    @RequestMapping(
            value = OTP,
            method = RequestMethod.POST
    )
    public ResponseEntity registerOtp(
            @RequestBody RegistrationUserDataRequest registrationUserDataRequest
    ) throws NotSupportedException {
        log.debug("registering otp user {}", registrationUserDataRequest);
        RegistrationRequest registrationRequest = RegistrationRequest.RegistrationRequest()
                .user(registrationUserDataRequest)
                .mode(RegistrationMode.OTP)
                .build();
        return this.register(registrationRequest);
    }


}
