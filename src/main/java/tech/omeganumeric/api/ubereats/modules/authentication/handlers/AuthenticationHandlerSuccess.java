package tech.omeganumeric.api.ubereats.modules.authentication.handlers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.services.StoreService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;

@Slf4j
@Component
public class AuthenticationHandlerSuccess implements AuthenticationSuccessHandler {


    private final StoreService storeService;
    private final UserEncoder usernameEncoder;

    public AuthenticationHandlerSuccess(
            StoreService storeService,
            UserEncoder usernameEncoder) {
        this.storeService = storeService;
        this.usernameEncoder = usernameEncoder;
    }

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            Authentication authentication
    ) {
        log.info("success logged");
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = this.usernameEncoder.decodeUser(userDetails.getUsername());
        Application application = this.usernameEncoder.decodeApplication(userDetails.getUsername());
        Role role = this.usernameEncoder.decodeRole(userDetails.getUsername());
        this.storeService.updateLastLoginOn(user, application, role, Instant.now());
    }
}
