package tech.omeganumeric.api.ubereats.modules.profile.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.profile.domains.ProfileUserUpdateDataRequest;
import tech.omeganumeric.api.ubereats.modules.profile.resources.ProfileUserResource;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;
import tech.omeganumeric.api.ubereats.modules.store.repositories.*;

import javax.persistence.EntityNotFoundException;

@Service
public class ProfileUserService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final UserRepository userRepository;
    private final ApplicationRepository applicationRepository;
    private final RoleRepository roleRepository;
    private final UserCredentialRepository userCredentialRepository;
    private final UserInformationRepository userInformationRepository;
    private final PhoneRepository phoneRepository;
    private final RepositoryEntityLinks repositoryEntityLinks;

    public ProfileUserService(
            UserRepository userRepository, ApplicationRepository applicationRepository,
            RoleRepository roleRepository, UserCredentialRepository userCredentialRepository,
            UserInformationRepository userInformationRepository, PhoneRepository phoneRepository,
            RepositoryEntityLinks repositoryEntityLinks
    ) {
        this.userRepository = userRepository;
        this.applicationRepository = applicationRepository;
        this.roleRepository = roleRepository;
        this.userCredentialRepository = userCredentialRepository;
        this.userInformationRepository = userInformationRepository;
        this.phoneRepository = phoneRepository;
        this.repositoryEntityLinks = repositoryEntityLinks;
    }


    public ProfileUserResource retrieveUserProfile(
            final String identifier,
            final String applicationName,
            final String roleName
    ) {
        User user = this.findUserByIdentifier(identifier);
        Application application = this.findApplicationByName(applicationName);
        Role role = this.findRoleByName(roleName);

        validateUserApplicationRole(user, application, role);

        return ProfileUserResource.ProfileUserResource()
                .user(user)
                .application(application)
                .role(role)
                .repositoryEntityLinks(this.repositoryEntityLinks)
                .build();
    }

    public ProfileUserResource updateUserProfile(
            final String identifier,
            final String applicationName,
            final String roleName,
            final ProfileUserUpdateDataRequest profileUserUpdateDataRequest
            ) {
        log.info("user update {} {}",
                profileUserUpdateDataRequest.getDetails(),
                profileUserUpdateDataRequest.getCredential()
        );

        User user = this.findUserByIdentifier(identifier);
        Application application = this.findApplicationByName(applicationName);
        Role role = this.findRoleByName(roleName);
        validateUserApplicationRole(user, application, role);
        UserDto updateData = profileUserUpdateDataRequest.toDto();
        user = updateData.map(user, application, role);
        user = this.userRepository.save(user);

        return ProfileUserResource.ProfileUserResource()
                .user(user)
                .application(application)
                .role(role)
                .repositoryEntityLinks(this.repositoryEntityLinks)
                .build();
    }

    public boolean deleteUserProfile(
            final String identifier,
            final String applicationName,
            final String roleName
    ) {
        User user = this.findUserByIdentifier(identifier);
        Application application = this.findApplicationByName(applicationName);
        Role role = this.findRoleByName(roleName);
        validateUserApplicationRole(user, application, role);
        UserCredential userCredential = this.userCredentialOf(user,application,role);
        if(userCredential == null) {
            return false;
        }
        if(user.getUserCredentials().size() == 1 ) {
            this.userRepository.delete(user);
        } else {
            this.userCredentialRepository.delete(userCredential);
        }
        return true;
    }

    private UserCredential userCredentialOf(User user, Application application, Role role) {

        return user.getUserCredentials().stream()
                .filter(userCredential -> userCredential.getRole()
                        .getName().equalsIgnoreCase(role.getName()))
                .filter(userCredential -> userCredential.getApplication()
                        .getName().equalsIgnoreCase(application.getName()))
                .findFirst()
                .orElse(null)
                ;

    }

    private void validateUserApplicationRole(
            User user,
            Application application,
            Role role
    ) {
        if (!
                user.getUserCredentials().stream()
                        .filter(userCredential -> userCredential.getRole()
                                .getName().equalsIgnoreCase(role.getName()))
                        .anyMatch(userCredential -> userCredential.getApplication()
                                .getName().equalsIgnoreCase(application.getName()))
        ) {
            log.warn("User Profile: EntityNotFoundException User for  {} {} {} ",
                    user, application, role
            );
            throw  new EntityNotFoundException(
                    String.format(
                            "User Profile: EntityNotFoundException User for  %s %s %s ",
                            user.toString(),
                            application.toString(),
                            role.toString()
                    )
            );
        }
    }


    private User findUserByIdentifier(String identifier) {
        return userRepository.findByUniqueKey(
                identifier
        ).orElseThrow(
                () -> {
                    log.warn("User Profile: EntityNotFoundException for user  {} ",
                            identifier
                    );
                    return new EntityNotFoundException(
                            String.format(
                                    "User Profile: EntityNotFoundException for user %s ",
                                    identifier
                            )
                    );
                }
        );
    }

    private User findUserByIdentifier(String login, String email, String phoneNumber) {
        return userRepository.findByUniqueKey(
                login, email, phoneNumber
        ).orElseThrow(
                () -> {
                    log.warn("User Profile: EntityNotFoundException for user  {} {} {} ",
                            login, email, phoneNumber
                    );
                    return new EntityNotFoundException(
                            String.format(
                                    "User Profile: EntityNotFoundException for user %s %s %s ",
                                    login, email, phoneNumber
                            )
                    );
                }
        );
    }

    private Application findApplicationByName(String applicationName) {
        return applicationRepository.findByName(applicationName)
                .orElseThrow(() -> {
                    log.warn("User Profile: EntityNotFoundException for application  {} ",
                            applicationName
                    );
                    return new EntityNotFoundException(
                            String.format(
                                    "User Profile: EntityNotFoundException for application %s ",
                                    applicationName
                            )
                    );
                });
    }

    private Role findRoleByName(String roleName) {
        return roleRepository.findByName(roleName)
                .orElseThrow(() -> {
                    log.warn("User Profile: EntityNotFoundException for role  {} ",
                            roleName
                    );
                    return new EntityNotFoundException(
                            String.format(
                                    "User Profile: EntityNotFoundException for role %s ",
                                    roleName
                            )
                    );
                });
    }
}
