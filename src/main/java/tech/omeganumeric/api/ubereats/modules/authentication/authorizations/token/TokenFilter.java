package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.web.filter.OncePerRequestFilter;


@Data
@EqualsAndHashCode(callSuper = true)
public abstract class TokenFilter<Service extends TokenService, Provider extends TokenProvider>
        extends OncePerRequestFilter {

    private final Service service;
    private final Provider provider;
    private String token = null;


    protected TokenFilter(Service service, Provider provider) {
        this.service = service;
        this.provider = provider;
    }

    public void throwException(String token, String message, Exception e) {
        throw new TokenException(
                String.format(
                        "Security exception for unknown user - %s Token %s",
                        message, token),
                e);
    }

}
