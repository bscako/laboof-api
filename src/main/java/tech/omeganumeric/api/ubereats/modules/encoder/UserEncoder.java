/**
 *
 */
package tech.omeganumeric.api.ubereats.modules.encoder;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tech.omeganumeric.api.ubereats.modules.authentication.configs.properties.AuthenticationProperties;
import tech.omeganumeric.api.ubereats.modules.domain.data.PhoneData;
import tech.omeganumeric.api.ubereats.modules.domain.data.UserIdDataAbstract;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Phone;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.RoleRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author BSCAKO
 *
 */

@Slf4j
@Component
public class UserEncoder {

    private final AuthenticationProperties.TokenProperties.UserTokenProperties userTokenProperties;
    private final UserRepository userRepository;
    private final ApplicationRepository applicationRepository;
    private final RoleRepository roleRepository;

    public UserEncoder(
            AuthenticationProperties authenticationProperties,
            UserRepository userRepository,
            ApplicationRepository applicationRepository,
            RoleRepository roleRepository) {
        this.userTokenProperties = authenticationProperties.getToken().getUser();
        this.userRepository = userRepository;
        this.applicationRepository = applicationRepository;
        this.roleRepository = roleRepository;
    }

    public boolean validate(String encodedValue) {
        return (encodedValue != null)
                && (encodedValue.split(this.userTokenProperties.getEncoder().getDelimiter()).length == 5);
    }

    public String encode(UserDto user) {
        if ((user == null) || (user.getApplication() == null) || user.getRole() == null) {
            return null;
        }
        List<String> elements = Arrays.asList(
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getLogin)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getEmail)
                        .orElse(null),
                Optional.ofNullable(user.getIdentifier())
                        .map(UserIdDataAbstract::getPhone)
                        .map(PhoneData::getNumber)
                        .orElse(null),
                user.getApplication().getName(),
                user.getRole().getName()
        );

        return String.join(this.userTokenProperties.getEncoder().getDelimiter(), elements);
    }


    public String encode(User user, Application application, Role role) {
        if ((user == null) || (application == null) || role == null) {
            return null;
        }
        List<String> elements = Arrays.asList(
                user.getLogin(),
                user.getEmail(),
                user.getPhone().getNumber(),
                application.getName(),
                role.getName()
        );

        return String.join(this.userTokenProperties.getEncoder().getDelimiter(), elements);
    }

    public User decodeUser(String encodedValue) {
        if (this.validate(encodedValue)) {
            User user = User.builder()
                    .login(this.decodeUsername(encodedValue).get(0))
                    .email(this.decodeUsername(encodedValue).get(1))
                    .phone(Phone.builder().number(this.decodeUsername(encodedValue).get(2)).build())
                    .build();
            user = this.userRepository.findByUniqueKey(
                    user.getLogin(),
                    user.getEmail(),
                    user.getPhone().getNumber()
            ).orElseThrow(EntityNotFoundException::new);
            return user;
        }
        return null;
    }

    public Application decodeApplication(String encodedValue) {
        if (this.validate(encodedValue)) {
            Application application = Application.builder()
                    .name(this.decodeUsername(encodedValue).get(3))
                    .build();
            application = this.applicationRepository.findByName(application.getName())
                    .orElseThrow(EntityNotFoundException::new);
            return application;
        }
        return null;
    }

    public Role decodeRole(String encodedValue) {
        if (this.validate(encodedValue)) {
            return Optional.ofNullable(
                    this.decodeUsername(encodedValue).get(4)
            ).map(s -> this.roleRepository.findByName(s)
                    .orElseThrow(EntityNotFoundException::new)
            ).orElse(null);
        }
        return null;
    }

    private List<String> decodeUsername(String encodedValue) {
        if (validate(encodedValue)) {
            return Stream.of(encodedValue
                    .split(this.userTokenProperties.getEncoder().getDelimiter()))
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

}
