package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.IdUserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@RestResource
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"userCredentials"})
@ToString(callSuper = true, exclude = {"userCredentials"})
@Table(
        name = Application.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = "name",
                        name = "uk_application_name")
        }
)
public class Application extends IdUserDateAudit {

    public static final String FIELD_ENTITY = "application";
    public static final String FIELD_ENTITY_TABLE_NAME = "applications";

    public static final String FIELD_NAME = "name";
    public static final String FIELD_TYPE = "type";

    @Column(unique = true, nullable = false)
    @NotNull
    private String name;

    @Column
    private String type;

    @Column
    private String description;

    @OneToMany(mappedBy = "application", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = {"application"})
    @Builder.Default
    private Set<UserCredential> userCredentials = new HashSet<>();

    private void basics() {
        this.setName(this.getName().toUpperCase());
        if (this.getType() != null) {
            this.setType(this.getType().toUpperCase());
        }
    }

    private void auditingAssociations() {
        for (UserCredential userCredential : this.getUserCredentials()) {
            userCredential.setApplication(this);
        }
    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.auditingAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.auditingAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }

}
