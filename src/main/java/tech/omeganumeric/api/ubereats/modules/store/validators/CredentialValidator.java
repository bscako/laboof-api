package tech.omeganumeric.api.ubereats.modules.store.validators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import tech.omeganumeric.api.ubereats.modules.store.configs.StoreConfig;
import tech.omeganumeric.api.ubereats.modules.store.domains.enums.RegistrationMode;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;
import tech.omeganumeric.api.ubereats.modules.store.exceptions.PasswordException;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserCredentialRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.Instant;

public class CredentialValidator implements
        ConstraintValidator<ValidCredential, UserCredential> {


    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final ApplicationContext beanRepository =  StoreConfig.contextProvider().getContext();
    private final UserCredentialRepository repository = (UserCredentialRepository) beanRepository
            .getBean("userCredentialRepository");
    private final PasswordEncoder passwordEncoder = (PasswordEncoder) beanRepository
            .getBean("passwordEncoderBean");

    @Override
    public void initialize(ValidCredential constraintAnnotation) {

    }

    @Override
    public boolean isValid(UserCredential value, ConstraintValidatorContext context) {
        UserCredential userCredential = repository.findById(value.getId()).orElse(null);

        if(userCredential == null
                && value.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                && value.getPassword() != null
        ) {
            log.info("user with registration %s mode can not have password");
            throw new PasswordException(
                    String.format(
                            "user with registration %s mode can not have password"
                    )
            );
        }

        if(userCredential == null
                && !value.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                && value.getPassword() == null
        ) {
            throw new PasswordException(
                    String.format(
                            "user with registration %s mode must have password"
                    )
            );
        }

        if(userCredential == null
                && !value.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                && value.getPassword() != null
        ) {
            value.setPassword(passwordEncoder.encode(value.getPassword()));
        }

        if(userCredential != null
                && value.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                && value.getPassword() != null
        ) {
            throw new PasswordException(
                    String.format(
                            "user registered with %s mode can not have password",
                            userCredential.getRegistrationMode()
                    )
            );
        }

        if(userCredential != null
                && !value.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                && value.getPassword() == null
        ) {
            throw new PasswordException(
                    String.format(
                            "user with registration %s mode must have password"
                    )
            );
        }

        if(userCredential != null
                && !value.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                && value.getPassword() != null
        ) {
            log.info(
                    "user update credential with password {} {}",
                    value.getPassword(),
                    userCredential.getPassword()
            );
            if(passwordEncoder.matches(value.getPassword(), userCredential.getPassword())) {
                log.info("user update credential with password: same password");
                throw new PasswordException(
                        String.format(
                                "Same password"
                        )
                );
            } else {
                log.info("user update credential with password new password");
                value.setPassword(passwordEncoder.encode(value.getPassword()));
                value.setLastResetPassword(Instant.now());
            }
        }
        return false;
    }





}

