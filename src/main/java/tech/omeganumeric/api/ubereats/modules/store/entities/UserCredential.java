package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import tech.omeganumeric.api.ubereats.modules.store.configs.StoreConfig;
import tech.omeganumeric.api.ubereats.modules.store.domains.enums.RegistrationMode;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.UserDateAudit;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.UserCredentialId;
import tech.omeganumeric.api.ubereats.modules.store.exceptions.PasswordException;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.Optional;


@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@RestResource
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder(buildMethodName = "buildInternal")
@EqualsAndHashCode(callSuper = true, exclude = {"user", "previousState"})
@ToString(callSuper = true, exclude = {"user", "role", "application", "previousState"})
@Table(name = UserCredential.FIELD_ENTITY_TABLE_NAME)
//@ValidCredential
public class UserCredential extends UserDateAudit {

    public static final String FIELD_ENTITY = "user_credential";
    public static final String FIELD_ENTITY_TABLE_NAME = "users_credentials";

    @JsonIgnore
    private transient final Logger log = LoggerFactory.getLogger(this.getClass());

    @EmbeddedId
    private UserCredentialId id;

    @MapsId("userId")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = {"userCredentials"})
    @NotNull
    private User user;

    @MapsId("roleId")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = {"userCredentials"})
    @NotNull
    private Role role;

    @MapsId("applicationId")
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = {"userCredentials"})
    @NotNull
    private Application application;

    @Column
    private String password;

    @Enumerated(EnumType.STRING)
    @Column(updatable = false)
    @Builder.Default
    private RegistrationMode registrationMode = RegistrationMode.DEFAULT;

    @Column(nullable = false)
    @Builder.Default
    private boolean enabled = false;

    @Column
    @Builder.Default
    private Instant lastResetPassword = Instant.now();

    @Column
    private Instant lastLoginOn;

    @JsonIgnore
    private transient UserCredential previousState;

    private void basics() {
        this.auditingPassword();
    }

    private void validate(UserCredential userCredential, PasswordEncoder passwordEncoder) {

        if (userCredential == null
                && this.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                && this.getPassword() != null
        ) {
            throw new PasswordException(
                    "user with registration %s mode can not have password"
            );
        }

        if (
                userCredential == null
                        && !this.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                        && this.getPassword() == null
        ) {
            throw new PasswordException(
                    "user with registration %s mode must have password"
            );
        }

        if (
                userCredential != null
                        && this.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                        && this.getPassword() != null
        ) {
            throw new PasswordException(
                    String.format(
                            "user registered with %s mode can not have password",
                            userCredential.getRegistrationMode()
                    )
            );
        }
        if (
                userCredential != null
                        && !this.getRegistrationMode().equals(RegistrationMode.SOCIAL)
                        && this.getPassword() != null
                        && passwordEncoder.matches(this.getPassword(), userCredential.getPassword())

        ) {
            throw new PasswordException(
                    "Same password"
            );
        }


    }

    private void auditingPassword() {
        ApplicationContext beanRepository = StoreConfig.contextProvider().getContext();
        PasswordEncoder passwordEncoder = (PasswordEncoder) beanRepository
                .getBean("passwordEncoderBean");
        this.validate(previousState, passwordEncoder);

        if (previousState == null) {
            // new credential
            if (!this.getRegistrationMode().equals(RegistrationMode.SOCIAL)) {
                this.setPassword(passwordEncoder.encode(this.getPassword()));
            }
        } else {
            // update credential
            log.info(
                    "update credential auditing mode {} new password raw {} credential {}",
                    this.getRegistrationMode(),
                    this.getPassword(),
                    previousState.getPassword()
            );
            if (!this.getRegistrationMode().equals(RegistrationMode.SOCIAL)) {
                // not social user
                if (this.getPassword() == null) {
                    // no change password
                    this.setPassword(previousState.getPassword());
                } else {
                    if (!passwordEncoder.matches(this.getPassword(), previousState.getPassword())) {
                        // not same password
                        this.setPassword(passwordEncoder.encode(this.getPassword()));
                        this.setLastResetPassword(Instant.now());
                    }
                    // change password
                }


            }

        }

        log.info("auditing mode {} password {}", this.getRegistrationMode(), this.getPassword());
    }

    private void auditingAssociations() {

    }

    @PostLoad
    private void savePreviousState() {
        this.previousState = SerializationUtils.clone(this); // from apache commons-lang
    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.auditingAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.auditingAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }

    private void initialization() {
        if (this.getId() == null) {
            this.setId(new UserCredentialId());
        }
        if (this.getApplication() != null) {
            this.getId().setApplicationId(this.getApplication().getId());
        }

        if (this.getRole() != null) {
            this.getId().setRoleId(this.getRole().getId());
        }

        if (this.getUser() != null) {
            this.getId().setUserId(this.getUser().getId());
        }

    }


    public static class UserCredentialBuilder {
        public Optional<UserCredential> buildOptional() {
            return Optional.of(this.build());
        }

        public UserCredential build() {
            UserCredential userCredential = this.buildInternal();
            userCredential.initialization();
            return userCredential;
        }
    }


}
