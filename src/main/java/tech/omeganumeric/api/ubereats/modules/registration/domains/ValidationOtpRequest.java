package tech.omeganumeric.api.ubereats.modules.registration.domains;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.omeganumeric.api.ubereats.modules.validators.RequestValidatorInterfaces;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
public class ValidationOtpRequest extends RequestValidatorInterfaces implements Serializable {

    @NotNull
    private RegistrationUserDataOtpRequest user;

    @NotNull
    private String otp;
}
