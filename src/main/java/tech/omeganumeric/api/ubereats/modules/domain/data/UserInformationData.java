package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.MediaDto;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserInformationDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserInformation;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UserInformationData extends ValidatorDataInterfaces<UserInformationDto> {

    @NotNull
    @JsonView({Details.class})
    private String name;

    @JsonView({Details.class})
    private String firstname;

    @JsonView({Details.class})
    private String lastname;

    @JsonView({Details.class})
    private MediaData avatar;

    public static UserInformationData of(UserInformation details) {
        return UserInformationData.builder()
                .name(details.getName())
                .firstname(details.getFirstname())
                .lastname(details.getLastname())
                .avatar(MediaData.of(details.getAvatar()))
                .build();
    }

    public static UserInformationData of(UserInformationDto details) {
        return UserInformationData.builder()
                .name(details.getName())
                .firstname(details.getFirstname())
                .lastname(details.getLastname())
                .avatar(MediaDto.of(details.getAvatar()))
                .build();
    }

    @Override
    public UserInformationDto toDto() {
        return UserInformationDto.of(this);
    }
}
