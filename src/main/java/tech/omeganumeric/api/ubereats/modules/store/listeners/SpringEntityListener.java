package tech.omeganumeric.api.ubereats.modules.store.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;

import javax.persistence.PostLoad;
import javax.persistence.PostPersist;

public class SpringEntityListener {

    private static final Logger LOG = LoggerFactory.getLogger(SpringEntityListener.class);

    private static final SpringEntityListener INSTANCE = new SpringEntityListener();

    private volatile AutowireCapableBeanFactory beanFactory;

    public static SpringEntityListener get() {
        return INSTANCE;
    }
    public AutowireCapableBeanFactory getBeanFactory() {
        return beanFactory;
    }
    public void setBeanFactory(AutowireCapableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @PostLoad
    @PostPersist
    public void inject(Object object) {
        AutowireCapableBeanFactory beanFactory = get().getBeanFactory();
        if(beanFactory == null) {
            LOG.warn("Bean Factory not set! Depdendencies will not be injected into: '{}'", object);
            return;
        }
        LOG.debug("Injecting dependencies into entity: '{}'.", object);
        beanFactory.autowireBean(object);
    }
}
