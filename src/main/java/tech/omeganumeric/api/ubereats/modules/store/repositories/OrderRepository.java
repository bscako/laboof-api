package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.Order;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.OrderId;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = OrderRepository.PATH,
        path = OrderRepository.PATH,
        exported = false
)
public interface OrderRepository extends MetaRepository<Order, OrderId> {
    String PATH = "orders";

    String SELECTION = "SELECT l from Order l " +
            "left join fetch  l.status " +
            "left join fetch  l.payment " +
            "left join fetch  l.restaurant " +
            "left join fetch  l.menu " +
            "left join fetch  l.driver " +
            "left join fetch  l.eater " +
            "";

    @Query("SELECT l from Order l " +
            "left join fetch  l.status " +
            "left join fetch  l.payment " +
            "left join fetch  l.restaurant " +
            "left join fetch  l.menu " +
            "left join fetch  l.driver " +
            "left join fetch  l.eater "
    )
    @Override
    List<Order> findAll();


    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Order> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    <S extends Order> Optional<S> findOne(Example<S> example);

    @Override
    @RestResource(exported = false)
    List<Order> findAllById(Iterable<OrderId> iterable);
}
