package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenConfigurer;

@Slf4j
public class UserTokenConfigurer extends TokenConfigurer<UserTokenService, UserTokenProvider> {


    private final AuthenticationSuccessHandler authenticationSuccessHandler;

    public UserTokenConfigurer(
            UserTokenService service, UserTokenProvider provider,
            AuthenticationSuccessHandler authenticationSuccessHandler
    ) {
        super(service, provider);
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        UserTokenFilter filter = new UserTokenFilter(
                getService(),
                getProvider(), authenticationSuccessHandler);
        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    }
}
