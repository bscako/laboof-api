package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.PhoneDto;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.RoleDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.Phone;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class RoleData extends ValidatorDataInterfaces<RoleDto> {

    @NotNull
    @JsonView({Details.class})
    private String name;

    public static RoleData of(Phone phone) {
        return RoleData.builder()
                .name(phone.getNumber())
                .build();
    }

    public static RoleData of(PhoneDto phone) {
        return RoleData.builder()
                .name(phone.getNumber())
                .build();
    }

    @Override
    public RoleDto toDto() {
        return RoleDto.of(this);
    }
}
