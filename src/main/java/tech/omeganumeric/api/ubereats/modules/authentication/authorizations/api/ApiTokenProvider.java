package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.configs.properties.AuthenticationProperties;

import java.io.Serializable;

@Component
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class ApiTokenProvider extends TokenProvider
        implements Serializable {

    public ApiTokenProvider(AuthenticationProperties properties) {
        super(
                properties.getToken().getApi().getHeader(),
                properties.getToken().getApi().getSecret(),
                properties.getToken().getApi().getExpiration(),
                properties.getToken().getApi().getStarter()
        );
    }


}
