package tech.omeganumeric.api.ubereats.modules.profile.domains;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import tech.omeganumeric.api.ubereats.modules.domain.data.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserIdDto;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString(callSuper = true)
public class ProfileUserUpdateDataRequest
        extends UserDataUpdate<UserDto, UserInformationData, UserCredentialData, PhoneData> {

    public ProfileUserUpdateDataRequest(
            UserIdDataAbstract<UserIdDto, PhoneData> identifier,
            UserInformationData details,
            UserCredentialData userCredentialData
    ) {
        super(identifier, details, userCredentialData);
    }

    @Override
    public UserDto toDto() {
        return UserDto.of(this);
    }
}
