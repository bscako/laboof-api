package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import tech.omeganumeric.api.ubereats.configs.security.SecurityConfig;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenException;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.stream.Collectors;


@Slf4j
public class ApiTokenFilter extends TokenFilter<ApiTokenService, ApiTokenProvider> {


    protected ApiTokenFilter(ApiTokenService service, ApiTokenProvider provider) {
        super(service, provider);
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws ServletException, IOException, TokenException {
        String token = this.getProvider().resolveToken(request);
        log.debug("Filter: Api Token : {} ", token);
        try {
            if (token != null) {
                final UserDetails userDetails = this.getService()
                        .loadUserByUsername(
                                this.getProvider().getUsernameFromToken(token)
                        );
                if (this.getProvider().validateToken(token, userDetails)) {
                    this.setToken(null);
                    throw new TokenException("Invalid API_KEY");
                }
                log.info(String.format(
                        "Authenticated API : %s  with token: %s  ",
                        userDetails.getUsername(),
                        token));
            } else {
                boolean inBlackList = Arrays.stream(SecurityConfig.AUTH_BLACKLIST)
                        .filter((String s) -> s.equalsIgnoreCase(
                                request.getRequestURI().substring(1))
                        )
                        .collect(Collectors.toList()).size() == 1;
                boolean inUserWhiteList = Arrays.stream(SecurityConfig.USER_KEY_WHITELIST)
                        .filter((String s) -> s.equalsIgnoreCase(
                                request.getRequestURI().substring(1))
                        )
                        .collect(Collectors.toList()).size() == 1;
                if (inBlackList || inUserWhiteList) {
                    throw new TokenException("Need token API in header request");
                }
            }
            this.setToken(token);
        } catch (ExpiredJwtException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - Expired token {} - {}", token, e.getMessage());
            super.throwException(token, "Expired APi", e);

        } catch (UsernameNotFoundException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - UsernameNotFound for token {} - {}", token, e.getMessage());
            super.throwException(token, "UsernameNotFound of API", e);
        } catch (BadCredentialsException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - Bad Credentials {} - {} ", token, e.getMessage());
            super.throwException(token, "Bad Credentials of API", e);
        } catch (TokenException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - Bad Token {} - {} ", token, e.getMessage());
            super.throwException(token, "Invalid API", e);
        }
        chain.doFilter(request, response);
    }
}
