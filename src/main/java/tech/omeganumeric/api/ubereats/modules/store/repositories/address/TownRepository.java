package tech.omeganumeric.api.ubereats.modules.store.repositories.address;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.address.Town;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = TownRepository.PATH,
        path = TownRepository.PATH,
        exported = false)
public interface TownRepository extends MetaRepository<Town, Long> {
    String PATH = "towns";

    String SELECTION = "SELECT l from Town l " +
            "left join fetch l.addresses as a " +
            "left join fetch l.district as d " +
            "";

    @Query("SELECT l from Town l " +
            "left join fetch l.addresses as a " +
            "left join fetch l.district as d " +
            ""
    )
    @Override
    List<Town> findAll();


//    @Query(SELECTION + "where (:name is null or lower(l.name) = lower(:name)) " +
//            "")
//    Optional<Town> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Town> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Town> findAllById(Iterable<Long> iterable);
}
