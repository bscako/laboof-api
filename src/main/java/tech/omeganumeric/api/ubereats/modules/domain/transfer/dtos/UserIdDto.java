package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.UserIdData;
import tech.omeganumeric.api.ubereats.modules.domain.data.UserIdDataAbstract;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@ApiModel(value = "UserIdDto")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class UserIdDto extends UserIdDataAbstract<UserIdDto, PhoneDto> {

    @JsonIgnore
    @Null
    private Long id;

    @Builder(builderMethodName = "UserIdDto")
    public UserIdDto(@NotNull String login, @NotNull String email, @NotNull PhoneDto phone, @Null Long id) {
        super(login, email, phone);
        this.id = id;
    }

    public static UserIdDto of(User user) {
        return UserIdDto.UserIdDto()
                .id(user.getId())
                .login(user.getLogin())
                .email(user.getEmail())
                .phone(PhoneDto.of(user.getPhone()))
                .build();
    }


    public static UserIdDto of(UserDto user) {
        return UserIdDto.UserIdDto()
                .login(user.getIdentifier().getLogin())
                .email(user.getIdentifier().getEmail())
                .phone(user.getIdentifier().getPhone().toDto())
                .build();
    }

    public static UserIdDto of(UserIdDto user) {
        return UserIdDto.UserIdDto()
                .login(user.getLogin())
                .email(user.getEmail())
                .phone(user.getPhone().toDto())
                .build();
    }

    public static UserIdDto of(UserIdData user) {
        return UserIdDto.UserIdDto()
                .login(user.getLogin())
                .email(user.getEmail())
                .phone(user.getPhone().toDto())
                .build();
    }

    public static UserIdDto of(UserIdDataAbstract<UserDto, PhoneDto> user) {
        return UserIdDto.UserIdDto()
                .login(user.getLogin())
                .email(user.getEmail())
                .phone(user.getPhone())
                .build();
    }

    @Override
    public UserIdDto toDto() {
        return UserIdDto.of(this);
    }
}
