package tech.omeganumeric.api.ubereats.modules.registration.domains;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tech.omeganumeric.api.ubereats.modules.domain.data.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserIdDto;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class RegistrationUserDataRequest extends UserData<UserDto, PhoneData, UserInformationData,
        ApplicationData, RoleData,
        UserCredentialData> {

    @Builder(builderMethodName = "RegistrationUserDataRequest")
    public RegistrationUserDataRequest(
            UserIdDataAbstract<UserIdDto, PhoneData> identifier,
            UserInformationData details,
            ApplicationData applicationData,
            RoleData roleData,
            UserCredentialData userCredentialData
    ) {
        super(identifier, applicationData, roleData, details, userCredentialData);
    }

    @Override
    public UserDto toDto() {
        return UserDto.of(this);
    }
}
