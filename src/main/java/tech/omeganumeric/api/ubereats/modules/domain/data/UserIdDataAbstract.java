package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.hibernate.cfg.NotYetImplementedException;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder(builderMethodName = "UserIdDataAbstract")
@ToString
public class UserIdDataAbstract<Dto, PhoneType> extends ValidatorDataInterfaces<Dto> {

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private String login;

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private String email;

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private PhoneType phone;

    @Override
    public Dto toDto() {
        throw new NotYetImplementedException();
    }
}
