package tech.omeganumeric.api.ubereats.modules.domain.transfer.converters;

public interface DtoConverter<Dto> {
    Dto toDto();
}
