package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.context.ApplicationContext;
import tech.omeganumeric.api.ubereats.modules.store.configs.StoreConfig;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.IdUserDateAudit;
import tech.omeganumeric.api.ubereats.modules.store.exceptions.StoreException;
import tech.omeganumeric.api.ubereats.modules.store.services.media.AbstractMediaService;
import tech.omeganumeric.api.ubereats.modules.store.services.media.AvatarService;
import tech.omeganumeric.api.ubereats.modules.store.services.media.MediaService;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"menu", "userInformation"})
@ToString(callSuper = true, exclude = {"menu", "userInformation"})
@Table(
        name = Media.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(columnNames = "name", name = "uk_media_name"),
                @UniqueConstraint(columnNames = {"name", "type"}, name = "uk_media_name_type")
        })
public class Media extends IdUserDateAudit {

    public static final String FIELD_ENTITY = "media";
    public static final String FIELD_ENTITY_TABLE_NAME = "medias";

    private static final long serialVersionUID = -6941448217079263647L;

    @OneToOne(mappedBy = "media", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = {"media"})
    @Builder.Default
    Menu menu = null;

    @OneToOne(mappedBy = "avatar", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = {"avatar"})
    @Builder.Default
    UserInformation userInformation = null;

    @Column(nullable = false, unique = true)
    @NotNull
    private String name;

    @Column(nullable = false)
    @NotNull
    private String type;

    @JsonIgnore
//    @NotNull
    private transient byte[] file;

    @Builder.Default
    private transient boolean avatar = false;

    @JsonIgnore
    private transient Media previousState;

    public boolean isAvatar() {
        ApplicationContext beanRepository = StoreConfig.contextProvider().getContext();
        AvatarService avatarService = (AvatarService) beanRepository
                .getBean("avatarService");
        try {
            return avatarService.loadMediaResource(this) != null;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


    private void storeResource() {

        ApplicationContext beanRepository = StoreConfig.contextProvider().getContext();
        MediaService mediaService = (MediaService) beanRepository
                .getBean("mediaService");
        AvatarService avatarService = (AvatarService) beanRepository
                .getBean("avatarService");
        AbstractMediaService service = mediaService;
        if (this.avatar) {
            service = avatarService;
        }

        if (file != null) {

            if (previousState == null) {
                service.storeMediaResource(this.getFile(), this);
            } else if (
                    !previousState.getName().equalsIgnoreCase(this.getName())
            ) {
                service.removeMediaResource(previousState);
                service.storeMediaResource(this.getFile(), this);
            }

        } else {
            try {
                if (service.loadMediaResource(this) == null) {
                    throw new StoreException("No upload file and file does  not exist");
                }
            } catch (IOException e) {
                throw new StoreException(e.getMessage(), e);
            }
        }
    }

    public byte[] resource() {
        ApplicationContext beanRepository = StoreConfig.contextProvider().getContext();
        MediaService mediaService = (MediaService) beanRepository
                .getBean("mediaService");
        AvatarService avatarService = (AvatarService) beanRepository
                .getBean("avatarService");
        AbstractMediaService service = mediaService;
        if (isAvatar()) {
            service = avatarService;
        }
        try {
            return service.loadMediaResource(this);
        } catch (IOException e) {
            throw new StoreException(e.getMessage(), e);
        }
    }

    public void updateAssociations() {
//        if(this.getUserInformation()!=null) {
//            this.getUserInformation().setAvatar(this);
//        }
//        if(this.getMenu()!=null) {
//            this.getMenu().setMedia(this);
//        }
    }

    private void basics() {
        this.storeResource();
    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.updateAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.updateAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }


}
