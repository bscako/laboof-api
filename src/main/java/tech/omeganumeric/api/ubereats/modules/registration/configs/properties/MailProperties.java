package tech.omeganumeric.api.ubereats.modules.registration.configs.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

//@Configuration
@ConfigurationProperties(prefix = "spring.mail")
@Data
public class MailProperties {

    private String host;

    private Integer port;

    private String username;

    private String password;

    private boolean debug;

    private Transport transport;

    @Data
    public static class Transport {

        private String protocol;

        private boolean authentication;

        private StartTls starttls;

        @Data
        public static class StartTls {
            private boolean enabled;
        }
    }


}
