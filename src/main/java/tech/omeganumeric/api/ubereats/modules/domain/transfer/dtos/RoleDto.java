package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.RoleData;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@ApiModel(value = "roleDto")
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto extends RoleData {

    @Null
    @JsonIgnore
    private Long id;


    @Builder(builderMethodName = "RoleDto")
    public RoleDto(Long id, @NotNull String name) {
        super(name);
        this.id = id;
    }

    public static RoleDto of(Role role) {
        return RoleDto.RoleDto()
                .id(role.getId())
                .name(role.getName())
                .build();
    }

    public static RoleDto of(RoleData role) {
        return RoleDto.RoleDto()
                .name(role.getName())
                .build();
    }
}
