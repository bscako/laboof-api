package tech.omeganumeric.api.ubereats.modules.store.entities.embeddable;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode
@ToString
public class UserCredentialId implements Serializable {

    @Column(name = "user_id", unique = true)
    private Long userId;

    @Column(name = "application_id")
    @NotNull
    private Long applicationId;

    @Column(name = "role_id")
    private Long roleId;

}
