package tech.omeganumeric.api.ubereats.modules.authentication.resources;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.ResourceSupport;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserToken;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.RoleRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;


@Value
@EqualsAndHashCode(callSuper = true)
public class AuthenticationUserResource extends ResourceSupport {

    @JsonIgnoreProperties({"id", "credential"})
    private UserDto user;

    private String token;

    public AuthenticationUserResource(
            User user,
            Application application,
            Role role,
            UserEncoder usernameEncoder,
            TokenProvider tokenProvider,
            RepositoryEntityLinks repositoryEntityLinks
    ) {
        this.user = UserDto.of(user, application, role);

        this.token = tokenProvider
                .generateToken(new UserToken(
                        user,
                        application,
                        role,
                        usernameEncoder)
                );
        this.add(repositoryEntityLinks
                .linkToSingleResource(UserRepository.class, user.getId())
                .withSelfRel());
        this.add(repositoryEntityLinks
                .linkToSingleResource(ApplicationRepository.class, application.getId())
                .withSelfRel());
        this.add(repositoryEntityLinks
                .linkToSingleResource(RoleRepository.class, user.getId())
                .withSelfRel());
    }
}
