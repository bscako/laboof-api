package tech.omeganumeric.api.ubereats.modules.store.entities.embeddable;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class OrderId extends RestaurantMenuId {

    @Column(name = "eater_id", nullable = false)
    @NotNull
    private Long eaterId;

    @Column(name = "driver_id")
    @NotNull
    private Long driverId;

    @Column(name = "code", nullable = false)
    private String code;

}
