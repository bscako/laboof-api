package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserInformation;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = UserInformationRepository.PATH,
        path = UserInformationRepository.PATH,
        exported = false)
public interface UserInformationRepository extends MetaRepository<UserInformation, Long> {
    String PATH = "users_informations";

    String SELECTION = "SELECT l from UserInformation l " +
            "left join fetch l.avatar as av " +
            "left join fetch l.user as u " +
            "left join fetch u.phone as p " +
            "left join fetch u.userCredentials as c " +
            "";

    @Query("SELECT l from UserInformation l " +
            "left join fetch l.avatar as av " +
            "left join fetch l.user as u " +
            "left join fetch u.phone as p " +
            "left join fetch u.userCredentials as c " +
            "")
    @Override
    List<UserInformation> findAll();

    @Query(SELECTION + " where lower(u.login) = lower(:userPKeyLogin) " +
            "")
    Optional<UserInformation> findByUserPKeyLogin(@Param("userPKeyLogin") String userPKeyLogin);

    @Query(SELECTION + "where lower(u.email) = lower(:userPKeyEmail) " +
            "")
    Optional<UserInformation> findByUserPKeyEmail(@Param("userPKeyEmail") String userPKeyEmail);


    @Query(SELECTION + " where p.number = :userPKeyPhoneNumber " +
            "")
    Optional<UserInformation> findByUserPKeyPhoneNumber(@Param("userPKeyPhoneNumber") String userPKeyPhoneNumber);


    @Query(SELECTION + " where lower(u.email) = lower(:userPKeyEmail) " +
            "or lower(u.login) = lower(:userPKeyLogin) " +
            "or lower(p.number) = lower(:userPKeyPhoneNumber) " +
            "")
    Optional<UserInformation> findByUserPKeyEmailOrLoginOrPhoneNumber(
            @Param("userPKeyEmail") String userPKeyEmail,
            @Param("userPKeyLogin") String userPKeyLogin,
            @Param("userPKeyPhoneNumber") String userPKeyPhoneNumber
    );

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends UserInformation> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<UserInformation> findAllById(Iterable<Long> iterable);
}
