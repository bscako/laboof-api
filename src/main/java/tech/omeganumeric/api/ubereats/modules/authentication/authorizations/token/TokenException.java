package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token;

public class TokenException extends RuntimeException {

    private static final long serialVersionUID = -2806739563284198752L;

    public TokenException(String message, Throwable cause) {
        super(message, cause);
    }

    public TokenException(String message) {
        super(message);
    }
}
