package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.ApplicationData;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;

import javax.validation.constraints.Null;


@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationDto extends ApplicationData {

    @JsonIgnore
    @Null
    private Long id;

    private String type;

    @Builder(builderMethodName = "ApplicationDto")
    public ApplicationDto(
            Long id,
            String name,
            String type
    ) {
        super(name);
        this.id = id;
        this.type = type;
        super.setName(name);
    }

    public static ApplicationDto of(ApplicationData request) {
        return ApplicationDto.ApplicationDto()
                .name(request.getName())
                .build();
    }

    public static ApplicationDto of(Application application) {
        return ApplicationDto.ApplicationDto()
                .id(application.getId())
                .type(application.getType())
                .name(application.getName())
                .build();
    }

    interface New {

    }

    interface Exist {

    }

    interface Update extends Exist {

    }

    interface Details {

    }

    interface AdminDetails extends Details {

    }


}
