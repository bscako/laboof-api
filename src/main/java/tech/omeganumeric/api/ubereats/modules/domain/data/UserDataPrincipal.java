package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.hibernate.cfg.NotYetImplementedException;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserIdDto;

import javax.validation.constraints.NotNull;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "UserDataPrincipal")
@ToString(callSuper = true)
public class UserDataPrincipal<Dto, Phone, Application, Role> extends ValidatorDataInterfaces<Dto> {

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private UserIdDataAbstract<UserIdDto, Phone> identifier;

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private Application application;

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private Role role;

    @Override
    public Dto toDto() {
        throw new NotYetImplementedException();
    }
}
