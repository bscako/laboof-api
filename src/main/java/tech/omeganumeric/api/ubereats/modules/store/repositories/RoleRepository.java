package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = RoleRepository.PATH,
        path = RoleRepository.PATH,
        exported = false
)
public interface RoleRepository extends MetaRepository<Role, Long> {
    String PATH = "roles";

    String SELECTION = "SELECT l from Role l " +
            "left join fetch l.userCredentials as c " +
            "";

    @Query("SELECT l from Role l " +
            "left join fetch l.userCredentials as c " +
            ""
    )
    @Override
    List<Role> findAll();


    @Query(SELECTION + "where :name is null or  lower(l.name) = lower(:name) " +
            "")
    Optional<Role> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Role> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Role> findAllById(Iterable<Long> iterable);
}
