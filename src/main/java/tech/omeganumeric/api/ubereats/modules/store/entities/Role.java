package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.rest.core.annotation.RestResource;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.IdUserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@RestResource
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"userCredentials"})
@ToString(callSuper = true, exclude = {"userCredentials"})
@Table(
        name = Role.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = "name",
                        name = "uk_role_name")
        }
)
public class Role extends IdUserDateAudit {

    public static final String FIELD_ENTITY = "role";
    public static final String FIELD_ENTITY_TABLE_NAME = "users_roles";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_DESCRIPTION = "description";

    private static final long serialVersionUID = 5049453559169591098L;

    @NaturalId(mutable = true)
    @Column(nullable = false, unique = true)
    @NotNull
    private String name;

    @Column
    private String description;


    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = {"role"})
    @Builder.Default
    private Set<UserCredential> userCredentials = new HashSet<>();

    private void basics() {
        this.name = this.getName().toUpperCase();
    }

    private void auditingAssociations() {
        for (UserCredential userCredential : this.getUserCredentials()) {
            userCredential.setRole(this);
        }
    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.auditingAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.auditingAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }
}
