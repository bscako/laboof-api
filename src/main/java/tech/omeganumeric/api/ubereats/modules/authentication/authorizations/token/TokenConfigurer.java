package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;

@Data
@EqualsAndHashCode(callSuper = true)
public abstract class TokenConfigurer<
        Service extends TokenService,
        Provider extends TokenProvider
        >
        extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    private final Service service;
    private final Provider provider;

    public TokenConfigurer(Service service, Provider provider) {
        this.service = service;
        this.provider = provider;
    }
}
