package tech.omeganumeric.api.ubereats.modules.store.repositories.address;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.address.District;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = DistrictRepository.PATH,
        path = DistrictRepository.PATH,
        exported = false)
public interface DistrictRepository extends MetaRepository<District, Long> {
    String PATH = "districts";

    String SELECTION = "SELECT l from District l " +
            "left join fetch l.department as d " +
            "left join fetch l.towns as t " +
            "";

    @Query("SELECT l from District l " +
            "left join fetch l.department as d " +
            "left join fetch l.towns as t " +
            ""
    )
    @Override
    List<District> findAll();


//    @Query(SELECTION + "where (:name is null or lower(l.name) = lower(:name)) " +
//            "")
//    Optional<District> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends District> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<District> findAllById(Iterable<Long> iterable);
}
