package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.UserCredentialData;
import tech.omeganumeric.api.ubereats.modules.store.domains.enums.RegistrationMode;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Optional;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class UserCredentialDto extends UserCredentialData {

    @NotNull
    @JsonView({Details.class})
    private UserDto user;

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private ApplicationDto application;

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private RoleDto role;

    @Builder(builderMethodName = "UserCredentialDto")
    public UserCredentialDto(
            Date lastResetPassword,
            Date lastLoginOn,
            String password,
            boolean enabled,
            RegistrationMode registeredAs,
            UserDto user,
            ApplicationDto application,
            RoleDto role
    ){
        super(password, lastResetPassword, lastLoginOn, enabled, registeredAs);
        this.user = user;
        this.application = application;
        this.role = role;
    }

    public static UserCredentialDto of(UserCredential userCredential) {
        return UserCredentialDto.UserCredentialDto()
                .application(ApplicationDto.of(userCredential.getApplication()))
                .role(RoleDto.of(userCredential.getRole()))
                .password(userCredential.getPassword())
                .lastResetPassword(Optional.ofNullable(userCredential.getLastResetPassword())
                        .map(Date::from).orElse(null))
                .lastLoginOn(
                        Optional.ofNullable(userCredential.getLastLoginOn())
                                .map(Date::from).orElse(null)
                )
                .enabled(userCredential.isEnabled())
                .registeredAs(userCredential.getRegistrationMode())
                .build();
    }

    public static UserCredentialDto of(User user, Application application, Role role) {
        return user.getUserCredentials().stream().filter(
                userCredential ->
                        userCredential.getRole().getName().equalsIgnoreCase(role.getName())
                                && userCredential.getApplication().getName().equalsIgnoreCase(application.getName())
        ).findFirst().map(UserCredentialDto::of).orElse(null);
    }

    public static UserCredentialDto of(UserCredentialData userCredential) {
        return UserCredentialDto.UserCredentialDto()
                .password(userCredential.getPassword())
                .lastResetPassword(userCredential.getLastResetPassword())
                .lastLoginOn(userCredential.getLastLoginOn())
                .build();
    }

    public UserCredential map(UserCredential userCredential) {
        final UserCredential result = userCredential;
        result.setPreviousState(userCredential);
        result.setPassword(this.getPassword());

        return result;
    }

    interface New {

    }

    interface Exist {

    }

    interface Update extends Exist {

    }

    interface Details {

    }

    interface AdminDetails extends Details {

    }



}
