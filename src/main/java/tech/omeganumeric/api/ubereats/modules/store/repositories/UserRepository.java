package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.Phone;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = UserRepository.PATH,
        path = UserRepository.PATH,
        exported = false)
public interface UserRepository extends MetaRepository<User, Long> {
    String PATH = "users";

    String FETCH_JOIN = "left join fetch e.user as u ";

    String FETCH_JOINED = " ";

    String SELECTION = "SELECT l from User l " +
            "left join fetch l.phone as p " +
            "left join fetch l.userInformation as i " +
            "left join fetch l.userCredentials as c " +
            "left join fetch l.residence as r " +
            "left join fetch l.delivery as d " +
            "left join fetch l.savedAddresses as sa " +
            "left join fetch l.location as lo " +
            "left join fetch l.driverOrders as do " +
            "left join fetch l.eaterOrders as eo " +
            "left join fetch l.ratedEaters as re " +
            "left join fetch l.ratedDrivers as rd " +
            "left join fetch l.rateMenus as rm " +
            "";

    @Query("SELECT l from User l " +
            "left join fetch l.phone as p " +
            "left join fetch l.userInformation as i " +
            "left join fetch l.userCredentials as c " +
            "left join fetch l.residence as r " +
            "left join fetch l.delivery as d " +
            "left join fetch l.savedAddresses as sa " +
            "left join fetch l.location as lo " +
            "left join fetch l.driverOrders as do " +
            "left join fetch l.eaterOrders as eo " +
            "left join fetch l.ratedEaters as re " +
            "left join fetch l.ratedDrivers as rd " +
            "left join fetch l.rateMenus as rm " +
            ""
    )
    @Override
    List<User> findAll();


    @Query(SELECTION + " where lower(l.email) = lower(:email) " +
            "")
    Optional<User> findByEmail(@Param("email") String email);

    @Query(SELECTION + " where lower(l.login) = lower(:login) " +
            "")
    Optional<User> findByLogin(@Param("login") String login);

    @Query(SELECTION + "where l.phone = :phone " +
            "")
    Optional<User> findByPhone(@Param("phone") Phone phone);

    @Query(SELECTION + " where p.number = :phoneNumber " +
            "")
    Optional<User> findByPhoneNumber(@Param("phoneNumber") String phoneNumber);


    @Query(SELECTION + " where :email is null or  lower(l.email) = lower(:email) " +
            "or :login is null or lower(l.login) = lower(:login) " +
            "or :phoneNumber is null or  p.number = :phoneNumber " +
            "")
    Optional<User> findByUniqueKey(
            @Param("login") String login,
            @Param("email") String email,
            @Param("phoneNumber") String phoneNumber
    );

    @Query(SELECTION + " where :username is null or  lower(l.email) = lower(:username) " +
            "or lower(l.login) = lower(:username) " +
            "or p.number = :username " +
            "")
    Optional<User> findByUniqueKey(
            @Param("username") String username
    );

    boolean existsByLogin(String login);

    boolean existsByEmail(String email);


    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends User> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<User> findAllById(Iterable<Long> iterable);
}
