package tech.omeganumeric.api.ubereats.modules.store.configs.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties("store")
public class StoreProperties {

    private boolean initialized;
    private RoleProperties role;
    private ApplicationProperties application;
    private FileStorageProperties file;


    @Data
    public static class RoleProperties {
        private List<String> name = new ArrayList<>();
        private List<String> description = new ArrayList<>();
    }

    @Data
    public static class ApplicationProperties {
        private List<String> name = new ArrayList<>();
        private List<String> description = new ArrayList<>();
        private List<String> type = new ArrayList<>();

    }

    @Data
    public static class FileStorageProperties {

        private String path;

        private List<String> extensions = new ArrayList<>();
    }

}
