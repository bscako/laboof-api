package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.RestaurantMenu;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.RestaurantMenuId;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = RestaurantMenuRepository.PATH,
        path = RestaurantMenuRepository.PATH,
        exported = false)
public interface RestaurantMenuRepository
        extends MetaRepository<RestaurantMenu, RestaurantMenuId> {
    String PATH = "restaurants_menus";

    String SELECTION = "SELECT l from RestaurantMenu l " +
            "left join fetch  l.restaurant " +
            "left join fetch  l.menu " +
            "";

    @Query("SELECT l from RestaurantMenu l " +
            "left join fetch  l.restaurant " +
            "left join fetch  l.menu "
    )
    @Override
    List<RestaurantMenu> findAll();


    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends RestaurantMenu> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    <S extends RestaurantMenu> Optional<S> findOne(Example<S> example);

    @Override
    @RestResource(exported = false)
    List<RestaurantMenu> findAllById(Iterable<RestaurantMenuId> iterable);
}
