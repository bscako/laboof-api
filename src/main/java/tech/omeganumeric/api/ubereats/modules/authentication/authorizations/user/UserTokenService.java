package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiTokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenService;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;

@Service
public class UserTokenService extends TokenService<ApiTokenProvider> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final UserRepository userRepository;
    private final UserEncoder usernameEncoder;

    protected UserTokenService(
            ApiTokenProvider provider,
            UserRepository userRepository,
            UserEncoder usernameEncoder
    ) {
        super(provider);
        this.userRepository = userRepository;
        this.usernameEncoder = usernameEncoder;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.usernameEncoder.decodeUser(username);
        Application application = this.usernameEncoder.decodeApplication(username);
        Role role = this.usernameEncoder.decodeRole(username);
        if (user != null && application != null && role != null) {
            return new UserToken(
                    user,
                    application,
                    role,
                    this.usernameEncoder
            );
        }
        return null;
    }
}
