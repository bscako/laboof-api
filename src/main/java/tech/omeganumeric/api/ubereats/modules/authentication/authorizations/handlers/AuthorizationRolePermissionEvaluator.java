package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.handlers;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.access.expression.DenyAllPermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import tech.omeganumeric.api.ubereats.configs.properties.AppProperties;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class AuthorizationRolePermissionEvaluator implements PermissionEvaluator {

    private static final PermissionEvaluator denyAll = new DenyAllPermissionEvaluator();

    private final UserEncoder usernameEncoder;
    private final AppProperties appProperties;

    public AuthorizationRolePermissionEvaluator(
            UserEncoder usernameEncoder,
            AppProperties appProperties
    ) {
        this.usernameEncoder = usernameEncoder;
        this.appProperties = appProperties;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {

        User user = null;
        Application application = null;
        List<Role> roles = new ArrayList<>();
        if (authentication != null) {
            UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            user = this.usernameEncoder.decodeUser(userDetails.getUsername());
            application = this.usernameEncoder.decodeApplication(userDetails.getUsername());
        }

        Application finalApplication = application;
        final String applicationName = Optional.ofNullable(targetDomainObject)
                .map(o -> {
                    if (targetDomainObject instanceof String) return targetDomainObject.toString();
                    else return null;
                }).map(s -> {
            if (s.equalsIgnoreCase(appProperties.getName())) {
                return appProperties.getName();
            }
            if (s.equalsIgnoreCase("self")) {
                return Optional.ofNullable(finalApplication)
                        .map(Application::getName)
                        .orElse(s);
            }
            return s;
        }).orElse(null);

        if (permission instanceof String && user != null) {
            roles = user.getUserCredentials().stream()
                    .filter(
                            userCredential -> userCredential.getApplication()
                                    .getName().equalsIgnoreCase(applicationName)
                    )
                    .map(UserCredential::getRole)
                    .filter(role -> role.getName().equalsIgnoreCase(permission.toString()))
                    .collect(Collectors.toList());
        }

        return roles.size() == 1;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType, Object permission) {
        // I will not implement this method just because I don't needed in this demo.
        throw new UnsupportedOperationException();
    }
}
