package tech.omeganumeric.api.ubereats.modules.registration.otp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.registration.configs.properties.RegistrationProperties;
import tech.omeganumeric.api.ubereats.modules.registration.otp.sender.mail.MailSenderService;
import tech.omeganumeric.api.ubereats.modules.registration.otp.sender.mail.SenderDataEmail;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OtpService {

    private final Logger log = LoggerFactory.getLogger(OtpService.class);

    private final OtpGenerator otpGenerator;
    private final RegistrationProperties.OtpProperties properties;
    private final MailSenderService mailSenderService;

    public OtpService(
            RegistrationProperties properties,
            OtpGenerator otpGenerator,
            MailSenderService mailSenderService
    ) {
        this.properties = properties.getOtp();
        this.otpGenerator = otpGenerator;
        this.mailSenderService = mailSenderService;
    }

    public String getOtpOf(OtpUser user) {
        return otpGenerator.getOtpByKey(user);
    }

    public boolean existOtpOf(OtpUser user) {
        return otpGenerator.getOtpByKey(user) != null;
    }

    public OtpResponse generateOtp(OtpUser user) {
        boolean isGenerated = this.generate(user);
        RegistrationProperties.OtpProperties.GeneratorProperties.MessageProperties
                properties = this.properties.getGenerator().getMessage();
        return OtpResponse.builder()
                .message(properties.getSuccess())
                .success(isGenerated)
                .build();
    }

    public OtpResponse validateOtp(OtpUser otpUser, String otp) {
        boolean isValid = this.validate(otpUser, otp);
        return OtpResponse.builder()
                .message("Entered OTP is valid!")
                .success(isValid)
                .build();
    }

    private boolean generate(OtpUser user) {
        log.info("generate otp for {}", user.getIdentifier().toString());
        String otp = otpGenerator.generateOtp(user);
        List<Boolean> isSentMailList = new ArrayList<>();
        List<Boolean> isSentSmsList = new ArrayList<>();
        List<Boolean> isSentCallList = new ArrayList<>();
        log.info("generated otp {} for {} ",otp, user.getIdentifier().toString());
        if (otp != null) {

            log.info("{} sending otp {} to {} ",
                    this.properties.getSender().getProviders(),
                    otp, user.getIdentifier().toString());
            for (String s : this.properties.getSender().getProviders()) {
                if (s.equalsIgnoreCase("mail")) {
                    isSentMailList.add(sendOtpWithEmail(user, otp));
                }
                if (s.equalsIgnoreCase("sms")) {
                    isSentSmsList.add(sendOtpWithSms(user, otp));
                }
                if (s.equalsIgnoreCase("call")) {
                    isSentCallList.add(sendOtpWithCall(user, otp));
                }
            }
        }

        log.info("{} - {} - {} sent otp {} to {} ",
                isSentMailList, isSentSmsList, isSentCallList,
                otp, user.getIdentifier().toString());

        boolean isSentMail = isSent(isSentMailList);
        boolean isSentSms = isSent(isSentSmsList);
        boolean isSentCall = isSent(isSentCallList);

        log.info("{} - {} - {} sent otp {} to {} ",
                isSentMail, isSentSms, isSentCall,
                otp, user.getIdentifier().toString());

        return isSentMail || isSentSms || isSentCall;
    }

    private boolean validate(OtpUser user, String otp) {
        String cacheOtp = otpGenerator.getOtpByKey(user);
        return Optional.ofNullable(cacheOtp).map(s -> {
            if (s.equals(otp)) {
                otpGenerator.clearOtpFromCache(user);
                return true;
            }
            return false;
        }).orElse(false);
    }

    private boolean isSent(List<Boolean> sendStates) {
        return !((sendStates.size() != this.properties.getSender().getProviders().size())
                || sendStates.stream().filter(b -> false).findAny().orElse(false))
                ;
    }

    private boolean sendOtpWithEmail(OtpUser user, String otp) {
        List<String> recipients = new ArrayList<>();
        recipients.add(user.getIdentifier().getEmail());

        // generate emailDTO object
        SenderDataEmail email = (SenderDataEmail) SenderDataEmail.builder()
                .subject(this.properties.getSender().getSubject())
                .body(
                        this.properties.getSender().getBody().getHeader() +
                                this.properties.getSender().getBody().getKey() +
                                otp +
                                this.properties.getSender().getBody().getFooter()
                )
                .recipients(recipients)
                .build();
        email.setHtml(this.properties.getSender().getBody().isHtml());
        return mailSenderService.send(email);
    }

    private boolean sendOtpWithSms(OtpUser user, String otp) {
        return true;
    }

    private boolean sendOtpWithCall(OtpUser user, String otp) {
        return true;
    }
}
