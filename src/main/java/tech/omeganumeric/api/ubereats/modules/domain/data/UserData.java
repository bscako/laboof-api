package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import org.hibernate.cfg.NotYetImplementedException;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserIdDto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;


@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@ToString(callSuper = true)
public class UserData
        <Dto, Phone, Information, Application, Role, Credential>
        extends UserDataPrincipal<Dto, Phone, Application, Role>
{

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private Information details;

    @JsonIgnore
    @NotNull(groups = {New.class, Update.class})
    @Null(groups = {NewSocial.class, UpdateSocial.class})
    @JsonView({Details.class})
    private Credential credential;


    @Builder(builderMethodName = "UserData")
    public UserData(
            UserIdDataAbstract<UserIdDto, Phone> identifier,
            Application application, Role role,
            Information details,
            Credential credential
    ) {
        super(identifier, application, role);
        this.details = details;
        this.credential = credential;
    }

    @Override
    public Dto toDto() {
        throw new NotYetImplementedException();
    }
}
