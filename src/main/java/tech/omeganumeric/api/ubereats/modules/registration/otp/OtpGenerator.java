package tech.omeganumeric.api.ubereats.modules.registration.otp;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.authentication.configs.properties.AuthenticationProperties;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;
import tech.omeganumeric.api.ubereats.modules.registration.configs.properties.RegistrationProperties;

import java.util.Random;
import java.util.concurrent.ExecutionException;

@Service
public class OtpGenerator {

    private final Logger log = LoggerFactory.getLogger(OtpService.class);

    private final RegistrationProperties.OtpProperties.GeneratorProperties generator;

    private final UserEncoder userEncoder;

    private final AuthenticationProperties.TokenProperties.UserTokenProperties.UserEncoderProperties userEncoderProperties;

    private LoadingCache<String, String> otpCache;


    public OtpGenerator(
            RegistrationProperties properties,
            UserEncoder userEncoder
    ) {
        super();
        this.generator = properties.getOtp().getGenerator();
        this.userEncoder = userEncoder;
        otpCache = CacheBuilder.newBuilder()
                .expireAfterWrite(this.generator.getExpiration().getValue(),
                        this.generator.getExpiration().getUnit())
                .build(
                        new CacheLoader<String, String>() {

                            @Override
                            public String load(String key) throws Exception {
                                return null;
                            }
                        }
                );
        userEncoderProperties = properties.getAuthenticationProperties().getToken().getUser().getEncoder();
    }

    /**
     * Method for generating OTP and put it in cache.
     *
     * @param key - cache key
     * @return cache value (generated OTP number)
     */
    public String generateOtp(OtpUser key) {
        Random random = new Random();
        String otp = otp();
        log.info("otp cache putting otp {} for {}", otp, key);
        String encoded = this.userEncoder.encode(key);
        otpCache.put(encoded, otp);
        return otp;
    }

    private String otp() {
        StringBuilder value = new StringBuilder();
        Random random = new Random();
        int length = generator.getValue().getLength();
        for (int i = 0; i < length; i++) {
            int index = random.ints(0, length)
                    .findFirst().orElse(0);
            value.append(index);
        }
        return value.toString();
    }

    /**
     * Method for getting OTP value by key.
     *
     * @param key - target key
     * @return OTP value
     */
    public String getOtpByKey(OtpUser key) throws OtpException {
        String encoded = this.userEncoder.encode(key);
        try {
            return otpCache.get(encoded);
        } catch (CacheLoader.InvalidCacheLoadException e) {
            log.warn("Otp Generator: InvalidCacheLoadException for user  {} ", key.getIdentifier());
            throw new OtpException(
                    String.format(
                            "Otp Generator: InvalidCacheLoadException Not found user  %s ",
                            key.getIdentifier().toString().replaceAll(userEncoderProperties.getDelimiter(), " ")
                    )
            );
        } catch (ExecutionException e) {
            return null;
        }
    }

    /**
     * Method for removing key from cache.
     *
     * @param key - target key
     */
    public void clearOtpFromCache(OtpUser key) {
        String encoded = this.userEncoder.encode(key);
        otpCache.invalidate(encoded);
    }
}
