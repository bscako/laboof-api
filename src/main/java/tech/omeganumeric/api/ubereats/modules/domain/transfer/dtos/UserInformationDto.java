package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.MediaData;
import tech.omeganumeric.api.ubereats.modules.domain.data.UserInformationData;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserInformation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInformationDto extends UserInformationData {

    @JsonIgnore
    @Null
    private Long id;

    @NotNull
    @JsonIgnoreProperties({"id"})
    @JsonView({Details.class})
    private UserIdDto identifier;

    @NotNull
    @JsonIgnoreProperties({"id"})
    @JsonView({Details.class})
    private MediaDto avatar;

    @Builder(builderMethodName = "UserInformationDto")
    public UserInformationDto(
            @NotNull String name, String firstname, String lastname,
            @Null Long id, @NotNull UserIdDto identifier, MediaDto avatar
    ) {
        super(name, firstname, lastname, MediaData.of(avatar));
        this.id = id;
        this.identifier = identifier;
        this.avatar = avatar;
    }


    public static UserInformationDto of(UserInformation userInformation) {
        return UserInformationDto.UserInformationDto()
                .id(userInformation.getId())
                .identifier(UserIdDto.of(userInformation.getUser()))
                .name(userInformation.getName())
                .firstname(userInformation.getFirstname())
                .lastname(userInformation.getLastname())
                .avatar(MediaDto.of(userInformation.getAvatar()))
                .build();
    }

    public static UserInformationDto of(UserInformationData userInformation) {
        return UserInformationDto.UserInformationDto()
                .name(userInformation.getName())
                .firstname(userInformation.getFirstname())
                .lastname(userInformation.getLastname())
//                .avatar(MediaDto.of(userInformation.getAvatar()))
                .build();
    }


    public UserInformation map(UserInformation userInformation) {
        userInformation.setName(
                Optional.ofNullable(this.getName()).orElse(userInformation.getName())
        );
        userInformation.setFirstname(
                Optional.ofNullable(this.getFirstname()).orElse(userInformation.getFirstname())
        );
        userInformation.setLastname(
                Optional.ofNullable(this.getLastname()).orElse(userInformation.getLastname())
        );
        return userInformation;
    }

    public static UserInformationDto of(User user) {
        return UserInformationDto.of(user.getUserInformation());
    }

    interface New {

    }

    interface Exist {

    }

    interface Update extends Exist {

    }

    interface Details {

    }

    interface AdminDetails extends Details {

    }
}
