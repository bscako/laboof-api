package tech.omeganumeric.api.ubereats.modules.profile.domains;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tech.omeganumeric.api.ubereats.modules.domain.data.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserIdDto;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class ProfileUserIdentifierDataRequest extends UserDataPrincipal<UserDto, PhoneData,
        ApplicationData, RoleData> {

    @Builder(builderMethodName = "RegistrationUserDataOtpRequest")
    public ProfileUserIdentifierDataRequest(
            UserIdDataAbstract<UserIdDto, PhoneData> identifier,
            ApplicationData applicationData,
            RoleData roleData
    ) {
        super(identifier, applicationData, roleData);
    }

    @Override
    public UserDto toDto() {
        return UserDto.of(this);
    }
}
