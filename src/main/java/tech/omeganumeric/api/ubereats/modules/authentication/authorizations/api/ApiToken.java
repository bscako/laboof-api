package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.Token;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@JsonIgnoreProperties({
        "password",
        "lastPasswordResetDate",
        "authorities",
        "enabled"
})
public class ApiToken extends Token {

    private static final long serialVersionUID = 2038426995877959196L;

    private final Application api;

    public ApiToken(Application api) {
        super(
                api.getName(),
                null,
                null,
                true,
                Date.from(api.getCreatedAt())
        );
        this.api = api;
    }
}
