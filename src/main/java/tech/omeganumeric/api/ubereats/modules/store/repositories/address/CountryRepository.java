package tech.omeganumeric.api.ubereats.modules.store.repositories.address;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.address.Country;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = CountryRepository.PATH,
        path = CountryRepository.PATH,
        exported = false)
public interface CountryRepository extends MetaRepository<Country, Long> {
    String PATH = "countries";

    String SELECTION = "SELECT l from Country l " +
            "left join fetch l.departments as d " +
            "left join fetch l.region as r " +
            "";

    @Query("SELECT l from Country l " +
            "left join fetch l.departments as d " +
            "left join fetch l.region as r " +
            ""
    )
    @Override
    List<Country> findAll();


//    @Query(SELECTION + "where (:name is null or lower(l.name) = lower(:name)) " +
//            "")
//    Optional<Country> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Country> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Country> findAllById(Iterable<Long> iterable);
}
