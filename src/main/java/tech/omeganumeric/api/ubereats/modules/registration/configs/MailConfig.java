package tech.omeganumeric.api.ubereats.modules.registration.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import tech.omeganumeric.api.ubereats.modules.registration.configs.properties.MailProperties;

import java.util.Properties;

@Configuration
public class MailConfig {

    private static String PROPERTIES_PROTOCOL = "mail.transport.protocol";
    private static String PROPERTIES_SMTP_AUTHENTICATION = "mail.smtp.auth";
    private static String PROPERTIES_SMTP_TLS_ENABLED = "mail.smtp.starttls.enable";
    private static String PROPERTIES_DEBUG = "mail.debug";

    private final MailProperties mailProperties;

    public MailConfig(MailProperties provider) {
        this.mailProperties = provider;
    }

    @Bean
    public JavaMailSender mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        javaMailSender.setHost(mailProperties.getHost());
        javaMailSender.setPort(mailProperties.getPort());

        javaMailSender.setUsername(mailProperties.getUsername());
        javaMailSender.setPassword(mailProperties.getPassword());

        Properties properties = javaMailSender.getJavaMailProperties();
        properties.put(PROPERTIES_PROTOCOL, mailProperties.getTransport().getProtocol());
        properties.put(PROPERTIES_SMTP_AUTHENTICATION, mailProperties.getTransport().isAuthentication());
        properties.put(PROPERTIES_SMTP_TLS_ENABLED, mailProperties.getTransport().getStarttls().isEnabled());
        properties.put(PROPERTIES_DEBUG, mailProperties.isDebug());

        return javaMailSender;
    }

}
