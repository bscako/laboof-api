package tech.omeganumeric.api.ubereats.modules.registration.otp;

public class OtpException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = -2806739563284198752L;

    public OtpException(String message, Throwable cause) {
        super(message, cause);
    }

    public OtpException(String message) {
        super(message);
    }
}
