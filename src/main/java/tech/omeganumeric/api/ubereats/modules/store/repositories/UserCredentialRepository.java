package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.UserCredentialId;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = UserCredentialRepository.PATH,
        path = UserCredentialRepository.PATH,
        exported = false)
public interface UserCredentialRepository extends MetaRepository<UserCredential, UserCredentialId> {
    String PATH = "users_credentials";

    String SELECTION = "SELECT l from UserCredential l " +
            "left join fetch l.application as a " +
            "left join fetch l.role as r " +
            "left join fetch l.user as u " +
            "left join fetch u.phone as p " +
            "left join fetch u.userInformation as i " +
            "";

    @Query("SELECT l from UserCredential l " +
            "left join fetch l.application as a " +
            "left join fetch l.role as r " +
            "left join fetch l.user as u " +
            "left join fetch u.phone as p " +
            "left join fetch u.userInformation as i " +
            ""
    )
    @Override
    List<UserCredential> findAll();

    @Query(SELECTION + "where lower(a.name) = lower(:applicationName) " +
            "")
    List<UserCredential> findByApplicationName(@Param("applicationName") String applicationName);

    @Query(SELECTION + "where lower(u.login) = lower(:userPKeyLogin) " +
            "")
    Optional<UserCredential> findByUserPKeyLogin(@Param("userPKeyLogin") String userPKeyLogin);

    @Query(SELECTION + "where lower(u.email) = lower(:userPKeyEmail) " +
            "")
    Optional<UserCredential> findByUserPKeyEmail(@Param("userPKeyEmail") String userPKeyEmail);


    @Query(SELECTION + "where lower(p.number) = lower(:userPKeyPhoneNumber) " +
            "")
    Optional<UserCredential> findByUserPKeyPhoneNumber(@Param("userPKeyPhoneNumber") String userPKeyPhoneNumber);


    @Query(SELECTION + " where lower(u.email) = lower(:userPKeyEmail) " +
            "or lower(u.login) = lower(:userPKeyLogin) " +
            "or lower(p.number) = lower(:userPKeyPhoneNumber) " +
            "")
    Optional<UserCredential> findByUserPKeyEmailOrLoginOrPhoneNumber(
            @Param("userPKeyEmail") String userPKeyEmail,
            @Param("userPKeyLogin") String userPKeyLogin,
            @Param("userPKeyPhoneNumber") String userPKeyPhoneNumber
    );

    @Query(SELECTION + " where a.name = :applicationName " +
            " and (lower(u.email) = lower(:userPKeyEmail) " +
            "or lower(u.login) = lower(:userPKeyLogin) " +
            "or lower(p.number) = lower(:userPKeyPhoneNumber) ) " +
            "")
    Optional<UserCredential> findByApplicationNameAndUserPKeyEmailOrLoginOrPhoneNumber(
            @Param("applicationName") String applicationName,
            @Param("userPKeyEmail") String userPKeyEmail,
            @Param("userPKeyLogin") String userPKeyLogin,
            @Param("userPKeyPhoneNumber") String userPKeyPhoneNumber
    );


    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends UserCredential> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<UserCredential> findAllById(Iterable<UserCredentialId> iterable);
}
