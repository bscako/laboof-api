package tech.omeganumeric.api.ubereats.modules.registration.otp.sender.mail;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.omeganumeric.api.ubereats.modules.registration.otp.SenderData;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class SenderDataEmail extends SenderData {

    private boolean html;

    @Builder
    public SenderDataEmail(
            String subject, String body, List<String> recipients,
            List<String> ccList, List<String> bccList, String attachmentPath,
            boolean html
    ) {
        super(subject, body, recipients, ccList, bccList, attachmentPath);
        this.html = html;
    }
}
