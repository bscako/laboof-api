package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.springframework.data.rest.core.annotation.RestResource;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.IdUserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@RestResource
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"user", "avatar"})
@ToString(callSuper = true, exclude = {"user"})
@Table(
        name = UserInformation.FIELD_ENTITY_TABLE_NAME
)
public class UserInformation extends IdUserDateAudit {

    public static final String FIELD_ENTITY = "user_information";
    public static final String FIELD_ENTITY_TABLE_NAME = "users_information";

    private static final long serialVersionUID = 3679301844774105320L;

    @OneToOne(mappedBy = "userInformation", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"userInformation"})
    @Builder.Default
    private User user = null;

    @OneToOne(cascade = CascadeType.ALL, targetEntity = Media.class)
    @JoinTable(
            name = "users_avatars",
            joinColumns = @JoinColumn(name = "user_id", unique = true),
            inverseJoinColumns = @JoinColumn(
                    name = "media_id",
                    unique = true
            )
    )
    @JsonIgnoreProperties(value = {"userInformation"})
    @Builder.Default
    private Media avatar = null;

    @Column(nullable = false)
    @NotNull
    private String name;

    @Column
    private String firstname;

    @Column
    private String lastname;

    private void basics() {

    }

    private void updateAssociation() {
        if (this.getAvatar() != null) {
            this.getAvatar().setUserInformation(this);
        }
        if (this.getUser() != null) {
            this.getUser().setUserInformation(this);
        }
    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.updateAssociation();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.updateAssociation();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }


}
