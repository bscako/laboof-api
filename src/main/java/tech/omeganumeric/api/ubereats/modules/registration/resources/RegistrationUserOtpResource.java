package tech.omeganumeric.api.ubereats.modules.registration.resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.ResourceSupport;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.registration.otp.OtpResponse;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.RoleRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;

@Value
@EqualsAndHashCode(callSuper = true)
public class RegistrationUserOtpResource extends ResourceSupport
        implements RegistrationModeResource,
        RegistrationResource<RegistrationUserOtpResource> {

    @JsonIgnoreProperties({"id"})
    private UserDto user;

    private OtpResponse otp;

    @Builder(builderMethodName = "RegistrationUserOtpResource")
    public RegistrationUserOtpResource(
            User user, Application application,
            Role role, OtpResponse otp,
            RepositoryEntityLinks repositoryEntityLinks
    ) {
        this.user = UserDto.of(user, application, role);
        this.otp = otp;
        this.add(repositoryEntityLinks
                .linkToSingleResource(UserRepository.class, user.getId())
                .withSelfRel());
        this.add(repositoryEntityLinks
                .linkToSingleResource(ApplicationRepository.class, application.getId())
                .withSelfRel());
        this.add(repositoryEntityLinks
                .linkToSingleResource(RoleRepository.class, role.getId())
                .withSelfRel());
    }

    @JsonIgnore
    @Override
    public RegistrationUserResource userResource() {
        return null;
    }

    @JsonIgnore
    @Override
    public RegistrationUserOtpResource otpResource() {
        return this;
    }

    @JsonIgnore
    @Override
    public RegistrationUserOtpResource resource() {
        return otpResource();
    }
}
