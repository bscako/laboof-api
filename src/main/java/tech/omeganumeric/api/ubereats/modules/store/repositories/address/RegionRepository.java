package tech.omeganumeric.api.ubereats.modules.store.repositories.address;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.address.Region;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = RegionRepository.PATH,
        path = RegionRepository.PATH,
        exported = false)
public interface RegionRepository extends MetaRepository<Region, Long> {
    String PATH = "regions";

    String SELECTION = "SELECT l from Region l " +
            "left join fetch l.town as t " +
            "";

    @Query("SELECT l from Region l " +
            "left join fetch l.countries as c " +
            ""
    )
    @Override
    List<Region> findAll();


//    @Query(SELECTION + "where (:name is null or lower(l.name) = lower(:name)) " +
//            "")
//    Optional<Region> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Region> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Region> findAllById(Iterable<Long> iterable);
}
