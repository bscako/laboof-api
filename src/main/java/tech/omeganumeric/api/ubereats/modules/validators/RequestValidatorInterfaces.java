package tech.omeganumeric.api.ubereats.modules.validators;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
//@AllArgsConstructor
public class RequestValidatorInterfaces implements Serializable {

    interface New {

    }

    interface Exist {

    }

    interface Update extends Exist {

    }

    interface Details {

    }

    interface AdminDetails extends Details {

    }
}
