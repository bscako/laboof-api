package tech.omeganumeric.api.ubereats.modules.store.configs;

import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.services.ApplicationContextProvider;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@EnableJpaAuditing
public class StoreConfig {


    // default insertion
    private static final List<Role> ROLES = Stream.of(
            Role.builder().name("USER").description("Simple user").build(),
            Role.builder().name("ADMIN").description("Admin").build(),
            Role.builder().name("SUPER_ADMIN").description("super admin").build()
    ).collect(Collectors.toList());

    private static final List<Application> APPLICATIONS = Stream.of(
            Application.builder().name("EATER").description("eater application").type("Application").build(),
            Application.builder().name("Driver").description("driver application").type("Application").build(),
            Application.builder().name("Restaurant").description("restaurant application").type("application").build()
    ).collect(Collectors.toList());


    private final AutowireCapableBeanFactory beanFactory;

    public StoreConfig(AutowireCapableBeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    @Bean
    public static ApplicationContextProvider contextProvider() {
        return new ApplicationContextProvider();
    }
}
