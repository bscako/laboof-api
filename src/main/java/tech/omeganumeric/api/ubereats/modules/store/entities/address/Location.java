package tech.omeganumeric.api.ubereats.modules.store.entities.address;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.store.entities.Address;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.IdUserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Table(
        name = Location.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = "id",
                        name = "uk_location_id"),
                @UniqueConstraint(
                        columnNames = {"longitude", "latitude"},
                        name = "uk_location_longitude_latitude")
        }
)
public class Location extends IdUserDateAudit {

    public static final String FIELD_ENTITY = "location";
    public static final String FIELD_ENTITY_TABLE_NAME = "locations";

    @Column(nullable = false, unique = true)
    private Long longitude;

    @Column(nullable = false, unique = true)
    private Long latitude;

    @Column
    private Long altitude;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address", unique = true)
//    @MapsId
    @JsonIgnoreProperties(value = {"location"})
    @NotNull
    private Address address;

    @OneToMany(mappedBy = "location", cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonIgnoreProperties(value = {"location"})
    @Builder.Default
    private Set<User> users = new HashSet<>();


    public void updateAssociations() {
    }

    private void basics() {

    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.updateAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.updateAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }


}
