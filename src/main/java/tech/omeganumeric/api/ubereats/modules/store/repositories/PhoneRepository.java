package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.Phone;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = PhoneRepository.PATH,
        path = PhoneRepository.PATH,
        exported = false)
public interface PhoneRepository extends MetaRepository<Phone, Long> {
    String PATH = "phones";

    String SELECTION = "SELECT l from Phone l " +
//            "left join fetch l.user as u " +
            "";

    @Query("SELECT l from Phone l " +
            "left join fetch l.user as u " +
            "left join fetch u.userInformation as i " +
            "")
    @Override
    List<Phone> findAll();


    @Query(SELECTION + " where :number is null or  lower(l.number) = lower(:number) " +
            "")
    Optional<Phone> findByNumber(@Param("number") String number);

    boolean existsByNumber(String number);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Phone> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Phone> findAllById(Iterable<Long> iterable);
}
