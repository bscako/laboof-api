package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user;

import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenException;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@Slf4j
public class UserTokenFilter extends TokenFilter<UserTokenService, UserTokenProvider> {

    private final AuthenticationSuccessHandler authenticationSuccessHandler;

    protected UserTokenFilter(
            UserTokenService service,
            UserTokenProvider provider, AuthenticationSuccessHandler authenticationSuccessHandler) {
        super(service, provider);
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain chain
    ) throws ServletException, IOException, TokenException {
        String token = this.getProvider().resolveToken(request);
        log.debug("Filter: User Token : {} ", token);
        try {
            if (token != null) {
                final UserDetails userDetails = this.getService()
                        .loadUserByUsername(
                                this.getProvider().getUsernameFromToken(token)
                        );

                if (userDetails == null || this.getProvider().validateToken(token, userDetails)) {
                    log.warn("Filter: User Token : {} {}", userDetails, this.getProvider().getUsernameFromToken(token));
                    throw new TokenException("Invalid User Token");
                }
                UserToken userToken = (UserToken) userDetails;
                Authentication authentication = this.getService()
                        .authentication(
                                request, userDetails, null,
                                userToken.getRole().getName()
                        );
                SecurityContextHolder.getContext().setAuthentication(authentication);
                this.authenticationSuccessHandler.onAuthenticationSuccess(request, response, authentication);
                log.debug(String.format(
                        "Authenticated User : %s  with token: %s  ",
                        userDetails.getUsername(),
                        token));

            }
            this.setToken(token);
        } catch (ExpiredJwtException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - Expired token {} - {}", token, e.getMessage());
            super.throwException(token, "Expired User ", e);

        } catch (UsernameNotFoundException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - UsernameNotFound for token {} - {}", token, e.getMessage());
            super.throwException(token, "UsernameNotFound of User ", e);
        } catch (BadCredentialsException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - Bad Credentials {} - {} ", token, e.getMessage());
            super.throwException(token, "Bad Credentials of User ", e);
        } catch (TokenException e) {
            this.setToken(null);
            log.warn("Security exception for unknown user - Bad Token {} - {} ", token, e.getMessage());
            super.throwException(token, "Invalid User", e);
        }
        chain.doFilter(request, response);
    }
}
