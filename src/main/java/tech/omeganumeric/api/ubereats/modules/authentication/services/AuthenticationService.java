package tech.omeganumeric.api.ubereats.modules.authentication.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.configs.properties.AppProperties;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiTokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserToken;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserTokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserTokenService;
import tech.omeganumeric.api.ubereats.modules.authentication.configs.properties.AuthenticationProperties;
import tech.omeganumeric.api.ubereats.modules.authentication.domains.AuthenticationUserRequest;
import tech.omeganumeric.api.ubereats.modules.authentication.resources.AuthenticationApplicationResource;
import tech.omeganumeric.api.ubereats.modules.authentication.resources.AuthenticationUserResource;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.RoleRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;
import tech.omeganumeric.api.ubereats.modules.store.services.StoreService;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@Service
public class AuthenticationService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final AuthenticationManager authenticationManager;
    private final RepositoryEntityLinks repositoryEntityLinks;
    private final ApplicationRepository applicationRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final AppProperties appProperties;
    private final AuthenticationProperties authenticationProperties;
    private final ApiTokenProvider apiTokenProvider;
    private final UserTokenProvider userTokenProvider;
    private final UserTokenService userTokenService;
    private final UserEncoder usernameEncoder;
    private final StoreService storeService;

    public AuthenticationService(
            AuthenticationManager authenticationManager,
            AppProperties appProperties,
            ApplicationRepository applicationRepository,
            RepositoryEntityLinks repositoryEntityLinks,
            UserRepository userRepository,
            RoleRepository roleRepository,
            AuthenticationProperties authenticationProperties,
            ApiTokenProvider apiTokenProvider,
            UserTokenProvider userTokenProvider,
            UserTokenService userTokenService,
            UserEncoder usernameEncoder,
            StoreService storeService) {
        this.authenticationManager = authenticationManager;
        this.appProperties = appProperties;
        this.applicationRepository = applicationRepository;
        this.repositoryEntityLinks = repositoryEntityLinks;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.authenticationProperties = authenticationProperties;
        this.apiTokenProvider = apiTokenProvider;
        this.userTokenProvider = userTokenProvider;
        this.userTokenService = userTokenService;
        this.usernameEncoder = usernameEncoder;
        this.storeService = storeService;
    }

    public AuthenticationApplicationResource apiKey() {
        log.debug("api key");
        Application application = this.applicationRepository.findByName(this.appProperties.getName())
                .orElseThrow(EntityNotFoundException::new);
        log.debug("api key: {}", application);
        return new AuthenticationApplicationResource(application, this.apiTokenProvider, this.repositoryEntityLinks);
    }

    public AuthenticationUserResource authenticate(final AuthenticationUserRequest data) {
        User user = this.userRepository.findByUniqueKey(data.getUsername())
                .orElseThrow(EntityNotFoundException::new);
        Application application = this.applicationRepository.findByName(data.getApplication())
                .orElseThrow(EntityNotFoundException::new);
        String roleName = data.getRole();
        if (roleName == null || roleName.isEmpty()) {
            roleName = this.authenticationProperties.getRole();
        }

        Role role = this.roleRepository.findByName(roleName).orElseThrow(EntityNotFoundException::new);
        log.info("trying authentication user {} using application {} as {} roles",
                user.getLogin(),
                application.getName(),
                role.getName()
        );

        Authentication authentication = this.userTokenService.authentication(
                new UserToken(user, application, role, this.usernameEncoder),
                data.getPassword(),
                role.getName()
        );
        log.info("authenticated user {} using application {}", user, application);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new AuthenticationUserResource(
                user,
                application,
                role,
                this.usernameEncoder,
                this.userTokenProvider,
                this.repositoryEntityLinks
        );
    }

    public AuthenticationUserResource authenticatedUser(UserDetails userDetails) {
        return Optional.ofNullable(userDetails)
                .map(UserDetails::getUsername)
                .map(
                        username -> new AuthenticationUserResource(
                                this.usernameEncoder.decodeUser(username),
                                this.usernameEncoder.decodeApplication(username),
                                this.usernameEncoder.decodeRole(username),
                                this.usernameEncoder,
                                this.userTokenProvider,
                                this.repositoryEntityLinks
                        )
                )
                .orElseThrow(() -> new AuthenticationCredentialsNotFoundException("No user logged ! Please sign in "));
    }

    public void logout(UserDetails userDetails) {
        SecurityContextHolder.getContext().setAuthentication(null);
    }


}
