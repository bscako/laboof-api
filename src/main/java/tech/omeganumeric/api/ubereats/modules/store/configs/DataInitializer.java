package tech.omeganumeric.api.ubereats.modules.store.configs;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import tech.omeganumeric.api.ubereats.configs.properties.AppProperties;
import tech.omeganumeric.api.ubereats.configs.properties.app.IAppUserProperties;
import tech.omeganumeric.api.ubereats.modules.store.configs.properties.StoreProperties;
import tech.omeganumeric.api.ubereats.modules.store.entities.*;
import tech.omeganumeric.api.ubereats.modules.store.entities.address.*;
import tech.omeganumeric.api.ubereats.modules.store.entities.order.OrderStatus;
import tech.omeganumeric.api.ubereats.modules.store.entities.payment.PaymentMode;
import tech.omeganumeric.api.ubereats.modules.store.repositories.*;
import tech.omeganumeric.api.ubereats.modules.store.repositories.address.RegionRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.order.OrderStatusRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.payment.PaymentModeRepository;

import javax.imageio.ImageIO;
import javax.persistence.EntityNotFoundException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@Slf4j
public class DataInitializer implements CommandLineRunner {


    private final AppProperties appProperties;
    private final StoreProperties storeProperties;
    private final RoleRepository roleRepository;
    private final ApplicationRepository applicationRepository;
    private final UserRepository userRepository;
    private final UserInformationRepository userInformationRepository;
    private final RegionRepository regionRepository;
    private final PaymentModeRepository paymentModeRepository;
    private final OrderStatusRepository orderStatusRepository;
    private final MediaRepository mediaRepository;

    private final List<Role> ROLES;
    private final List<Application> APPLICATIONS;
    private final List<User> USERS;

    public DataInitializer(
            AppProperties appProperties,
            StoreProperties storeProperties,
            RoleRepository roleRepository,
            ApplicationRepository applicationRepository,
            UserRepository userRepository,
            UserInformationRepository userInformationRepository, RegionRepository regionRepository,
            PaymentModeRepository paymentModeRepository,
            OrderStatusRepository orderStatusRepository,
            MediaRepository mediaRepository
    ) {
        ROLES = this.Roles(storeProperties);
        APPLICATIONS = this.Applications(storeProperties);
        this.userInformationRepository = userInformationRepository;
        this.regionRepository = regionRepository;
        this.paymentModeRepository = paymentModeRepository;
        this.orderStatusRepository = orderStatusRepository;
        this.mediaRepository = mediaRepository;
        USERS = this.Users();
        this.appProperties = appProperties;
        this.storeProperties = storeProperties;
        this.roleRepository = roleRepository;
        this.applicationRepository = applicationRepository;
        this.userRepository = userRepository;
    }


    @Override
    public void run(String... args) throws Exception {

        if (storeProperties.isInitialized()) {
            this.initializeStoreAuthentication();
            this.initializeStorePayment();
            this.initializeStoreOrders();
            this.initializeStoreAddress();
            this.initializeStoreMedia();
        }


    }

    private Media addResource(Media media) {
        try {
            String baseDir = "./media";
            Path path = Paths.get(
                    ResourceUtils.getFile(
                            String.format(
                                    "%s/%s.%s",
                                    baseDir, media.getName(), media.getType()
                            )
                    ).getAbsolutePath()
            ).toAbsolutePath().normalize();
            log.info("media {} ", String.format(
                    "%s/%s.%s",
                    baseDir, media.getName(), media.getType()
            ));
            BufferedImage originalImage = ImageIO.read(
                    new File(path.toAbsolutePath().toString())
            );
            // convert BufferedImage to byte array
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            ImageIO.write(originalImage, media.getType(), byteArrayOutputStream);
            byteArrayOutputStream.flush();
            media.setFile(byteArrayOutputStream.toByteArray());
            byteArrayOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return media;
    }

    private void initializeStoreMedia() {
        final List<Media> medias = Stream.of(
                Media.builder()
                        .name("samsara")
                        .type("jpg")
                        .build()

        ).map(this::addResource).collect(Collectors.toList());
        this.mediaRepository.saveAll(medias);

        UserInformation admin = this.userInformationRepository.findByUserPKeyLogin("admin").orElse(null);

        Media avatar = Media.builder()
                .name("castle")
                .type("png")
                .avatar(true)
                .build();
        this.addResource(avatar);

        if (admin != null) {
//            avatar =  this.mediaRepository.save(avatar);
//            avatar.setUserInformation(admin);
            avatar = this.mediaRepository.save(avatar);
            admin.setAvatar(
                    avatar
            );
            log.info("file {}", admin.getAvatar().getFile() == null);
            this.userInformationRepository.save(admin);
        }
    }

    private List<Region> Regions() {
        return Stream.of(
                Region.builder()
                        .code("AF")
                        .name("Africa")
                        .countries(
                                Stream.of(
                                        Country.builder()
                                                .name("Benin")
                                                .code2("BJ")
                                                .code3("BJ")
                                                .density(12.0d)
                                                .population(1L)
                                                .phoneCode("229")
                                                .departments(
                                                        Stream.of(
                                                                Department.builder()
                                                                        .name("Litoral")
                                                                        .code("LT")
                                                                        .variant("litoral")
                                                                        .districts(
                                                                                Stream.of(
                                                                                        District.builder()
                                                                                                .name("Cotonou")
                                                                                                .code("CTN")
                                                                                                .variant("cotonou")
                                                                                                .towns(
                                                                                                        Stream.of(
                                                                                                                Town.builder()
                                                                                                                        .name("Cadjehoun")
                                                                                                                        .variant("cotonou")
                                                                                                                        .addresses(
                                                                                                                                Stream.of(
                                                                                                                                        Address.builder()
                                                                                                                                                .street("Boulevard de ;a marina")
                                                                                                                                                .building("28")
                                                                                                                                                .room("22")
                                                                                                                                                .build()

                                                                                                                                ).collect(Collectors.toSet())
                                                                                                                        )
                                                                                                                        .build()

                                                                                                        ).collect(Collectors.toSet())
                                                                                                )
                                                                                                .build()

                                                                                ).collect(Collectors.toSet())
                                                                        )
                                                                        .build()

                                                        ).collect(Collectors.toSet())
                                                )
                                                .variant("danxome")
                                                .build(),
                                        Country.builder()
                                                .name("nigeria")
                                                .code2("NG")
                                                .code3(null)
                                                .density(null)
                                                .population(null)
                                                .phoneCode(null)
                                                .departments(new HashSet<>())
                                                .variant(null)
                                                .build()

                                ).collect(Collectors.toSet())
                        )
                        .build(),
                Region.builder()
                        .code("EU")
                        .name("Europe")
                        .build(),
                Region.builder().code("NA").name("North America")
                        .build(),
                Region.builder().code("OC").name("Oceanic")
                        .build(),
                Region.builder().code("SA").name("South America")
                        .build()
        ).collect(Collectors.toList());
    }

    private void initializeStoreAddress() {
        this.regionRepository.saveAll(this.Regions());
    }

    private void initializeStorePayment() {
        // paymment mode
        final List<PaymentMode> paymentModes = Stream.of(
                PaymentMode.builder()
                        .mode("CARD_VISA")
                        .description("")
                        .payments(new HashSet<>())
                        .build(),
                PaymentMode.builder()
                        .mode("CASH")
                        .description("")
                        .payments(new HashSet<>())
                        .build(),
                PaymentMode.builder()
                        .mode("PAY_PAL")
                        .description("")
                        .payments(new HashSet<>())
                        .build(),
                PaymentMode.builder()
                        .mode("MOBILE_BANK")
                        .description("")
                        .payments(new HashSet<>())
                        .build()

        ).collect(Collectors.toList());
        this.paymentModeRepository.saveAll(paymentModes);

    }

    private void initializeStoreOrders() {
        // orderstatus
        final List<OrderStatus> orderStatuses = Stream.of(
                OrderStatus.builder()
                        .status("DELIVERED")
                        .description("")
                        .orders(new HashSet<>())
                        .build(),
                OrderStatus.builder()
                        .status("PENDING")
                        .description("")
                        .orders(new HashSet<>())
                        .build(),
                OrderStatus.builder()
                        .status("ACCEPTED")
                        .description("")
                        .orders(new HashSet<>())
                        .build(),
                OrderStatus.builder()
                        .status("ON_ROAD")
                        .description("")
                        .orders(new HashSet<>())
                        .build()
        ).collect(Collectors.toList());
        this.orderStatusRepository.saveAll(orderStatuses);

    }

    private void initializeStoreAuthentication() {
        // roles
        List<Role> roles = this.roleRepository.saveAll(ROLES);

        // api application
        Application api = Application.builder()
                .name(this.appProperties.getName())
                .description(this.appProperties.getDescription())
                .type(this.appProperties.getType())
                .build();
        api = applicationRepository.save(api);
        // applications
        List<Application> applications = this.applicationRepository.saveAll(APPLICATIONS);

        // api users
        User adminApi = User.builder()
                .email(this.appProperties.getAdmin().getEmail())
                .login(this.appProperties.getAdmin().getLogin())
                .phone(Phone.builder().number(this.appProperties.getAdmin().getPhone()).build())
                .userInformation(
                        UserInformation.builder()
                                .name(this.appProperties.getAdmin().getName())
                                .firstname(this.appProperties.getAdmin().getFirstname())
                                .lastname(this.appProperties.getAdmin().getLastname())
                                .build()
                )
                .build();
        adminApi = userRepository.save(adminApi);
        List<Role> adminRoles = roles.stream()
                .filter(savedRole -> ROLES.stream().anyMatch(
                        role -> savedRole.getName().equalsIgnoreCase(role.getName())
                        )
                )
                .collect(Collectors.toList());
        adminApi = addCredentialsToUser(adminApi, api, adminRoles, this.appProperties.getAdmin());
        adminApi = userRepository.save(adminApi);

        User userApi = User.builder()
                .email(this.appProperties.getUser().getEmail())
                .login(this.appProperties.getUser().getLogin())
                .phone(Phone.builder().number(this.appProperties.getUser().getPhone()).build())
                .userInformation(
                        UserInformation.builder()
                                .name(this.appProperties.getUser().getName())
                                .firstname(this.appProperties.getUser().getFirstname())
                                .lastname(this.appProperties.getUser().getLastname())
                                .build()
                )
                .build();
        userApi = userRepository.save(userApi);
        List<Role> userRoles = roles.stream()
                .filter(savedRole -> savedRole.getName().equalsIgnoreCase(ROLES.get(0).getName()))
                .collect(Collectors.toList());
        userApi = addCredentialsToUser(userApi, api, userRoles, this.appProperties.getUser());
        userApi = userRepository.save(userApi);


        // applications users
        List<User> users = USERS.stream().map(
                user -> User.builder()
                        .email(user.getEmail())
                        .login(user.getLogin())
                        .phone(user.getPhone())
                        .userInformation(user.getUserInformation())
                        .build()
        ).collect(Collectors.toList());

        users = this.userRepository.saveAll(users);

        List<User> usersWithCredentials = new ArrayList<>();

        for (int index = 0; index < users.size(); index++) {
            for (UserCredential userCredential : USERS.get(index).getUserCredentials()) {
                Role role = roles.stream()
                        .filter(
                                r -> r.getName().equalsIgnoreCase(userCredential.getRole().getName()
                                )
                        ).findFirst().orElseThrow(EntityNotFoundException::new);
                Application application = applications.stream()
                        .filter(
                                a -> a.getName().equalsIgnoreCase(userCredential.getApplication().getName()
                                )
                        ).findFirst().orElseThrow(EntityNotFoundException::new);
                users.get(index).getUserCredentials().add(
                        UserCredential.builder()
                                .application(application)
                                .role(role)
                                .user(users.get(index))
                                .password(userCredential.getPassword())
                                .enabled(true)
                                .build()
                );
            }
        }

        users = this.userRepository.saveAll(users);
    }

    private User addCredentialsToUser(User user, Application application, List<Role> roles, IAppUserProperties userProperties) {
        for (Role role : roles) {
            user.getUserCredentials().add(
                    UserCredential.builder()
                            .application(application)
                            .role(role)
                            .user(user)
                            .password(userProperties.getPassword())
                            .enabled(true)
                            .build()
            );
        }
        return user;
    }

    private List<Role> Roles(StoreProperties storeProperties) {
        List<Role> result = new ArrayList<>();
        StoreProperties.RoleProperties role = storeProperties.getRole();
        if (role.getName().size() == role.getDescription().size()) {

            for (int i = 0; i < role.getName().size(); i++) {
                result.add(
                        Role.builder()
                                .name(role.getName().get(i))
                                .description(role.getDescription().get(i))
                                .build()
                );
            }
        }
        return result;
    }

    private List<Application> Applications(StoreProperties storeProperties) {
        List<Application> result = new ArrayList<>();
        StoreProperties.ApplicationProperties application = storeProperties.getApplication();
        if ((application.getName().size() == application.getDescription().size())
                && (application.getName().size() == application.getType().size())
        ) {

            for (int i = 0; i < application.getName().size(); i++) {
                result.add(
                        Application.builder()
                                .name(application.getName().get(i))
                                .description(application.getDescription().get(i))
                                .type(application.getType().get(i))
                                .build()
                );
            }
        }
        return result;
    }

    private List<User> Users() {
        return Stream.of(
                User.builder()
                        .email("super@admin.uber")
                        .login("super_admin")
                        .phone(Phone.builder().number("7").build())
                        .userInformation(UserInformation.builder().name("super admin").build())
                        .userCredentials(
                                new HashSet<>(
                                        Arrays.asList(
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(0))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(0))
                                                        .role(ROLES.get(1))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(1))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(1))
                                                        .role(ROLES.get(1))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(2))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(2))
                                                        .role(ROLES.get(1))
                                                        .password("test")
                                                        .build()

                                        )
                                )
                        )
                        .build(),
                User.builder()
                        .email("user@eater.uber")
                        .login("eater_user")
                        .phone(Phone.builder().number("1").build())
                        .userInformation(UserInformation.builder().name("eater user").build())
                        .userCredentials(
                                new HashSet<>(
                                        Arrays.asList(
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(0))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build()
                                        )
                                )
                        )
                        .build(),
                User.builder()
                        .email("admin@eater.uber")
                        .login("eater_admin")
                        .phone(Phone.builder().number("2").build())
                        .userInformation(UserInformation.builder().name("eater admin").build())
                        .userCredentials(
                                new HashSet<>(
                                        Arrays.asList(
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(0))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(0))
                                                        .role(ROLES.get(1))
                                                        .password("test")
                                                        .build()

                                        )
                                )
                        )
                        .build(),
                User.builder()
                        .email("user@driver.uber")
                        .login("driver_user")
                        .phone(Phone.builder().number("3").build())
                        .userInformation(UserInformation.builder().name("driver user").build())
                        .userCredentials(
                                new HashSet<>(
                                        Arrays.asList(
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(1))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build()
                                        )
                                )
                        )
                        .build(),
                User.builder()
                        .email("admin@driver.uber")
                        .login("driver_admin")
                        .phone(Phone.builder().number("4").build())
                        .userInformation(UserInformation.builder().name("driver admin").build())
                        .userCredentials(
                                new HashSet<>(
                                        Arrays.asList(
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(1))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(1))
                                                        .role(ROLES.get(1))
                                                        .password("test")
                                                        .build()

                                        )
                                )
                        )
                        .build(),
                User.builder()
                        .email("user@restaurant.uber")
                        .login("restaurant_user")
                        .phone(Phone.builder().number("5").build())
                        .userInformation(UserInformation.builder().name("restaurant user").build())
                        .userCredentials(
                                new HashSet<>(
                                        Arrays.asList(
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(2))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build()

                                        )
                                )
                        )
                        .build(),
                User.builder()
                        .email("admin@restaurant.uber")
                        .login("restaurant_admin")
                        .phone(Phone.builder().number("6").build())
                        .userInformation(UserInformation.builder().name("restaurant admin").build())
                        .userCredentials(
                                new HashSet<>(
                                        Arrays.asList(
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(2))
                                                        .role(ROLES.get(0))
                                                        .password("test")
                                                        .build(),
                                                UserCredential.builder()
                                                        .application(APPLICATIONS.get(2))
                                                        .role(ROLES.get(1))
                                                        .password("test")
                                                        .build()

                                        )
                                )
                        ).build()

        ).collect(Collectors.toList());
    }

}
