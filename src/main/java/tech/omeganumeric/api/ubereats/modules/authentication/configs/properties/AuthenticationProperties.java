package tech.omeganumeric.api.ubereats.modules.authentication.configs.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties("authentication")
@Data
@NoArgsConstructor
public class AuthenticationProperties {

    private String role;

    private TokenProperties token;

    @Data
    @NoArgsConstructor
    public static class TokenProperties {
        private ApiTokenProperties api;
        private UserTokenProperties user;

        @Data
        @NoArgsConstructor
        public static class ApiTokenProperties {

            private String header;

            private String secret;

            private long expiration;

            private String starter;
        }

        @Data
        @NoArgsConstructor
        @EqualsAndHashCode(callSuper = true)
        public static class UserTokenProperties extends ApiTokenProperties {

            private UserEncoderProperties encoder;

            @Data
            @NoArgsConstructor
            public static class UserEncoderProperties {
                private String delimiter;
            }
        }

    }
}
