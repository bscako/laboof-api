package tech.omeganumeric.api.ubereats.modules.store.exceptions;

public class PasswordException extends RuntimeException {

    public PasswordException(String message, Throwable cause) {
        super(message, cause);
    }

    public PasswordException(String message) {
        super(message);
    }

}
