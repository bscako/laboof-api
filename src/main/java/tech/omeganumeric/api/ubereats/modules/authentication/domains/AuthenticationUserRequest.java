package tech.omeganumeric.api.ubereats.modules.authentication.domains;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Builder
@Data
public class AuthenticationUserRequest {

    @NotNull
    @NotEmpty
    @NotBlank
    private String application;

    @NotNull
    @NotEmpty
    @NotBlank
    private String username;

    @NotNull
    @NotEmpty
    @NotBlank
    private String password;

    private String role;
}
