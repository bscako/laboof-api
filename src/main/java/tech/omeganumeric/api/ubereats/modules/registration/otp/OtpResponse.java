package tech.omeganumeric.api.ubereats.modules.registration.otp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OtpResponse {

    private String message;

    @JsonIgnore
    private boolean success;

}
