package tech.omeganumeric.api.ubereats.modules.registration.configs.properties;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import tech.omeganumeric.api.ubereats.modules.authentication.configs.properties.AuthenticationProperties;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

//@Configuration
@ConfigurationProperties(prefix = "registration")
@Data
public class RegistrationProperties {

    private OtpProperties otp;

    @Autowired
    private AuthenticationProperties authenticationProperties;

    @Data
    public static class OtpProperties {


        private SenderProperties sender;
        private GeneratorProperties generator;

        @Data
        public static class SenderProperties {
            private List<String> providers = new ArrayList<>();
            private String subject;
            private BodyProperties body;

            @Data
            public static class BodyProperties {
                private boolean html;
                private String header;
                private String key;
                private String footer;
            }

        }


        @Data
        public static class GeneratorProperties {

            private String values;

            private ValueProperties value;

            private ExpirationProperties expiration;

            private MessageProperties message;

            @Data
            public static class ValueProperties {
                private int length;
            }


            @Data
            public static class MessageProperties {
                private String success;
            }

            @Data
            public static class ExpirationProperties {
                private TimeUnit unit;

                private long value;
            }
        }


    }

}
