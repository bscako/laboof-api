package tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.data.PhoneData;
import tech.omeganumeric.api.ubereats.modules.store.entities.Phone;

import javax.validation.constraints.Null;
import java.util.Optional;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class PhoneDto extends PhoneData {

    @JsonIgnore
    @Null
    private Long id;


    @Builder(builderMethodName = "PhoneDto")
    public PhoneDto(
            Long id,
            String number) {
        super(number);
        this.id = id;
    }

    public static PhoneDto of(Phone phone) {
        return PhoneDto.PhoneDto()
                .id(phone.getId())
                .number(phone.getNumber())
                .build();
    }

    public static PhoneDto of(PhoneData phone) {
        return PhoneDto.PhoneDto()
                .number(phone.getNumber())
                .build();
    }

    public Phone map(Phone phone) {
        phone.setNumber(
                Optional.ofNullable(this.getNumber()).orElse(phone.getNumber())
        );
        return phone;
    }


}
