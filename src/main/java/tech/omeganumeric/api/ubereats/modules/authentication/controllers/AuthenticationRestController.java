package tech.omeganumeric.api.ubereats.modules.authentication.controllers;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tech.omeganumeric.api.ubereats.configs.AppConfig;
import tech.omeganumeric.api.ubereats.controllers.AppRestController;
import tech.omeganumeric.api.ubereats.exceptions.ControllerException;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiTokenService;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user.UserTokenService;
import tech.omeganumeric.api.ubereats.modules.authentication.domains.AuthenticationUserRequest;
import tech.omeganumeric.api.ubereats.modules.authentication.resources.AuthenticationUserResource;
import tech.omeganumeric.api.ubereats.modules.authentication.services.AuthenticationService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping(
        value = AuthenticationRestController.PATH,
        consumes = AppConfig.API_CONSUMES,
        produces = AppConfig.API_PRODUCES
)
@Api(
        value = AuthenticationRestController.PATH,
        tags = {AuthenticationRestController.TAG, AppRestController.TAG},
        description = " All services relating to access authentication"
)
public class AuthenticationRestController extends ControllerException {

    public static final String PATH = AppConfig.BASE_PATH + "secure/auth";
    public static final String USER = "user";
    public static final String TAG = "Authentication";
    public static final String LOGOUT = USER + "/logout";

    private final Logger log = LoggerFactory.getLogger(this.getClass());


    private final AuthenticationService authenticationService;
    private final ApiTokenService apiTokenService;
    private final UserTokenService userTokenService;

    public AuthenticationRestController(
            AuthenticationService authenticationService, ApiTokenService apiTokenService,
            UserTokenService userTokenService
    ) {
        this.authenticationService = authenticationService;
        this.apiTokenService = apiTokenService;
        this.userTokenService = userTokenService;
    }


    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<AuthenticationUserResource> authenticateUser(
            @RequestBody @Validated AuthenticationUserRequest data,
            HttpServletResponse response
    ) {
        log.info("Authentication user with Credentials: {}", data);
        AuthenticationUserResource authenticationUserResource = this.authenticationService.authenticate(data);
        this.apiTokenService.addHeaderKey(response, this.authenticationService.apiKey().getToken());
        this.userTokenService.addHeaderKey(response, authenticationUserResource.getToken());
        return ResponseEntity.ok(authenticationUserResource);
    }

    @PreAuthorize("isAuthenticated()")
    @RequestMapping(
            value = USER,
            method = RequestMethod.GET
    )
    public ResponseEntity<AuthenticationUserResource> currentUser(
            @AuthenticationPrincipal final UserDetails userDetails,
            HttpServletResponse response
    ) {
        log.debug("Retrieve current authenticated user {}", userDetails);
        AuthenticationUserResource authenticationUserResource = this.authenticationService.authenticatedUser(userDetails);
        this.userTokenService.addHeaderKey(response, authenticationUserResource.getToken());
        return ResponseEntity.ok(authenticationUserResource);
    }


    @PreAuthorize("isAuthenticated()")
    @RequestMapping(
            value = LOGOUT,
            method = RequestMethod.GET
    )
    public ResponseEntity logout(
            @AuthenticationPrincipal final UserDetails userDetails, HttpServletResponse response
    ) {
        log.debug("log out current authenticated user {}", userDetails);
        this.authenticationService.logout(userDetails);
        this.userTokenService.addHeaderKey(response, null);
        return ResponseEntity.ok().build();
    }

}
