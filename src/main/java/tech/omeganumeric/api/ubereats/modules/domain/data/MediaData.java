package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.MediaDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.Media;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class MediaData extends ValidatorDataInterfaces<MediaDto> {

    @NotNull
    @JsonView({Details.class})
    private String name;

    @JsonView({Details.class})
    @NotNull
    private String type;

    @JsonView({Details.class})
    private byte[] resource;

    @Builder.Default
    private boolean avatar = false;

    public static MediaData of(Media media) {
        return Optional.ofNullable(media)
                .map(media1 -> MediaData.builder()
                        .name(media.getName())
                        .type(media.getType())
                        .resource(media.resource())
                        .avatar(media.isAvatar())
                        .build()
                ).orElse(null);
    }

    public static MediaData of(MediaDto media) {
        return Optional.ofNullable(media)
                .map(m -> MediaData.builder()
                        .name(media.getName())
                        .type(media.getType())
                        .resource(media.getResource())
                        .avatar(media.isAvatar())
                        .build()
                )
                .orElse(null)
                ;
    }

    @Override
    public MediaDto toDto() {
        return MediaDto.of(this);
    }
}
