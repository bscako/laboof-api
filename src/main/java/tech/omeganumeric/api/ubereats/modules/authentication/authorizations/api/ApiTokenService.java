package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenService;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;

@Service
@Slf4j
public class ApiTokenService extends TokenService<ApiTokenProvider> {

    private final ApplicationRepository applicationRepository;

    protected ApiTokenService(
            ApiTokenProvider provider,
            ApplicationRepository applicationRepository
    ) {
        super(provider);
        this.applicationRepository = applicationRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Application application = this.applicationRepository
                .findByName(username).orElseThrow(
                        () -> new UsernameNotFoundException(
                                String.format("No application found with name '%s'.", username
                                )
                        )
                );
        return new ApiToken(application);
    }
}
