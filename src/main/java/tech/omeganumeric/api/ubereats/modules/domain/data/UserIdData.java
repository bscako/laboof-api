package tech.omeganumeric.api.ubereats.modules.domain.data;

import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserIdDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@ToString(callSuper = true)
public class UserIdData extends UserIdDataAbstract<UserIdDto, PhoneData> {


    @Builder(builderMethodName = "UserIdData")
    public UserIdData(@NotNull String login, @NotNull String email, @NotNull PhoneData phone) {
        super(login, email, phone);
    }

    public static UserIdData of(User user) {
        return UserIdData.UserIdData()
                .login(user.getLogin())
                .email(user.getEmail())
                .phone(PhoneData.of(user.getPhone()))
                .build();
    }

    public static UserIdData of(UserData user) {
        UserIdDataAbstract<UserIdDto, PhoneData> userIdDataAbstract = user.getIdentifier();
        return UserIdData.UserIdData()
                .login(user.getIdentifier().getLogin())
                .email(user.getIdentifier().getEmail())
                .phone(userIdDataAbstract.getPhone())
                .build();
    }

    public static UserIdData of(UserIdData user) {
        return UserIdData.UserIdData()
                .login(user.getLogin())
                .email(user.getEmail())
                .phone(user.getPhone())
                .build();
    }
    @Override
    public UserIdDto toDto() {
        return UserIdDto.of(this);
    }
}
