package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.NaturalId;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.IdUserDateAudit;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"user"})
@ToString(callSuper = true, exclude = {"user"})
@Table(
        name = Phone.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = "number",
                        name = "uk_phone_number")
        }
)
public class Phone extends IdUserDateAudit {

    public static final String FIELD_ENTITY = "phone";
    public static final String FIELD_ENTITY_TABLE_NAME = "phones";
    public static final String FIELD_NUMBER = "number";

    private static final long serialVersionUID = 3135100165282441128L;

    @NaturalId(mutable = true)
    @Column(nullable = false, length = 10, unique = true)
    @Size(min = 1, max = 10)
    @Pattern(regexp = "^(0|[1-9][0-9]*)$")
    private String number;

    @OneToOne(mappedBy = "phone", orphanRemoval = true)
    @JsonIgnoreProperties(value = {"phone"})
    @Builder.Default
    private User user = null;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "restaurant")
    @JsonIgnoreProperties(value = {"phones"})
    @Builder.Default
    private Restaurant restaurant = null;

    private void basics() {
        this.number = this.getNumber().toUpperCase();
    }

    @PrePersist
    public void beforePersist() {
        this.basics();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }

}
