package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.Token;
import tech.omeganumeric.api.ubereats.modules.encoder.UserEncoder;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(callSuper = true)
public class UserToken extends Token {

    private static final long serialVersionUID = 2038426995877959196L;

    private final User user;
    private final Application application;
    private final Role role;

    public UserToken(
            User user, Application application,
            Role role, UserEncoder usernameEncoder) {
        super(
                usernameEncoder.encode(user, application, role),
                Optional.ofNullable(CredentialOf(user, application, role))
                        .map(UserCredential::getPassword)
                        .orElse(null),
                GrantedAuthoritiesOf(user, application),
                Optional.ofNullable(CredentialOf(user, application, role))
                        .map(UserCredential::isEnabled)
                        .orElse(false),
                Optional.ofNullable(CredentialOf(user, application, role))
                        .map(UserCredential::getLastResetPassword)
                        .map(Date::from).orElse(null)
        );
        this.application = application;
        this.user = user;
        this.role = role;
    }

    private static List<GrantedAuthority> GrantedAuthoritiesOf(User user, Application application) {
        if (user == null || application == null) {
            return new ArrayList<>();
        }
        return user.getUserCredentials().stream()
                .filter(
                        userCredential -> userCredential.getApplication()
                                .getName().equalsIgnoreCase(application.getName())
                )
                .map(UserCredential::getRole)
                .map(Role::getName)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }

    private static UserCredential CredentialOf(User user, Application application, Role role) {
        if (user == null || application == null || role == null) {
            return null;
        }
        return
                user.getUserCredentials().stream()
                .filter(
                        userCredential -> userCredential.getApplication().getName().equalsIgnoreCase(application.getName())
                )
                        .filter(
                                userCredential -> userCredential.getRole().getName().equalsIgnoreCase(role.getName())
                        ).findFirst().orElse(null);
    }
}
