package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.RateMenu;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.RateMenuId;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = RateMenuRepository.PATH,
        path = RateMenuRepository.PATH,
        exported = false
)
public interface RateMenuRepository extends MetaRepository<RateMenu, RateMenuId> {
    String PATH = "rates_menus";

    String SELECTION = "SELECT l from RateMenu l " +
            "left join fetch  l.restaurant " +
            "left join fetch  l.menu " +
            "left join fetch  l.user " +
            "";

    @Query("SELECT l from RateMenu l " +
            "left join fetch  l.restaurant " +
            "left join fetch  l.menu " +
            "left join fetch  l.user " +
            ""
    )
    @Override
    List<RateMenu> findAll();


    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends RateMenu> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    <S extends RateMenu> Optional<S> findOne(Example<S> example);

    @Override
    @RestResource(exported = false)
    List<RateMenu> findAllById(Iterable<RateMenuId> iterable);
}
