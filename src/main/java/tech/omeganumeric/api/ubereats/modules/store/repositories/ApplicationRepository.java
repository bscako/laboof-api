package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = ApplicationRepository.PATH,
        path = ApplicationRepository.PATH,
        exported = false)
public interface ApplicationRepository extends MetaRepository<Application, Long> {
    String PATH = "applications";

    String SELECTION = "SELECT l from Application l " +
            "left join fetch l.userCredentials as c " +
            "";

    @Query("SELECT l from Application l " +
            "left join fetch l.userCredentials as c " +
            ""
    )
    @Override
    List<Application> findAll();


    @Query(SELECTION + "where (:name is null or lower(l.name) = lower(:name)) " +
            "")
    Optional<Application> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Application> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Application> findAllById(Iterable<Long> iterable);
}
