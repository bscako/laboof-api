package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.PhoneDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.Phone;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class PhoneData extends ValidatorDataInterfaces<PhoneDto> {


    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private String number;

    public static PhoneData of(Phone phone) {
        return PhoneData.builder()
                .number(phone.getNumber())
                .build();
    }

    public static PhoneData of(PhoneDto phone) {
        return PhoneData.builder()
                .number(phone.getNumber())
                .build();
    }

    @Override
    public PhoneDto toDto() {
        return PhoneDto.of(this);
    }
}
