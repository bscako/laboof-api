package tech.omeganumeric.api.ubereats.modules.registration.otp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder(builderMethodName = "dataBuilder")
@AllArgsConstructor
@NoArgsConstructor
public class SenderData {

    private String subject;
    private String body;
    private List<String> recipients;

    private List<String> ccList;
    private List<String> bccList;
    private String attachmentPath;

}
