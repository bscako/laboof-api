package tech.omeganumeric.api.ubereats.modules.registration.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.domain.data.PhoneData;
import tech.omeganumeric.api.ubereats.modules.domain.data.UserIdDataAbstract;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.*;
import tech.omeganumeric.api.ubereats.modules.registration.configs.properties.RegistrationProperties;
import tech.omeganumeric.api.ubereats.modules.registration.domains.RegistrationRequest;
import tech.omeganumeric.api.ubereats.modules.registration.domains.RegistrationUserDataOtpRequest;
import tech.omeganumeric.api.ubereats.modules.registration.domains.ValidationOtpRequest;
import tech.omeganumeric.api.ubereats.modules.registration.otp.OtpException;
import tech.omeganumeric.api.ubereats.modules.registration.otp.OtpResponse;
import tech.omeganumeric.api.ubereats.modules.registration.otp.OtpService;
import tech.omeganumeric.api.ubereats.modules.registration.otp.OtpUser;
import tech.omeganumeric.api.ubereats.modules.registration.resources.RegistrationModeResource;
import tech.omeganumeric.api.ubereats.modules.registration.resources.RegistrationResource;
import tech.omeganumeric.api.ubereats.modules.registration.resources.RegistrationUserOtpResource;
import tech.omeganumeric.api.ubereats.modules.registration.resources.RegistrationUserResource;
import tech.omeganumeric.api.ubereats.modules.store.domains.enums.RegistrationMode;
import tech.omeganumeric.api.ubereats.modules.store.entities.*;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.PhoneRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.RoleRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class RegistrationService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final RegistrationProperties registrationProperties;
    private final OtpService otpService;
    private final UserRepository userRepository;
    private final ApplicationRepository applicationRepository;
    private final RoleRepository roleRepository;
    private final PhoneRepository phoneRepository;
    private final RepositoryEntityLinks repositoryEntityLinks;

    public RegistrationService(
            RegistrationProperties registrationProperties, OtpService otpService,
            UserRepository userRepository, ApplicationRepository applicationRepository,
            RoleRepository roleRepository,
            PhoneRepository phoneRepository,
            RepositoryEntityLinks repositoryEntityLinks
    ) {
        this.registrationProperties = registrationProperties;
        this.otpService = otpService;
        this.userRepository = userRepository;
        this.applicationRepository = applicationRepository;
        this.roleRepository = roleRepository;
        this.phoneRepository = phoneRepository;
        this.repositoryEntityLinks = repositoryEntityLinks;
    }

    public RegistrationResource validateOtp(ValidationOtpRequest request) {
        return this.validateOtp(request.getUser().toDto(), request.getOtp());
    }

    public RegistrationResource resentOtp(RegistrationUserDataOtpRequest request) {
        return this.resentOtp(request.toDto());
    }

    private RegistrationResource resentOtp(UserDto userDto) {
        User user = this.findUserByUserIdentifier(userDto.getIdentifier());
        Application application = this.findApplicationByApplicationDto(userDto.getApplication());
        Role role = this.findRoleByRoleDto(userDto.getRole());
        OtpResponse otpGeneration = this.generateOtp(userDto);
        return RegistrationUserOtpResource.RegistrationUserOtpResource()
                .user(user)
                .application(application)
                .role(role)
                .otp(otpGeneration)
                .repositoryEntityLinks(repositoryEntityLinks)
                .build();
    }


    private RegistrationUserOtpResource validateOtp(UserDto userDto, String otp) {
        User user = this.findUserByUserIdentifier(userDto.getIdentifier());
        Application application = this.findApplicationByApplicationDto(userDto.getApplication());
        Role role = this.findRoleByRoleDto(userDto.getRole());

        OtpResponse otpValidation = this.otpService.validateOtp(OtpUser.of(userDto), otp);
        if (!otpValidation.isSuccess()) {
            log.warn("Registration: Otp validation exception for user  {} with otp {}",
                    userDto.getIdentifier().toString(),
                    otp
            );
            throw new OtpException(
                    String.format(
                            "Registration: Otp validation exception for user  %s with otp %s ",
                            userDto.getIdentifier().toString(), otp
                    )
            );
        }

        user.getUserCredentials().forEach(
                userCredential -> {
                    if(
                            userCredential.getRole().getName().equalsIgnoreCase(role.getName())
                                    && userCredential.getApplication().getName().equalsIgnoreCase(application.getName())
                    ) {
                        userCredential.setEnabled(otpValidation.isSuccess());
                    }
                }
        );
        user = userRepository.save(user);

        return RegistrationUserOtpResource.RegistrationUserOtpResource()
                .user(user)
                .application(application)
                .role(role)
                .otp(otpValidation)
                .repositoryEntityLinks(repositoryEntityLinks)
                .build();

    }

//    @Transactional
    public RegistrationResource register(RegistrationRequest registrationRequest) {
        UserDto userDto = registrationRequest.getUser().toDto();
        RegistrationMode mode = registrationRequest.getMode();
        RegistrationModeResource modeResource = this.register(
                userDto,
                mode
        );
        switch (mode) {
            case OTP:
                return modeResource.otpResource().resource();
            case SOCIAL:
                userDto.getAuthentication().setPassword(null);
                return modeResource.userResource().resource();
            default:
                return modeResource.userResource().resource();
        }
    }

    private RegistrationModeResource register(UserDto userDto, RegistrationMode mode) {
        Application application = this.findApplicationByApplicationDto(userDto.getApplication());
        Role role = this.findRoleByRoleDto(userDto.getRole());
        User user = this.findUserByUserIdentifier(userDto.getIdentifier());
        notExistUser(user, application, role);
        if (user == null) {
            user = User.builder()
                    .email(userDto.getIdentifier().getEmail())
                    .login(userDto.getIdentifier().getLogin())
                    .phone(
                            Phone.builder()
                                    .number(userDto.getIdentifier().getPhone().getNumber())
                                    .build()
                    )
                    .userInformation(
                            UserInformation.builder()
                                    .name(userDto.getDetails().getName())
                                    .firstname(userDto.getDetails().getFirstname())
                                    .lastname(userDto.getDetails().getLastname())
                                    .build()
                    )
                    .build();
            user = userRepository.save(user);
        }
        boolean withOtpGeneration = mode.equals(RegistrationMode.OTP);
        boolean enabled = true;
        OtpResponse otpGeneration = null;
        if(withOtpGeneration) {
            otpGeneration = this.generateOtp(userDto);
            enabled = !otpGeneration.isSuccess();
        }

        user.getUserCredentials().addAll(
                Stream.of(UserCredential.builder()
                        .user(user)
                        .application(application)
                        .role(role)
                        .password(userDto.getAuthentication().getPassword())
                        .registrationMode(mode)
                        .enabled(enabled)
                        .build()
                ).collect(Collectors.toSet())
        );
        user = userRepository.save(user);

        if(withOtpGeneration) {
            return RegistrationUserOtpResource.RegistrationUserOtpResource()
                    .user(user)
                    .application(application)
                    .role(role)
                    .otp(otpGeneration)
                    .repositoryEntityLinks(repositoryEntityLinks)
                    .build();
        }
        return RegistrationUserResource.RegistrationUserResource()
                .user(user)
                .application(application)
                .role(role)
                .repositoryEntityLinks(repositoryEntityLinks)
                .build();
    }

    private boolean existUserByUserIdentifier(UserIdDto identifier) {
        boolean login = userRepository.existsByLogin(identifier.getLogin());
        boolean email = userRepository.existsByEmail(identifier.getEmail());
        boolean phone = phoneRepository.existsByNumber(identifier.getPhone().getNumber());
        return (login || email || phone);
    }

    private boolean existUser(UserIdDataAbstract<UserIdDto, PhoneDto> identifier, Application application, Role role) {
        User user = this.findUserByUserIdentifierApplicationAndRole(identifier, application, role);
        return existUser(user, application, role);
    }

    private boolean existUser(User user, Application application, Role role) {
        return (
                user != null
                        && application != null
                        && role != null
                        && user.getUserCredentials().stream()
                        .filter(
                                u-> u.getApplication().getName().equalsIgnoreCase(application.getName())
                        ).filter(
                                u-> u.getRole().getName().equalsIgnoreCase(role.getName())
                        ).count() == 1
        );
    }

    private void notExistUser(UserIdDataAbstract<UserIdDto, PhoneDto> identifier, Application application, Role role) {
        if (this.existUser(identifier, application, role)) {
            log.warn("Registration: EntityExistsException for user  {} ",
                    identifier.toString()
            );
            throw new EntityExistsException(
                    String.format(
                            "Registration: EntityExistsException for user %s ",
                            identifier.toString()
                    )
            );
        }
    }

    private void notExistUser(User user, Application application, Role role ) {
        if (this.existUser(user, application, role)) {
            log.warn("Registration: EntityExistsException for user  {} ",
                    user.toString()
            );
            throw new EntityExistsException(
                    String.format(
                            "Registration: EntityExistsException for user %s ",
                            user.toString()
                    )
            );
        }
    }

    private User findUserByUserIdentifierApplicationAndRole(
            UserIdDataAbstract<UserIdDto, PhoneDto> identifier,
            Application application,
            Role role
    ) {
        User user = this.userRepository
                .findByUniqueKey(
                        Optional.ofNullable(identifier)
                                .map(UserIdDataAbstract::getLogin)
                                .orElse(""),
                        Optional.ofNullable(identifier)
                                .map(UserIdDataAbstract::getEmail)
                                .orElse(""),
                        Optional.ofNullable(identifier)
                                .map(UserIdDataAbstract::getPhone)
                                .map(PhoneData::getNumber)
                                .orElse("")
                )
                .orElse(null);
        if (
                user != null
                        && application != null
                        && role != null
                        && user.getUserCredentials().stream()
                        .filter(
                                u-> u.getApplication().getName().equalsIgnoreCase(application.getName())
                        ).filter(
                                u-> u.getRole().getName().equalsIgnoreCase(role.getName())
                        ).count() == 1
        ) return user;
        return null;
    }



    private OtpResponse generateOtp(UserDto userDto) {
        OtpResponse otpResponse = this.otpService.generateOtp(OtpUser.of(userDto));
        if (!otpResponse.isSuccess()) {
            log.warn("Registration: Otp Generation Exception for user  {} ", userDto.getIdentifier().toString());
            throw new OtpException(
                    String.format(
                            "Registration: Otp Generation Exception for user  %s ",
                            userDto.getIdentifier().toString()
                    )
            );
        }
        return otpResponse;
    }

    private User findUserByUserIdentifier(UserIdDataAbstract<UserIdDto, PhoneDto> identifier) {
        return userRepository.findByUniqueKey(
                Optional.ofNullable(identifier)
                        .map(UserIdDataAbstract::getLogin)
                        .orElse(""),
                Optional.ofNullable(identifier)
                        .map(UserIdDataAbstract::getEmail)
                        .orElse(""),
                Optional.ofNullable(identifier)
                        .map(UserIdDataAbstract::getPhone)
                        .map(PhoneData::getNumber)
                        .orElse("")
        ).orElse(null)
//        .orElseThrow(
//                () -> {
//                    log.warn("Registration: Validation Otp:  EntityNotFoundException for user  {} ",
//                            identifier.toString()
//                    );
//                    return new EntityNotFoundException(
//                            String.format(
//                                    "Registration: EntityNotFoundException for user %s ",
//                                    identifier.toString()
//                            )
//                    );
//                }
//        )
                ;
    }

    private Application findApplicationByApplicationDto(ApplicationDto applicationDto) {
        return applicationRepository.findByName(applicationDto.getName())
                .orElseThrow(() -> {
                    log.warn("Registration: EntityNotFoundException for application  {} ",
                            applicationDto.getName()
                    );
                    return new EntityNotFoundException(
                            String.format(
                                    "Registration: EntityNotFoundException for application %s ",
                                    applicationDto.getName()
                            )
                    );
                });
    }

    private Role findRoleByRoleDto(RoleDto roleDto) {
        return roleRepository.findByName(roleDto.getName())
                .orElseThrow(() -> {
                    log.warn("Registration: EntityNotFoundException for role  {} ",
                            roleDto.getName()
                    );
                    return new EntityNotFoundException(
                            String.format(
                                    "Registration: EntityNotFoundException for role %s ",
                                    roleDto.getName()
                            )
                    );
                });
    }


}
