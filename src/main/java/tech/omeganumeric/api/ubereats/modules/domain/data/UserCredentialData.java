package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserCredentialDto;
import tech.omeganumeric.api.ubereats.modules.store.domains.enums.RegistrationMode;
import tech.omeganumeric.api.ubereats.modules.store.entities.UserCredential;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class UserCredentialData extends ValidatorDataInterfaces<UserCredentialDto> {

    @NotNull(groups = New.class)
    @Null(groups = NewSocial.class)
    @JsonView({AdminDetails.class})
    private String password;

    @Null
    @JsonView({Details.class})
    private Date lastResetPassword;

    @Null
    @JsonView({Details.class})
    private Date lastLoginOn;

    @Null
    @JsonView({AdminDetails.class})
    private boolean enabled;

    @Null
    @JsonView({Details.class})
    @Builder.Default
    private RegistrationMode registeredWith = RegistrationMode.DEFAULT;

    public static UserCredentialData of(UserCredential userCredential) {
        return UserCredentialData.builder()
                .lastResetPassword(Date.from(userCredential.getLastResetPassword()))
                .lastLoginOn(Date.from(userCredential.getLastResetPassword()))
                .password(userCredential.getPassword())
                .enabled(userCredential.isEnabled())
                .registeredWith(userCredential.getRegistrationMode())
                .build();
    }

    public static UserCredentialData of(UserCredentialDto userCredential) {
        return UserCredentialData.builder()
                .lastResetPassword(userCredential.getLastResetPassword())
                .lastLoginOn(userCredential.getLastResetPassword())
                .password(userCredential.getPassword())
                .enabled(userCredential.isEnabled())
                .registeredWith(userCredential.getRegisteredWith())
                .build();
    }

    @Override
    public UserCredentialDto toDto() {
        return UserCredentialDto.of(this);
    }


}
