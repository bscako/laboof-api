package tech.omeganumeric.api.ubereats.modules.domain.data;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.ApplicationDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class ApplicationData extends ValidatorDataInterfaces<ApplicationDto> {

    @NotNull(groups = {New.class, Update.class})
    @JsonView({Details.class})
    private String name;

    public static ApplicationData of(Application application) {
        return ApplicationData.builder()
                .name(application.getName())
                .build();
    }

    public static ApplicationData of(ApplicationDto application) {
        return ApplicationData.builder()
                .name(application.getName())
                .build();
    }

    @Override
    public ApplicationDto toDto() {
        return ApplicationDto.of(this);
    }


}
