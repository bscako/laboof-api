package tech.omeganumeric.api.ubereats.modules.registration.otp.sender.phone;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.omeganumeric.api.ubereats.modules.registration.otp.SenderData;

@Data
@EqualsAndHashCode(callSuper = true)
@Builder
public class SenderDataPhone extends SenderData {

}
