package tech.omeganumeric.api.ubereats.modules.store.services;

import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;

import javax.transaction.Transactional;
import java.time.Instant;

@Service
public class StoreService {

    private final UserRepository userRepository;

    public StoreService(
            UserRepository userRepository
    ) {
        this.userRepository = userRepository;
    }

    @Transactional
    public void updateLastLoginOn(User user, Application application, Role role, Instant loginOn) {
        user.getUserCredentials().stream()
                .filter(userCredential -> userCredential
                        .getApplication().getName().equalsIgnoreCase(application.getName())
                )
                .filter(userCredential ->
                        userCredential.getRole().getName().equalsIgnoreCase(role.getName())
                )
                .forEach(
                        userCredential -> userCredential.setLastLoginOn(loginOn)
                );
        this.userRepository.save(user);
    }
}
