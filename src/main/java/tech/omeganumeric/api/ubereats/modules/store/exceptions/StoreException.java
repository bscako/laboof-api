package tech.omeganumeric.api.ubereats.modules.store.exceptions;

public class StoreException extends RuntimeException {

    public StoreException(String message, Throwable cause) {
        super(message, cause);
    }

    public StoreException(String message) {
        super(message);
    }

}
