package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.user;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenProvider;
import tech.omeganumeric.api.ubereats.modules.authentication.configs.properties.AuthenticationProperties;

import java.io.Serializable;

@Component
@Slf4j
@Data
@EqualsAndHashCode(callSuper = true)
public class UserTokenProvider extends TokenProvider
        implements Serializable {

    public UserTokenProvider(AuthenticationProperties properties) {
        super(
                properties.getToken().getUser().getHeader().toUpperCase(),
                properties.getToken().getUser().getSecret(),
                properties.getToken().getUser().getExpiration(),
                properties.getToken().getUser().getStarter()
        );
    }


}
