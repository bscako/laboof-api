package tech.omeganumeric.api.ubereats.modules.registration.controllers;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.omeganumeric.api.ubereats.configs.AppConfig;
import tech.omeganumeric.api.ubereats.controllers.AppRestController;
import tech.omeganumeric.api.ubereats.exceptions.ControllerException;
import tech.omeganumeric.api.ubereats.modules.registration.domains.RegistrationUserDataOtpRequest;
import tech.omeganumeric.api.ubereats.modules.registration.domains.ValidationOtpRequest;
import tech.omeganumeric.api.ubereats.modules.registration.resources.RegistrationResource;
import tech.omeganumeric.api.ubereats.modules.registration.services.RegistrationService;

@RestController
//@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = ValidationOtpRestController.PATH)
@Api(
        value = ValidationOtpRestController.PATH,
        tags = {AppRestController.TAG, RegistrationRestController.TAG},
        description = RegistrationRestController.DESCRIPTION
)
public class ValidationOtpRestController extends ControllerException {

    public static final String PATH = AppConfig.BASE_PATH + "user/validate/otp";

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final RegistrationService registrationService;

    public ValidationOtpRestController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity validateOtp(
            @RequestBody ValidationOtpRequest validationOtpRequest
    ) {
        log.debug("validating otp {}", validationOtpRequest);
        RegistrationResource registrationResource = this.registrationService.validateOtp(validationOtpRequest);
        return ResponseEntity.ok(registrationResource);
    }

    @RequestMapping(
            value = "/{otp}",
            method = RequestMethod.POST
    )
    public ResponseEntity validateOtp2(
            @PathVariable final String otp,
            @RequestBody final RegistrationUserDataOtpRequest user
    ) {
        log.debug("validating otp  method 2 {} {}", otp, user);
        ValidationOtpRequest request = ValidationOtpRequest.builder()
                .otp(otp)
                .user(user)
                .build();
        return validateOtp(request);
    }


}
