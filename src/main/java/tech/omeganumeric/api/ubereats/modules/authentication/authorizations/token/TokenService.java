package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.stream.Collectors;

public abstract class TokenService<
        Provider extends TokenProvider> implements UserDetailsService {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final Provider provider;


    protected TokenService(Provider provider) {
        this.provider = provider;
    }

    public void addHeaderKey(HttpServletResponse response, String token) {
        response.addHeader(
                this.provider.getHeader(),
                this.provider.getStarter() + token
        );
    }

    public Authentication authentication(String token, String credentials, String role) {
        final UserDetails userDetails = this
                .loadUserByUsername(
                        provider.getUsernameFromToken(token)
                );
        return this.authenticationToken(userDetails, credentials, role);
    }

    public Authentication authentication(UserDetails userDetails, String credentials, String role) {
        return this.authenticationToken(userDetails, credentials, role);
    }

    public Authentication authentication(
            HttpServletRequest request,
            UserDetails userDetails,
            String credentials,
            String role
    ) {
        UsernamePasswordAuthenticationToken authentication = authenticationToken(userDetails, credentials, role);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        return authentication;
    }

//    public Authentication authentication(
//            HttpServletRequest request,
//            UserDetails userDetails,
//            String credentials
//    ) {
//        UsernamePasswordAuthenticationToken authentication = authenticationToken(
//                userDetails, credentials,
//                userDetails.getAuthorities()
//                        .stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList())
//        );
//        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
//        return authentication;
//    }

    private boolean haveAuthority(UserDetails userDetails, String role) {
        return !userDetails.getAuthorities().stream()
                .map(o -> ((GrantedAuthority) o).getAuthority())
                .filter(s -> s.equalsIgnoreCase(role))
                .collect(Collectors.toList()).isEmpty();
    }


    private UsernamePasswordAuthenticationToken authenticationToken(
            UserDetails userDetails, String credentials,
            String role
    ) {

        if (!this.haveAuthority(userDetails, role)) {
            throw
                    new AuthenticationCredentialsNotFoundException(
                            String.format(
                                    "Security exception for user with username '%s' : " +
                                            "You don't have sufficient authority to authenticate " +
                                            "as %s",
                                    userDetails.getUsername(),
                                    role
                            )
                    );
        }
        return new UsernamePasswordAuthenticationToken(
                userDetails, credentials,
                userDetails.getAuthorities()
        );
    }


}
