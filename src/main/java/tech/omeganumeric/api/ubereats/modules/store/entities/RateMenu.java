package tech.omeganumeric.api.ubereats.modules.store.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import tech.omeganumeric.api.ubereats.modules.store.entities.audit.UserDateAudit;
import tech.omeganumeric.api.ubereats.modules.store.entities.embeddable.RateMenuId;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@EqualsAndHashCode(callSuper = true, exclude = {"restaurant", "menu", "user"})
@ToString(callSuper = true, exclude = {"restaurant", "menu", "user"})
@Table(name = RateMenu.FIELD_ENTITY_TABLE_NAME,
        uniqueConstraints = {
                @UniqueConstraint(
                        columnNames = {"restaurant_id", "menu_id", "user_id"},
                        name = "uk_restaurant_menu_ids")
        }

)
public class RateMenu extends UserDateAudit {

    public static final String FIELD_ENTITY = "Rate_Menu";
    public static final String FIELD_ENTITY_TABLE_NAME = "Rates_Menus";
    private static final long serialVersionUID = 7691304461650702232L;

    @EmbeddedId
    private RateMenuId id;

    @Column(nullable = false)
    @NotNull
    @Size(min = 1, max = 5)
    private int rate;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("userId")
    @JsonIgnoreProperties(value = {"rateMenus"})
    @NotNull
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("restaurantId")
    @JsonIgnoreProperties(value = {"rateMenus"})
    @NotNull
    private Restaurant restaurant;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("menuId")
    @JsonIgnoreProperties(value = {"rateMenus"})
    @NotNull
    private Menu menu;

    public void updateAssociations() {
    }

    private void basics() {

    }

    @PrePersist
    public void beforePersist() {
        this.basics();
        this.updateAssociations();
    }

    @PreUpdate
    public void beforeUpdate() {
        this.basics();
        this.updateAssociations();
    }

    @PreRemove
    public void beforeRemove() {
        this.basics();
    }

}
