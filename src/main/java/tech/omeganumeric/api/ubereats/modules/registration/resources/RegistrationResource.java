package tech.omeganumeric.api.ubereats.modules.registration.resources;

public interface RegistrationResource<T> {
    T resource();
}
