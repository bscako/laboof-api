package tech.omeganumeric.api.ubereats.modules.registration.otp;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import tech.omeganumeric.api.ubereats.modules.domain.data.UserIdDataAbstract;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.*;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;

@Data
@EqualsAndHashCode(callSuper = true)
public class OtpUser extends UserDto {


    @Builder(builderMethodName = "OtpUser")
    public OtpUser(
            UserIdDataAbstract<UserIdDto, PhoneDto> identifier, UserInformationDto details,
            ApplicationDto application, RoleDto role, UserCredentialDto credential,
            Long id
    ) {
        super(identifier, details, application, role, credential, id);
    }

    public static OtpUser of(User user, Application application, Role role) {
        return OtpUser.of(UserDto.of(user, application, role));
    }

    public static OtpUser of(UserDto user) {
        return OtpUser.OtpUser()
                .identifier(user.getIdentifier())
                .id(user.getId())
                .details(user.getDetails())
                .application(user.getApplication())
                .role(user.getRole())
                .credential(user.getCredential())
                .build();
    }
}
