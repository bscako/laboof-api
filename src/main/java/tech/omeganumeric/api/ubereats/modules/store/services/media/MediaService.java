/**
 *
 */
package tech.omeganumeric.api.ubereats.modules.store.services.media;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import tech.omeganumeric.api.ubereats.modules.store.configs.properties.StoreProperties;


@Slf4j
@Service
public class MediaService extends AbstractMediaService {

    private static final String PATH = "media";


    public MediaService(StoreProperties storeProperties) {
        super(storeProperties, PATH);
    }
}
