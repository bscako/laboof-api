package tech.omeganumeric.api.ubereats.modules.registration.resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.ResourceSupport;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.UserDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.entities.Role;
import tech.omeganumeric.api.ubereats.modules.store.entities.User;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.RoleRepository;
import tech.omeganumeric.api.ubereats.modules.store.repositories.UserRepository;

@Value
@EqualsAndHashCode(callSuper = true)
public class RegistrationUserResource extends ResourceSupport
        implements RegistrationModeResource, RegistrationResource<RegistrationUserResource> {

    @JsonIgnoreProperties({"id"})
    private UserDto user;

    @Builder(builderMethodName = "RegistrationUserResource")
    public RegistrationUserResource(
            User user,
            Application application,
            Role role,
            RepositoryEntityLinks repositoryEntityLinks
    ) {
        this.user = UserDto.of(user, application, role);
        this.add(repositoryEntityLinks
                .linkToSingleResource(UserRepository.class, user.getId())
                .withSelfRel());
        this.add(repositoryEntityLinks
                .linkToSingleResource(ApplicationRepository.class, application.getId())
                .withSelfRel());
        this.add(repositoryEntityLinks
                .linkToSingleResource(RoleRepository.class, role.getId())
                .withSelfRel());
    }

    @Override
    public RegistrationUserResource userResource() {
        return this;
    }

    @JsonIgnore
    @Override
    public RegistrationUserOtpResource otpResource() {
        return null;
    }

    @JsonIgnore
    @Override
    public RegistrationUserResource resource() {
        return userResource();
    }
}
