package tech.omeganumeric.api.ubereats.modules.store.entities.audit;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import tech.omeganumeric.api.ubereats.modules.store.entities.meta.AbstractMetaEntity;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@EntityListeners(AuditingEntityListener.class)
public abstract class UserAudit extends AbstractMetaEntity {

    @CreatedBy
    @Column(updatable = false)
    private Long createdBy;

    @LastModifiedBy
    @Column()
    private Long updatedBy;


}
