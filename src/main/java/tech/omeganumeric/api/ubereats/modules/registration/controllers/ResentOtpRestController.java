package tech.omeganumeric.api.ubereats.modules.registration.controllers;

import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tech.omeganumeric.api.ubereats.configs.AppConfig;
import tech.omeganumeric.api.ubereats.controllers.AppRestController;
import tech.omeganumeric.api.ubereats.exceptions.ControllerException;
import tech.omeganumeric.api.ubereats.modules.registration.domains.RegistrationUserDataOtpRequest;
import tech.omeganumeric.api.ubereats.modules.registration.resources.RegistrationResource;
import tech.omeganumeric.api.ubereats.modules.registration.services.RegistrationService;

@RestController
//@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = ResentOtpRestController.PATH)
@Api(
        value = ResentOtpRestController.PATH,
        tags = {AppRestController.TAG, RegistrationRestController.TAG},
        description = RegistrationRestController.DESCRIPTION
)
public class ResentOtpRestController extends ControllerException {

    public static final String PATH = AppConfig.BASE_PATH + "user/resent/otp";

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private final RegistrationService registrationService;

    public ResentOtpRestController(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity resentOtp(
            @RequestBody RegistrationUserDataOtpRequest registrationUserDataOtpRequest
    ) {
        log.debug("resent otp {}", registrationUserDataOtpRequest);
        RegistrationResource registrationUserResource =
                this.registrationService.resentOtp(registrationUserDataOtpRequest);
        return ResponseEntity.ok(registrationUserResource);
    }


}
