package tech.omeganumeric.api.ubereats.modules.store.repositories.address;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.address.Department;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = DepartmentRepository.PATH,
        path = DepartmentRepository.PATH,
        exported = false)
public interface DepartmentRepository extends MetaRepository<Department, Long> {
    String PATH = "departments";

    String SELECTION = "SELECT l from Department l " +
            "left join fetch l.districts as d " +
            "left join fetch l.country as c " +
            "";

    @Query("SELECT l from Department l " +
            "left join fetch l.districts as d " +
            "left join fetch l.country as c " +
            ""
    )
    @Override
    List<Department> findAll();


//    @Query(SELECTION + "where (:name is null or lower(l.name) = lower(:name)) " +
//            "")
//    Optional<Department> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Department> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Department> findAllById(Iterable<Long> iterable);
}
