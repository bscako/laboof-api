package tech.omeganumeric.api.ubereats.modules.authentication.resources;

import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.data.rest.webmvc.support.RepositoryEntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiToken;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenProvider;
import tech.omeganumeric.api.ubereats.modules.domain.transfer.dtos.ApplicationDto;
import tech.omeganumeric.api.ubereats.modules.store.entities.Application;
import tech.omeganumeric.api.ubereats.modules.store.repositories.ApplicationRepository;

@Value
@EqualsAndHashCode(callSuper = true)
public class AuthenticationApplicationResource extends ResourceSupport {

    private ApplicationDto application;

    private String token;

    public AuthenticationApplicationResource(
            Application application,
            TokenProvider tokenProvider,
            RepositoryEntityLinks repositoryEntityLinks
    ) {
        this.application = ApplicationDto.of(application);
        this.token = tokenProvider.generateToken(new ApiToken(application));
        Link link = repositoryEntityLinks.linkToSingleResource(ApplicationRepository.class, application.getId());
        this.add(link.withSelfRel());
    }

}
