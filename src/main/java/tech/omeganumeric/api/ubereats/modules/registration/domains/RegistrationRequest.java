package tech.omeganumeric.api.ubereats.modules.registration.domains;

import lombok.*;
import tech.omeganumeric.api.ubereats.modules.store.domains.enums.RegistrationMode;
import tech.omeganumeric.api.ubereats.modules.validators.RequestValidatorInterfaces;

import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(builderMethodName = "RegistrationRequest")
public class RegistrationRequest extends RequestValidatorInterfaces {

    @NotNull
    private RegistrationUserDataRequest user;

    private RegistrationMode mode;

}
