/**
 *
 */
package tech.omeganumeric.api.ubereats.modules.store.services.media;

import com.google.common.io.ByteSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ResourceUtils;
import tech.omeganumeric.api.ubereats.modules.store.configs.properties.StoreProperties;
import tech.omeganumeric.api.ubereats.modules.store.entities.Media;
import tech.omeganumeric.api.ubereats.modules.store.exceptions.StoreException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author BSCAKO
 *
 */
@Slf4j
public abstract class AbstractMediaService {

    private final Path fileStorageLocation;

    public AbstractMediaService(StoreProperties storeProperties, String path) {
        log.debug("Initializing");
        String realPath = storeProperties.getFile().getPath() + "/" + path;
        try {
            this.fileStorageLocation = Paths.get(
                    ResourceUtils.getFile(realPath).getAbsolutePath()
            ).toAbsolutePath().normalize();
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new StoreException("Could not create the directory where the uploaded files will be stored.",
                    ex);
        }
    }

    private String filename(Media media) {
        // Check if the file's name contains invalid characters
        if (media.getName().contains("..")) {
            throw new StoreException("Sorry! Filename contains invalid path sequence " + media.getName());
        }
        return String.format(
                "%s.%s",
                media.getName(), media.getType()
        );
    }

    public Media storeMediaResource(byte[] bytes, Media media) {
//        if ((media.getName() == null) && (multipartFile.getOriginalFilename() != null)) {
//            media.setName(StringUtils.cleanPath(multipartFile.getOriginalFilename()));
//        }
        if (media != null) {

            Path targetLocation = this.fileStorageLocation.resolve(filename(media));

            // Copy file to the target location (Replacing existing file with
            // the same name)
            try {
                InputStream inputStream = ByteSource.wrap(bytes).openStream();
//                InputStream inputStream = new ByteArrayInputStream(bytes);
                Files.copy(inputStream, targetLocation, StandardCopyOption.REPLACE_EXISTING);

            } catch (IOException e) {
                log.warn("Sorry! Can store file : {}", e.toString());
                throw new StoreException("Sorry! Can store file", e);
            }
        }
        return media;
    }

    public void removeMediaResource(Media media) {
        Path targetLocation = this.fileStorageLocation.resolve(media.getName()).normalize();
        try {
            Files.delete(targetLocation);
        } catch (IOException e) {
            log.warn("Sorry! Can store file ", e);
            throw new StoreException("Sorry! Can remove file", e);
        }
    }


    public byte[] loadMediaResource(Media media) throws IOException {
        Path path = this.fileStorageLocation.resolve(filename(media)).toAbsolutePath().normalize();
        BufferedImage originalImage = ImageIO.read(
                new File(path.toAbsolutePath().toString())
        );
        // convert BufferedImage to byte array
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(originalImage, media.getType(), byteArrayOutputStream);
        byteArrayOutputStream.flush();
//        media.setFile(byteArrayOutputStream.toByteArray());
        byte[] resource = byteArrayOutputStream.toByteArray();
        log.info("array : {}", resource.toString());
        byteArrayOutputStream.close();
        return resource;
    }

//	public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
//        return Arrays.asList(files)
//                .stream()
//                .map(file -> uploadFile(file))
//                .collect(Collectors.toList());
//    }


}
