package tech.omeganumeric.api.ubereats.modules.store.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;
import tech.omeganumeric.api.ubereats.modules.store.entities.Address;
import tech.omeganumeric.api.ubereats.modules.store.repositories.meta.MetaRepository;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RepositoryRestResource(
        collectionResourceRel = AddressRepository.PATH,
        path = AddressRepository.PATH,
        exported = false)
public interface AddressRepository extends MetaRepository<Address, Long> {
    String PATH = "address";

    String SELECTION = "SELECT l from Address l " +
            "left join fetch l.town as t " +
            "";

    @Query("SELECT l from Address l " +
            "left join fetch l.town as t " +
            ""
    )
    @Override
    List<Address> findAll();


//    @Query(SELECTION + "where (:name is null or lower(l.name) = lower(:name)) " +
//            "")
//    Optional<Address> findByName(@Param("name") String name);

    @Override
    @RestResource(exported = false)
    void deleteAll();

    @Override
    @RestResource(exported = false)
    void deleteAllInBatch();

    @Override
    @RestResource(exported = false)
    <S extends Address> List<S> saveAll(Iterable<S> iterable);

    @Override
    @RestResource(exported = false)
    List<Address> findAllById(Iterable<Long> iterable);
}
