package tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


@Data
public abstract class TokenProvider implements Serializable {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    static final String CLAIM_KEY_USERNAME = "sub";
    static final String CLAIM_KEY_CREATED = "iat";
    private static final long serialVersionUID = 2212428894870252583L;

    private final String header;
    private final String starter;
    private final long expiration; // 1h
    private final Clock clock = DefaultClock.INSTANCE;
    protected String secret;

    public TokenProvider(
            String header,
            String secret,
            long expiration,
            String starter
    ) {
        this.header = header.trim().toUpperCase();
        this.secret = Base64.getEncoder().encodeToString(secret.trim().getBytes());
        this.expiration = expiration;
        this.starter = starter.trim() + " ";
    }


    public String resolveToken(HttpServletRequest request) {
        String token = request.getHeader(this.header);
        if (token != null && token.startsWith(this.starter)) {
            return token.substring(this.starter.length());
        }
        return null;
    }

    public <T extends Token> String generateToken(T userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return this.doGenerateToken(claims, userDetails.getUsername());
    }


    public String getUsernameFromToken(String token) {
        return this.getClaimFromToken(token, Claims::getSubject);
    }

    private Date getIssuedAtDateFromToken(String token) {
        return this.getClaimFromToken(token, Claims::getIssuedAt);
    }

    private Date getExpirationDateFromToken(String token) {
        return this.getClaimFromToken(token, Claims::getExpiration);
    }

    private <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = this.getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private boolean isTokenExpired(String token) {
        final Date exp = this.getExpirationDateFromToken(token);
        return !exp.before(this.clock.now());
    }

    private boolean canTokenBeRefreshed(String token, Date lastPasswordReset) {
        final Date created = this.getIssuedAtDateFromToken(token);
        return this.isCreatedBeforeLastPasswordReset(created, lastPasswordReset)
                && (this.isTokenExpired(token) || this.ignoreTokenExpiration(token));
    }

    private String refreshToken(String token) {
        final Date createdDate = this.clock.now();
        final Date expirationDate = this.calculateExpirationDate(createdDate);

        final Claims claims = this.getAllClaimsFromToken(token);
        claims.setIssuedAt(createdDate);
        claims.setExpiration(expirationDate);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, this.secret)
                .compact();
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        Token user = (Token) userDetails;
        final String username = this.getUsernameFromToken(token);
        final Date created = this.getIssuedAtDateFromToken(token);
        return (username.equalsIgnoreCase(user.getUsername())
                && !this.isTokenExpired(token)
                && !this.isCreatedBeforeLastPasswordReset(
                created, user.getLastPasswordResetDate()
        ));
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(this.secret)
                .parseClaimsJws(token)
                .getBody();
    }

    private boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
        return ((lastPasswordReset == null) || !created.before(lastPasswordReset));
    }

    private boolean ignoreTokenExpiration(String token) {
        // here you specify tokens, for that the expiration is ignored
        return false;
    }

    private String doGenerateToken(Map<String, Object> claims, String subject) {
        final Date createdDate = this.clock.now();
        final Date expirationDate = this.calculateExpirationDate(createdDate);
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, this.secret)
                .compact();
    }

    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + (this.expiration * 1000));
    }
}
