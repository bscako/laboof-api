package tech.omeganumeric.api.ubereats.modules.domain.data;

import tech.omeganumeric.api.ubereats.modules.domain.transfer.converters.DtoConverter;

import java.io.Serializable;

public abstract class ValidatorDataInterfaces<Dto> implements DtoConverter<Dto>, Serializable {

    interface New {

    }

    interface NewSocial extends New{

    }

    interface Exist {

    }

    interface Update extends Exist {

    }

    interface UpdateSocial extends Update {

    }

    interface Details {

    }

    interface AdminDetails extends Details {

    }
}
