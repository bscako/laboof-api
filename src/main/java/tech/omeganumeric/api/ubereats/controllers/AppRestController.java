package tech.omeganumeric.api.ubereats.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tech.omeganumeric.api.ubereats.configs.AppConfig;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.api.ApiTokenService;
import tech.omeganumeric.api.ubereats.modules.authentication.controllers.AuthenticationRestController;
import tech.omeganumeric.api.ubereats.modules.authentication.resources.AuthenticationApplicationResource;
import tech.omeganumeric.api.ubereats.modules.authentication.services.AuthenticationService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
//@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping(value = AppRestController.PATH)
@Api(
        value = AppRestController.PATH,
        tags = {AppRestController.TAG},
        description = "All services relating to access webservices"
)
public class AppRestController {

    public static final String PATH = AppConfig.BASE_PATH + "secure";
    public static final String KEY = "key";
    public static final String TAG = "API";

    private final Logger log = LoggerFactory.getLogger(this.getClass());


    private final AuthenticationService authenticationService;
    private final ApiTokenService apiTokenService;

    public AppRestController(AuthenticationService authenticationService, ApiTokenService apiTokenService) {
        this.authenticationService = authenticationService;
        this.apiTokenService = apiTokenService;
    }


    @PreAuthorize("permitAll()")
    @RequestMapping(
            value = KEY,
            method = RequestMethod.GET
    )
    @ApiOperation(
            value = KEY,
            tags = AuthenticationRestController.TAG
    )
    public ResponseEntity<AuthenticationApplicationResource> retrieveApiKey(HttpServletRequest request, HttpServletResponse response) {
        log.debug("Retrieving api key ");
        AuthenticationApplicationResource apiKeyResource = this.authenticationService.apiKey();
        this.apiTokenService.addHeaderKey(response, apiKeyResource.getToken());
        return ResponseEntity.ok(apiKeyResource);
    }
}
