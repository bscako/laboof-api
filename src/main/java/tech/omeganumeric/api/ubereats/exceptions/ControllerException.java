/**
 *
 */
package tech.omeganumeric.api.ubereats.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import tech.omeganumeric.api.ubereats.modules.authentication.authorizations.token.TokenException;
import tech.omeganumeric.api.ubereats.modules.store.exceptions.PasswordException;
import tech.omeganumeric.api.ubereats.modules.store.exceptions.StoreException;

import javax.persistence.EntityNotFoundException;

/**
 * @author BSCAKO
 *
 */
public class ControllerException {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    private void log(Exception e) {
        log.info("resolved exception ", e);
    }

    @ExceptionHandler({TokenException.class})
    public ResponseEntity handleAuthenticationException(TokenException e) {
        log(e);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }

    @ExceptionHandler({PasswordException.class})
    public ResponseEntity handleAuthenticationException(PasswordException e) {
        log(e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity handleEntityNotFoundException(EntityNotFoundException e) {
        log(e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }

    @ExceptionHandler({TransactionSystemException.class})
    public ResponseEntity handleAuthenticationException(TransactionSystemException e) {
        Throwable ex = e.getRootCause();
        if(ex instanceof PasswordException){
            log(e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
        if (ex instanceof StoreException) {
            log(e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ex.getMessage());
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.toString());
    }

//	@ExceptionHandler({ AuthenticationException.class })
//	public ResponseEntity handleAuthenticationException(AuthenticationException e) {
//		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
//	}
//
////	@ExceptionHandler({ AuthorizationException.class })
////	public ResponseEntity<String> handleAuthorizationException(AuthorizationException e) {
////		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
////	}
//
//	@ExceptionHandler({ JsonWebTokenException.class })
//	public ResponseEntity<String> handleTokenException(JsonWebTokenException e) {
//		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
//	}
//
//	@ExceptionHandler({ MalformedJwtException.class })
//	public ResponseEntity<String> handleMalformedJwtException(MalformedJwtException e) {
//		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Bad token.");
//	}
//	@ExceptionHandler({ IllegalArgumentException.class })
//	public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e) {
//		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
//	}
//
//	@ExceptionHandler({ BadCredentialsException.class })
//	public ResponseEntity<String> handleBadCredentialsException(BadCredentialsException e) {
//		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
//	}
//
//	@ExceptionHandler({ FileNotFoundException.class })
//	public ResponseEntity<String> handleFileNotFoundException(FileNotFoundException e) {
//		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
//	}
//
//	@ExceptionHandler(MultipartException.class)
//	public ResponseEntity<String> handleMultipartException(MultipartException e) {
//		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
//	}

}
